<!DOCTYPE html>
<html>
<body>
    <div class="wrapper">
    <?php  if($this->session->userdata('lw_login')->s_changepwdstatus==1){
    $this->load->view('helper/sidebar'); } ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Change Password</h2>
                        </div>
                      <?php if($this->session->flashdata('message')) { ?>
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('message'); ?>
                                    </div>
                                <?php } ?>
                           
                            <form action="" id="mgrForm" method="post" enctype="multipart/form-data" >
                            <div class="page_box">                        
                                        <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Old Password <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="password" name="oldpass" id="oldpass" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        
                                        <div class="sep_box">                                        
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">New Password<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <p id="msg"></p>
                                                            <input title="Password Must contain 1 special character,1 uppercase, 1 lowercase and 1 numeric." type="password" name="newpass" id="newpass" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sep_box">                                        
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Confirm Password </div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="password" name="cpass" id="cpass" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input  type="submit" name="submit" onclick="return Validate()" value="Submit" id="submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>                          
                            </div>
                            </form>                                  
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
   
</html>

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#mgrForm").validate({
      rules: {
        oldpass: "required",
        newpass: "required",
        cpass:"required"
        
      },
      messages: {
        oldpass: "This field is required!",
        newpass: "This field is required!",
        cpass: "This field is required!"

      },
      submitHandler: function(form) {
        form.submit();
      }
  });
});
</script>
 <script type="text/javascript">
    function Validate() {
        var password = document.getElementById("newpass").value;
        var confirmPassword = document.getElementById("cpass").value;
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
        return true;
    }

    $("#newpass").keyup(function(){
        if(this.value.length < 6)
        var validated = 'Minimum 6 characters';
        $('#msg').text(validated);
/*
var pattern = /[!@#$%\^&*(){}[\]<>?/|\-]/;

      $("#newpass").each(function () {
        var validated =  'Correct format';
        if(this.value.length < 7)
            validated = 'Minimum 7 characters';
        if(!pattern.test(this.value))
           validated = 'At least one special character';
        if(!/[a-z]/.test(this.value))
            validated = 'At least one lowercase character';
        if(!/[A-Z]/.test(this.value))
            validated = 'At least one uppercase character';
        if(!/[0-9]/.test(this.value))
            validated = 'At least one numeric';
        if(/[^0-9a-zA-Z!@#$%\^&*(){}[\]<>?/|\-]/.test(this.value))
            validated = 'Not valid';
        //alert(validated);
        $('#msg').text(validated);

        
    });*/
   
        
  });

</script>