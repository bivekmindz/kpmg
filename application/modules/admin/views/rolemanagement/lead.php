<!DOCTYPE html>
<html>
<body>
 <div class="wrapper">

    <?php  $this->load->view('helper/sidebar'); 
    $logintype=$this->session->userdata('lw_login')->s_logintype; ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Lead List</h2>
                        </div>
                        <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                       
                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example1">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Lead Name</th>
                                                <th bgcolor='red'>Company Name</th>
                                                <th bgcolor='red'>Email/Login ID</th>
                                                <th bgcolor='red'>Mobile No.</th>
                                                <th bgcolor='red'>Country</th>
                                                <th bgcolor='red'>Address</th>
                                                <th bgcolor='red'>Pin Code</th>
                                                <th bgcolor='red'>Date/Time</th> 
                                                <th bgcolor='red'>Assigned Date/Time</th> 
                                                <th bgcolor='red'>Action</th>           
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                 <?php 
                                 $ik=0;
                                        if(!empty($vieww))
                                        {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $ik=$page*10; 
                                            }
                                        } ?>

                                        <?php $i=$ik;#0;
                                            foreach ($vieww as $value) {
                                            $i++;
                                            
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->s_username; ?></td>
                                            <td><?php echo $value->s_name; ?></td>
                                            <td><?php echo $value->s_email; ?></td>
                                            <td>+<?php echo $value->code.'-'.$value->s_mobile; ?></td> 
                                            <td><?php echo $value->name; ?></td>
                                            <td><?php echo $value->s_address; ?></td>
                                            <td><?php echo $value->s_pincode; ?></td>
                                            <td><?php echo date('d-m-Y h:i:s A',strtotime($value->s_createdon)); ?></td>
                                            <td><?php echo date('d-m-Y h:i:s A',strtotime($value->s_modifiedon)); ?></td>
                                            <td><a href="<?php echo base_url();?>admin/rolemngmnt/editlead/<?php echo $value->s_loginid;?>" ><i class="fa fa-pencil"></i> </a></td>
                                            

                                            
                                        </tr>
                                    <?php } ?>
                                     </tbody>
                                    </table>
                                    <div class="pagi_nation">
                                  <ul class="pagination">
                                    <?php foreach ($links as $link) {
                                    //p($link); //exit();
                                    echo "<li class='newli'>". $link."</li>";
                                    } ?>
                                  </ul>
                                </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>