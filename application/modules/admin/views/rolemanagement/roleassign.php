 <!DOCTYPE html>
<html>
<body>
<div class="wrapper">
<?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">

               
            <div class="page_contant">
           
             <div class="col-lg-12">
                <div class="page_name">
                            <h2>Role Management</h2>
                        </div>
                      <p style="color:red"><?php  echo $this->session->flashdata('message'); ?></p>
                       
                        <div class="page_box">
                             <form action="" id="" method="post" enctype="multipart/form-data" >
                                        <div class="sep_box">

                                      <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Role Type<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select id="emptypeid" name="emptypeid" onchange="getemp()">
                                                            <option value="">Select Role Type</option>
                                        <?php foreach ($emp as $key => $value) { ?>

                                    <option value="<?php echo $value->et_id; ?>">
                                                             <?php echo $value->et_name; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                     
                                      <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Employee<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select id="empid" name="empid" onchange="getmenu()">
                                                            <option value="">Select Employee</option>
                                        
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                <div id="menu" style="display:none;">
                                    <div class="sep_box">

                                      <div class="col-lg-6">
                                                <p class="he">Assign Menu</p>
                            <?php   foreach ($mainmenu as $key => $value) 
                                    { 
                                        if($value->parentid==0)
                                        {
                                            echo '<p><input type="checkbox" id="menu'.$value->id.'" onclick="allmenu('.$value->id.')" value="'.$value->id.'"> '.$value->menuname.'</p>'; 

                                            foreach ($mainmenu as $key => $val) 
                                            { 
                                                if($val->parentid==$value->id)
                                                {
                                                    echo '<p style="margin-left: 30px;"><input type="checkbox" value="'.$value->id.'#'.$val->id.'" class="submenu'.$value->id.'" name="menu[]"> '.$val->menuname.'</p>'; 
                                                } 
                                            }
                                        } 
                                    } 
                                    ?>
                                                        
                                            </div>
                                            
                                            <div class="col-lg-6" id="assignmenu"></div>

                                     

                                    </div>    
                                       <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="rolesubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                             </div> 
                                    </form>
                                   
                                    </div>

                                </div>


                            </div>
                        </div>

                        
                    </div>
                   

                    </div>
              
   <style type="text/css">
  
.he{background: beige;}
.fo{color: green;}
   </style>

   <script type="text/javascript">
    function getemp(){
    var emptypeid=$('#emptypeid').val();
    $('#menu').hide();
     $.ajax({
        url: '<?php echo base_url()?>admin/rolemngmnt/getemp',
        type: 'POST',
        data: {'emptypeid': emptypeid},
        success: function(data){
           var  option_brand = '<option value="">Select Employee</option>';
           $('#empid').empty();
           $("#empid").append(option_brand+data);
           
       }
   });

 }

 function getmenu(){
    var empid=$('#empid').val();
    if(empid=='')
    {
        $('#menu').hide();
    }
    else
    {
        $('#menu').show();

        $.ajax({
        url: '<?php echo base_url()?>admin/rolemngmnt/getassignmenu',
        type: 'POST',
        data: {'empid': empid},
        success: function(data){

           $("#assignmenu").empty();
           $("#assignmenu").append('<p class="he">Assigned Menu</p>'+data);
           
       }
   });

    }
 }

function allmenu(ids){

//alert($('#menu'+ids).prop('checked'));
    
    if($('#menu'+ids).prop('checked')==true)
    {
        $('.submenu'+ids).prop('checked', true);
    }
    else
    {
        $('.submenu'+ids).prop('checked', false);
    }
 }

function removemenu(ids)
{
    var empid=$('#empid').val();
    var r = confirm("Confirmed remove this menu!");
    if (r == true) 
    {
        $.ajax({
        url: '<?php echo base_url()?>admin/rolemngmnt/removeassignmenu',
        type: 'POST',
        data: {'empid': empid,'menuid': ids},
        success: function(data)
        {
            $('#'+ids).prop('checked', false);
            $('#c'+ids).remove();
            $('#r'+ids).remove();
            $('#m'+ids).html('Successfully remove this menu!');
            setTimeout(function(){ $('#m'+ids).remove(); }, 3000);
            
        }
        });  
    } 
    else 
    {
        return false;
    }

 }
 

</script>