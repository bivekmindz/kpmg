<!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>


     <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('#example').DataTable();
                                    } );
                                </script> 


    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>View User</h2>
                        </div>
                     
                        <div class="page_box">
                           <!--  <form method="post" action="">
<input style="background:#ac7cb7;border: none;color: #fff;padding: 5px 10px;font-size: 13px; float:right;  margin-bottom: 10px;" type="submit" value="Download Excel" name="excel">
</form>  -->
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>User ID</th>
                                                <th bgcolor='red'>Name</th>
                                                <th bgcolor='red'>Email ID</th>
                                                <th bgcolor='red'>Contact Number</th>
                                             
                                                <th bgcolor='red'>Status</th>
                                                <th bgcolor='red'>Date of Joining</th>
                                                
                                                                                           
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                            foreach ($vieww as $value) {
                                            $i++;
                                            
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->user_uid; ?></td>
                                            <td><?php echo $value->t_FName; ?></td>
                                            <td><?php echo $value->user_email; ?></td>
                                            <td>+<?php echo $value->ccode.'-'.$value->user_mobile; ?></td> 
                                        
                                            <td><?php if($value->user_otp_status==1) 
                                                echo '<span style="color:green">Varified</span>'; 
                                                else echo '<span style="color:red">Varify</span>';
                                            ?></td>
                                            <td><?php echo date('d-m-Y h:i:s A',strtotime($value->user_createdon)); ?></td>
                                            

                                            
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>