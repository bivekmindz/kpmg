<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
<!DOCTYPE html>

<html>

<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
            
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Employee List</h2>
                        </div>
                       
                                   <p style="color:green"><?php  echo $this->session->flashdata('message'); ?></p>
                          
           
               <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('#example').DataTable();
                                    $( ".grid_tbl" ).wrap( "<div class='new_width'></div>" );
                                    } );
                                </script>           
                        
                        
                
                            
      
 
        <div class="page_box" id="storelistingdiv">
    
             
          



             <table class="grid_tbl" id="example">
                                    <thead>
                                        <tr>
                                            <th bgcolor="red">S.No.</th>
                                               
                                                <th bgcolor="red">Employee Type</th>
                                                <th bgcolor="red">Employee ID</th>
                                                <th bgcolor="red">Employee Name</th>
                                                <th bgcolor='red'>Contact No.</th>
                                                <th bgcolor="red">Email/Login ID</th>
                                                <th bgcolor="red">Country</th>
                                                <th bgcolor="red">State/City</th>
                                                <th bgcolor="red">Address</th>
                                                <th bgcolor="red">Status</th>
                                               
                                        </tr>
                                    </thead>
                                    
                                    
                                    <tbody class="append"> 
                                    <?php 
                                        $i = 1;
                                            foreach ($list as $key => $value) { ?>
                                            <tr>


                                        <td><?php echo $i; ?></td>
                                        <td> <?php
                                      $parameter = array(
                                      'act_mode' => 'getemptypeset',
                                      'row_id'=>$value->s_merchantid,
                                      'counname'=>'',
                                      'coucode'=>'',
                                      'commid'=>''
                                      );
                                       //p($parameter); exit;
                             $response = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
                             echo $response->et_name;
                                           ?></td>
                                        <td><?php echo $value->s_uniqe_qr_code; ?></td>
                                        <td><?php echo $value->s_name; ?></td> 
                                        <td><?php echo $value->s_mobile; ?></td> 
                                        <td><?php echo $value->s_email; ?></td> 
                                        <td><?php echo $value->name; ?></td>
                                        <td><?php echo $value->g_statename.', '.$value->g_cityname; ?></td> 
                                        <td><?php echo $value->s_address; ?></td> 
                                        <td>
                                         <a href="<?php echo base_url();?>admin/user/editemp/<?php echo $value->s_loginid;?>" ><i class="fa fa-pencil"></i> </a> 
                                         <a href="<?php echo base_url();?>admin/user/deleteemp/<?php echo $value->s_loginid;?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash"></i> </a> 
                                         <a href="<?php echo base_url(); ?>admin/merchant/updatestatus/<?php echo $value->s_loginid.'/'.$value->s_status; ?>/Employee" >
                                        <?php if($value->s_status==1)  echo 'Active'; else echo '<span style="color:red">Inactive</span>'; ?></a>
                                         </td>     
                                    </tr> 
                                    <?php $i++; } ?>               
                                    </tbody>
                                    </table>
                                    
                
            </div>

             
        </div>     
              
         </div>
        </div>
    </div>


</body>
 
</html>
