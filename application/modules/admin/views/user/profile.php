<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
              <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Profile</h2>
                        </div>
                        
       <div class="page_box" id="retailerdiv">
                                     <div class="sep_box">
                                      <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Name</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input"><?php echo @$vieww->s_name; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                              <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Email/Login ID</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input"><?php echo @$vieww->s_email; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                    </div>

                                   

                                         <div class="sep_box">
                                            
                                              <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Country</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <?php  foreach ($country as $key => $value) {
                                                                    if($value->conid==@$vieww->s_countryid)
                                                                        { 
                                                                            echo $value->name;
                                                                        }
                                                                    }?>
                                                           
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">State/City</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                        <?php foreach ($state as $key => $value) { 
                                                if($value->g_id==@$vieww->s_stateid)
                                                { 
                                                    echo $value->g_statename.', '.$value->g_cityname;
                                                }
                                                } ?>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                            <div class="sep_box">

                                                 <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Address</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input"><?php echo @$vieww->s_address; ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                                
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Contact Number</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input"><?php echo @$vieww->s_mobile; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          
    
                                            </div>

                        
                    </div></div></div> </div>
        </div>
    </div>
</body>
   
</html>