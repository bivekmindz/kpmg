<!DOCTYPE html>

<html>

<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <?php 
                $getid=$this->uri->segment(4);
           
     
      if(@$getid!='') {
        echo '<form action="" method="post" enctype="multipart/form-data">';
            
      }

      else
      {
            echo '<form action="" id="empForm" method="post" enctype="multipart/form-data">';
      }
      ?>
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Add Employee</h2>
                        </div>
                      <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                           
                      
        <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                     
                                     <div class="sep_box">
                                            

                                      <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Employee Type<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select id="emptypeid" name="emptypeid">
                                                            <option value="">Select Employee Type</option>
                                        <?php foreach ($emp as $key => $value) { ?>

                                    <option value="<?php echo $value->et_id.'#'.$value->et_name; ?>">
                                                             <?php echo $value->et_name; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Employee Name <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="s_username" id="s_username" value="<?php echo @$vieww->s_name; ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             
                                    </div>

                                    <div class="sep_box">

                                                 <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Email/Login ID <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input id="s_loginemail" type="text" name="s_loginemail" value="<?php echo @$vieww->s_email; ?>" <?php if(@$vieww->s_email!=''){ echo 'disabled';} ?>/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Contact Number <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="s_mobileno" id="s_mobileno" value="<?php echo @$vieww->s_mobile; ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             </div>

                                         <div class="sep_box">
                                            
                                              <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select Country <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <?php if(@$vieww->s_email!=''){ 

                                                                foreach ($country as $key => $value) {
                                                                    if($value->conid==@$vieww->s_countryid)
                                                                        { 
                                                                            echo $value->name;
                                                                        }
                                                                    }

                                                                echo '<input type="hidden" name="countryid" id="countryid" value="'.$vieww->s_countryid.'"/>';
                                                            }else{ ?>
                                                           <select id="countryid" onchange="selectstate()" name="countryid" >
                                                            <option value="">Select Country</option>
                                                            <?php foreach ($country as $key => $value) { ?>

                                                            <option <?php if($value->conid==@$vieww->s_countryid){ echo "selected"; } ?>  value="<?php echo $value->conid; ?>">
                                                             <?php echo $value->name; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                              <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select State/City <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select id="stateid" name="stateid">
                                                            <option value="">Select State/City</option>
                                        <?php foreach ($state as $key => $value) { ?>

                                    <option <?php if($value->g_countryid==@$vieww->s_stateid){ echo "selected"; } ?>  value="<?php echo $value->g_countryid; ?>">
                                                             <?php echo $value->g_statename.', '.$value->g_cityname; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                            <div class="sep_box">

                                                 <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Address<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="companyaddress"  id="companyaddress"><?php echo @$vieww->s_address; ?></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             <?php if(@$vieww->s_email==''){ ?>

                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Password <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                     
                                                           <input type="text" name="s_loginpassword" id="s_loginpassword" autocomplete="off"  />
                                                           <p id="msgpwd"></p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php } ?>    
                                            </div>
                                             

<div class="sep_box processingimg" style="display:none">
    <div class="col-lg-6">
    <div class="row">
        <div class="col-lg-2">
            <div class="tbl_text"></div>
        </div>
        <div class="col-lg-4">
            <div class="tbl_input">
                <img src="<?php echo base_url(); ?>loader.gif">

            </div>
        </div>
        </div>
    </div>
</div>

                                            <div id="vendor_msg"></div>
                                             <div class="sep_box">
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="empsubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  






                                        </div>  
                                                                    

                                    </div>

                                </div>


                            </div>
                        </div>
                        </form>
                        
                    </div>


                    
               
                                        </div>
                                        
                                     
 
                                          
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>





<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>



<script type="text/javascript">
  $(document).ready(function(){
    $("#empForm").validate({
          // Specify the validation rules
          rules: {
              emptypeid: "required",
              s_username : "required",
              countryid : "required",
              stateid : "required",
              s_loginemail : {required:true, email:true},
              s_mobileno :  {required:true, number:true},
              s_loginpassword : "required"

          },
          
          messages: {
            
              emptypeid : "Please select Employee Type!",
              s_username :  "please enter name!" ,
              countryid : "please Select Country!",
              stateid : "please Select State!",
              s_loginemail : {required:"Please enter email ID!", email:"Please enter valid email ID!"},
              s_mobileno : {required:"Please enter contact number!", number:"Numbers only!"},
              s_loginpassword : "Please enter password!"
              
          },
          
          submitHandler: function(form) {
            var formData = new FormData($("#empForm")[0]);
            $.ajax({
            url: '<?php echo base_url(); ?>admin/user/insertemployee',
            type: 'POST',
            cache: false,
            data: formData,
            async: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            $(".processingimg").css("display","block");
            $("#vendor_msg").html("Saving, please wait....");
            $('#vendor_msg').css('color','green');
            },
            success: function(data){ 
                //alert(data);return false;
                if(data==0){
                    $('#vendor_msg').html("Email id already exist!");
                    $('#vendor_msg').css('color','red');
                    $(".processingimg").css("display","none");
                } else {
                    $('#vendor_msg').html("Successfully Saved.");
                    $('#vendor_msg').css('color','green');
                    $(".processingimg").css("display","none");
                    $("#empForm")[0].reset();
                   
                } 
            }
            });
          }
      });
   });




 function selectstate(){
   var countryid=$('#countryid').val();
   
     $.ajax({
        url: '<?php echo base_url()?>admin/merchant/getcountrydata',
        type: 'POST',
        //dataType: 'json',
        data: {'countryid': countryid},
        success: function(data){
      //alert(data); 

      var  option_brand = '<option value="">Select State/City</option>';
      $('#stateid').empty();
      $("#stateid").append(option_brand+data);

  }
});

 }

 </script>
