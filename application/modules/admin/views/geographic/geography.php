<div class="col-lg-10 col-lg-push-2">
            <div class="row">
         
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Add Geography</h2>
                        </div>
                        <?php echo validation_errors(); ?> 
                                  <?php  echo $this->session->flashdata('message'); ?>

                        <div class="page_box">
                            <div class="sep_box">

                                <div class="col-lg-12">
                                 <form action="" method="POST">
                                    <div class="row">

                                        <div class="sep_box">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-1">
                                                        <div class="tbl_text">City/State<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
            <input  type="text" name="address" id="autocomplete" placeholder="Enter your address" onFocus="geolocate()" required/>
                                                              <span class="couname_status"></span>
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>

                                             </div>

                                              <div class="sep_box">
                                                   
                                             <div class="col-lg-3">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">City</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                              <input type="text" name="city" id="locality" readonly/>
                                                              
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="col-lg-3">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">State</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                              <input type="text" name="state" id="administrative_area_level_1" readonly/>
                                                              
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Country</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="text" name="country" id="country" readonly/>
                                                           <input type="hidden" name="countrycode" id="countrycode" readonly/>
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                              <div class="sep_box">
                                             <div class="col-lg-3">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Code(+)</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="text" name="code" id="code" readonly/>
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Currency</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="text" name="currency" id="currency" readonly/>
                                                        </div>

                                                        
                                                    </div>
                                                </div>
                                            </div>
                                           

                                            <div class="col-lg-2">
                                                     <input id="submitBtn" type="submit" name="submit" value="Submit" class="retailer_btn" style="display:none"/>
                                            </div>
                                            
                                          
                                        </div>

                                            
                                          
                                        </div>
                                            </form>
                                     
                                    </div>

                                </div>
                            





                            </div>

                         

                                



                            </div>



                        
                    </div>
                </div>
            </div>
        </div>

    <script>
     
      var placeSearch, autocomplete;
      /*var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };*/

       var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
            
          }
        }

        /*-------------------------------------*/
        //alert(val);

$.ajax({        
    url: "<?php echo base_url(); ?>admin/geographic/getdetails",
    //url: "https://restcountries.eu/rest/v2/name/"+val,
    data: {'country':val},
    type: 'POST',
    success: function (resp) {
       //console.log(resp);
        var v=resp.split('##');
        $('#code').val(v[0]);
        $('#currency').val(v[1]);
        $('#countrycode').val(v[2]);
        $('#submitBtn').css('display','block');
        //$('#submitBtn').removeAttr('disabled');
    },
    error: function(e){
        alert('Error: '+e);
    }  
});
        /*------------------------------------------*/
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"
        async defer></script>

