


<div class="col-lg-10 col-lg-push-2">
            <div class="row">
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Add State</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, admin can add add state.
                           
                                  <?php echo validation_errors(); ?> 
                                  <?php  echo $this->session->flashdata('message'); ?>
<a href="<?php echo base_url('admin/geographic/statelist'); ?>" style="float:right;"><button>View State List</button></a></p>
                            </div>
                        </div>
                        <div class="page_box">
                            <div class="sep_box">

                                <div class="col-lg-12">
                                 <form action="" method="POST">
                                    <div class="row">
                                        <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select Country<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select name="countryid" id="countryid" required>
                                                               <option value="">Select  Country</option>
                                                              <?php foreach ($viewcountry as $value){  ?>
                                                                 <option value="<?php echo $value->conid; ?>"><?php echo $value->name.' (+'.$value->code.')'; ?></option>                  
                                                              <?php } ?>
                                                           </select> 
                                                        </div>

                                                      
                                                  

                                                    </div>


                                                </div>
                                            </div>
                                            
                                          
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select State <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                        <input type="text" id="statename" name="statename" required />
                                                        <span class="statename_status">
                                                        </span>
                                                        </div>

                                                      
                                                  

                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                        
                                     
                                    </div>


                                    <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4"></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           
                                                        </div>

                                                      
                                                  

                                                    </div>


                                                </div>
                                            </div>
                                            
                                          
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text"></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                         <input id="Submit1" name="submit" type="submit" value="Submit" class="btn_button sub_btn" onclick="return validate16();" />
                                                        </div>

                                                      
                                                  

                                                    </div>


                                                </div>
                                            </div>

                                          
                                        </div>
                                        
                                     
                                    </div>


                            </form>

                                </div>
                                





                            </div>

                            

                                



                            </div>



</div>
                        
                    </div>
                </div>
            </div>
        </div>

<script type="text/javascript">
   
    $(document).ready(function() {
        $("#statename").on("change", function() {
            var name=$('#statename').val();
                countryid = $("#countryid").val();
           
            $.ajax({
                url: '<?php echo base_url()?>admin/geographic/checkstate',
                type: 'POST',
                dataType: 'json',
                data: {'name': name,'countryid':countryid},
                success: function(data){
                    
                    if(data.statecount > 0){
                      $('#submitBtn').prop('disabled', 'disabled');
                      $('.statename_status').html("Data Already Existed");
                    } else {
                      $('#submitBtn').removeAttr('disabled');
                      $('.statename_status').html("");
                    }      
                }
            });
    
        });
    });
    
  
</script>

   