<script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            stateid : "required",
            cityid : "required",
            countryid : "required",
            streetname : "required"
        },
        // Specify the validation error messages
        messages: {
            stateid: "State name required",
            cityid: "State code required",
            countryid: "Country Id required",
            streetname: "Street name is required"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>


<div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <!-- <form action="" method="post" enctype="multipart/form-data" > -->
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Add Location</h2>
                        </div>
                        <div class="page_box">
                            <div class="col-lg-12">
                                <p> In this section, admin can add location!<a href="<?php echo base_url('admin/geographic/viewlocation'); ?>" style="float:right;"><button>View Location List</button></a></p>
                           
                                  <?php echo validation_errors(); ?> 
                                  <?php  echo $this->session->flashdata('message'); ?>

                            </div>
                        </div>
                        <div class="page_box">
                            <div class="sep_box">

                                <div class="col-lg-12">
                                 <form action="" method="POST" id="addCont">
                                    <div class="row">
                                        <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Country<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select name="countryid" id="countryid" onchange="selectstates();" required>
                <option value="">-- Select Country --</option>
                <?php foreach ($country as $key => $value) { ?>
                    <option value="<?php echo $value->conid; ?>"><?php echo $value->name; ?></option>
                <?php } ?>
                </select>
                                                        </div>

                                                      
                                                  

                                                    </div>


                                                </div>
                                            </div>
                                            
                                          
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">State<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <select id="stateid" name="stateid" value="stateid" onchange="selectcities();" required>
                                                             <option value="">-- Select State --</option>
                                                             </select>
                                                        </div>

                                                      
                                                  

                                                    </div>


                                                </div>
                                            </div>






                                        </div>
                                        
                                     <div class="sep_box">
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">City <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <select id="cityid" name="cityid" required>
                                                             <option value="">-- Select City --</option>
                                                            </select>
                                                        </div>
                                                    </div>                                                    
                                                  

                                                    </div>


                                                </div>

                                            
                                          
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Location <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <input  type="text" name="streetname"  id="streetname" required onblur="checkname();" />
                                                          <span class="streetname_status"></span>
                                                        </div>                                                     
                                                  

                                                    </div>


                                                </div>
                                            </div>


                                        </div>

                                        <div class="sep_box">                                           
                                          
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="tbl_input">
                                                        <input id="submitBtn" type="submit" name="submit" value="Submit" class="btn_button sub_btn" />
                                                        </div>

                                                      
                                                  

                                                    </div>


                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    </div>


                                            
                                          
                                            

                                          
                                        </div>
                                        
                                     
                                    </div>




                                </div>
                                </form>

                                


                        </div>

                        
                    </div>
                </div>
            </div>
        </div>

<script type="text/javascript">
function checkname(){
    var name=$('#streetname').val();
    $('#submitBtn').prop('disabled', 'disabled');
    $.ajax({
    url: '<?php echo base_url()?>admin/geographic/checklocname',
    type: 'POST',
    dataType: 'json',
    data: {'name': name},
    success: function(data) {
        if(data[0].count > 0){
            $('#submitBtn').prop('disabled', 'disabled');
            $(".streetname_status").html("Data Already Existed");
        } else {
            $('#submitBtn').removeAttr('disabled');
            $(".streetname_status").html("");
        }      
    }
    });
}

function selectstates(){
    var countryid=$('#countryid').val();
    $.ajax({
    url: '<?php echo base_url()?>admin/merchant/getstate',
    type: 'POST',
    //dataType: 'json',
    data: {'countryid': countryid},
    success: function(data){
    var  option_brand = '<option value="">Select State</option>';
    $('#stateid').empty();
    $("#stateid").append(option_brand+data);
    }
    });
}

function selectcities(){
    var stateid=$('#stateid').val();
    $.ajax({
    url: '<?php echo base_url()?>admin/merchant/getcity',
    type: 'POST',
    data: {'stateid': stateid},
    success: function(data){
    var  option_brand = '<option value="">Select City</option>';
    $('#cityid').empty();
    $("#cityid").append(option_brand+data);
    }
    });
}
</script>