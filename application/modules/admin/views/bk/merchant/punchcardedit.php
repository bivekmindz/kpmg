<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); 
    $getid=$this->uri->segment(4);
?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Punch Card Details</h2>
                       </div>
                                    <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            
   <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" id="punch" method="post" enctype="multipart/form-data">
                                     <div class="sep_box">
                                            
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Category <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                             <?php 
                                                foreach ($category as $key => $value) 
                                                { 
                                                  if($value->cat_id==$view->p_catid)
                                                  {
                                                    echo $value->cat_name; 
                                                    echo '<input type="hidden" name="category" value="'.$value->cat_id.'"/>'; 
                                                  }
                                                }
                                               ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Product Type<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          
                                                            <input type="text" class="product" name="of_pr_type" value="<?php echo $view->p_of_pr_type; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>

                                    <div class="sep_box">
                                            
                                        <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Paid Count<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                        <select id="pcount" name="oqty_pcount">
                                                             <?php for($i=1; $i<=20; $i++ ){ ?>
                                                            <option <?php if($view->p_oqty_pcount==$i) echo 'selected'; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Free Count <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select id="fcount" name="oday_fcount">
                                                             <?php for($i=1; $i<=20; $i++ ){ ?>
                                                            <option <?php if($view->p_oday_fcount==$i) echo 'selected'; ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             

                                        </div> 
                                        
                                            <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Start Date<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                               <input type="text" name="sdate" class="sdate" value="<?php echo $view->p_sdate; ?>" readonly/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">End Date<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                             <input type="text" class="edate" name="edate" value="<?php echo $view->p_edate; ?>" readonly/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="sep_box">

                                                <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Use Image</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="radio" name="imgaccess" value="1" <?php if($view->p_odayvalue_earnc==1) echo 'checked';?>/> Card
                                                            <input type="radio" name="imgaccess" value="2" <?php if($view->p_odayvalue_earnc==2) echo 'checked';?>/> Punch
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Punch Image</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <input type="file" name="oimg" id="oimg" />
                                                    <img src="<?php echo base_url().'public/offer/'.$view->p_oimg_burnp; ?>" alt="doc" width="50px;" height="50px;">
                                                    <input type="hidden" name="oimg1" value="<?php echo $view->p_oimg_burnp; ?>"/>
                                                            <span id="msg"> 
                                                                
                                                            </span>

                                                        </div>
                                                    </div>                                      
                                                </div>
                                            </div>
                                        </div>


                                             <div class="sep_box">
                                               <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">No. of Punch per User<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <select name="noofuser" id="noofpunch" >
                                                            <?php for($i=0; $i<=10; $i++ ){ ?>
                                                                <option <?php if($view->p_nouser==$i) echo 'selected'; ?> value="<?php echo $i; ?>">
                                                        <?php if($i==0) echo 'Unlimited';else echo $i;?> </option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">How it Works?<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="how"  id="how"><?php echo $view->p_how; ?></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
    
                                            </div>

                                             <div class="sep_box">
                                               <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Terms of Use<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <textarea name="term"  id="term"><?php echo $view->p_term; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">FAQs<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="faq"  id="faq"><?php echo $view->p_faq; ?></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         
    
                                            </div>

                                             <div class="sep_box">
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="program" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div>
                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>





<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />

<script type="text/javascript">
   

    $('.sdate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });
   
    $('.edate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });


    


</script>   



<script type="text/javascript">

  $(document).ready(function(){
    $("#punch").validate({
          // Specify the validation rules
          rules: {
              category: "required",
              of_pr_type:"required",
              pcount : "required",
              fcount : "required",
              sdate: "required",
              edate : "required",
              how : "required",
              term : "required",
              faq : "required"

          },
          
          messages: {
            
              category : "Please select category!",
              product : "Please select product!" ,
              pcount : "Please select paid count!",
              fcount : "Please select free count!",
              sdate : "please select start date!",
              edate : "please select end date!",
              how : "please enter how it works!",
              term : "please enter term of use!",
              faq : "please enter FAQs!"
              
          },
          
         
      });
   });

</script>


 <script>
                $(document).ready(function (){ 
                $("#oimg").change(function () { 
                  var val = $(this).val().toLowerCase(); 
                  var regex = new RegExp("(.*?)\.(jpeg|png|jpg)$"); 
                  if(!(regex.test(val))) { 
                  $(this).val(""); 
                  $("#msg").html("Unsupported file, only jpeg|png|jpg files are allowed!").css("color","red");
                  }
                }); 
                });
  </script>