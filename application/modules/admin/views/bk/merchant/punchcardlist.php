<link rel="stylesheet" href="<?php echo base_url();?>assets/css/viewer.css">
<script src="<?php echo base_url();?>assets/js/viewer.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script>
<style type="text/css">

    .alert-close {
    background: rgba(255,255,255,0.1);
    
    color: #000000;
    cursor: pointer;
    float: right;
    font-size: 25px;
  
}
    </style>
    <style type="text/css">
.deal_detail{
    float:left;
    width:100%;
    height:auto;
    background:#f1f1f1;
    padding:10px 10px;
}
.deal_left{
    float:left;
    width:25%;
}

.deal_right{
    float:left;
    width: 75%;
    padding: 0px 20px;
}
.deal_row{
    float:left;
    width:100%;
    padding:7px 0px;
}
.deal_d2
{
    float:left;
    width:50%;
    padding:0px 10px;
}
.deal_d1
{
    float:left;
    width:100%;
    padding:0px 10px;
}
.deal_text
{
    float:left;
    width:100%;
    font-weight:600;
}
.deal_text_b
{
    float:left;
    width:100%;
    font-weight:400;
    padding:8px 0;
}
.deal_text span{
    font-weight:400!important;
    margin-left:15px;
}

 </style>
    
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
    
    <script type="text/javascript">
    $(document).ready(function(c) {
        $('.alert-close').on('click', function(c){
            $(this).parent().fadeOut('slow', function(c){
                //location.reload();
            });
        }); 
    });
    </script>
    <script type="text/javascript">
  
  function showdealdetailstr(id)
  {
    $('.dealdetailstr').closest('tr').addClass('str');
    $('#sho_'+id).closest('tr').removeClass('str');
  }

</script>

<!DOCTYPE html>

<html>

<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); 
    $logintype=$this->session->userdata('lw_login')->s_logintype;
    if($logintype==3)
    {
        $id=$this->session->userdata('lw_login')->s_merchantid;
    }
    else
    {
        $id=$this->uri->segment(5);
    }   ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
            
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Punch Card List</h2>
                            <?php if($this->uri->segment(3)!='unpublishprogram'){ $a='access';?>
                        <a href="<?php echo base_url('admin/program/punch').'/'.$id;?>"><button type="button" style="float:right;">Punch Card Add</button></a><?php } ?></div>
                                   <p style="color:green"><?php  echo $this->session->flashdata('message'); ?></p>
                            
                      
               
             
 
        <div class="page_box" id="storelistingdiv">
    
             
          



             <table class="grid_tbl" id="example">
                                    <thead>
                                        <tr>
                                            <th bgcolor="red">S.No.</th>
                                            <th bgcolor="red">Merchant ID</th>  
                                            <th bgcolor="red">Category</th>  
                                            <th bgcolor="red">Product Name</th>
                                            <th bgcolor="red">Paid Count</th>
                                            <th bgcolor="red">Free Count</th>
                                            <th bgcolor='red'>No. Of Punch</th>
                                            <th bgcolor="red">Start Date</th>
                                            <th bgcolor="red">End Date</th> 
                                            <th bgcolor="red">Create Date/Time</th>
                                            <th bgcolor="red">Action</th>
                                            <th bgcolor="red">Status</th>
                                               
                                        </tr>
                                    </thead>
                                    
                                    
                                    <tbody class="append"> 
                                    <?php 
                                        $ik=0;
                                        if(!empty($programlist))
                                        {
                                            if($_GET['page'])
                                            {
                                              $page = $_GET['page']-1; 
                                              $ik=$page*10; 
                                            }
                                        } ?>

                                        <?php $i=$ik;#1;
                                      foreach ($programlist as $key => $value) { $i++;?>
                                      <tr class="append_wrapper">
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value->s_uniqe_qr_code; ?></td>
                                        <td><a href="javascript:void(0)" onclick="showdealdetailstr('<?php echo $i; ?>')"><?php echo $value->cat_name; ?></a></td>
                                        <td><?php echo $value->p_of_pr_type;//$value->p_name; ?></td>
                                        <td><?php echo $value->p_oqty_pcount; ?></td>
                                        <td><?php echo $value->p_oday_fcount; ?></td>
                                        <td><?php if($value->p_nouser==0) echo 'Unlimited';else echo $value->p_nouser; ?></td>
                                       
                                        <td><?php echo $value->p_sdate; ?></td> 
                                        <td><?php echo $value->p_edate; ?></td> 
                                        <td><?php echo $value->p_createon; ?></td>
                                        <td><?php if($logintype==2 or $logintype==3){ 
                                            if($value->p_status==1)  echo 'Active'; else echo '<span style="color:red">Inactive</span>'; }else { 
                                                $id1=$value->p_mrid;
                                if($value->p_status==1){ echo '<span style="color:green">Active</span>';}else{ ?>
                                            <a href="<?php echo base_url(); ?>admin/program/punchofferstatus/<?php echo $value->p_id.'/'.$value->p_status.'/'.$id1.'/PC'.'/'.$value->p_catid.'/'.$a; ?>" >
                                        <?php if($value->p_status==1)  echo 'Active'; else echo '<span style="color:red">Inactive</span>'; ?>
                                        </a><?php }} ?> </td>
                                        <td>
                                        <?php if($value->p_status==0){ ?>
                                            <a href="<?php echo base_url();?>admin/program/editprogram/<?php echo $value->p_id.'/'.$value->p_mrid;?>/PC" ><i class="fa fa-pencil"></i> </a> 
                                           <span style="color:red">Unpublish</span>
                                        <?php }
                                        else
                                        {
                                            if(strtotime($value->p_edate) < strtotime(date('d-m-Y'))){ echo '<span style="color:red">Expired</span>';}else { echo '<span style="color:green">Published</span>'; }
                                        } ?>
                                        
                                         </td>
                                       
                                        
                                         
                                    </tr> 

                                    <style>.str{display:none;}</style>
                                        <div id="showdiv">

                                          
                                        <tr class="dealdetailstr str"  id="sho_<?php echo $i; ?>">

                                            <td colspan="12">
                                                 <!-- <div class="alert-close">×</div> -->
                                                <div class="deal_detail" id="deal_detail" >
                                                    <div class="deal_left">
                                                    <?php if(!empty($value->p_oimg_burnp)){ ?>    
                                                    <ul class="docs-pictures clearfix">
                                                      <li><img data-original="<?php echo base_url().'public/offer/'.$value->p_oimg_burnp; ?>" src="<?php echo base_url().'public/offer/'.$value->p_oimg_burnp; ?>" alt="Punch" width="200px;" height="250px;"></li>
                                                    </ul>
                                                    <?php } ?>
                                                    </div>
                                                    
                                                    <div class="deal_right">
                                                         <div class="deal_row">
                                                            
                                                            <div class="deal_d2"><div class="deal_text">Use Image : <span><?php if($value->p_odayvalue_earnc==1) echo 'Card'; else echo 'Punch'; ?></span></div></div>
                                                        </div>
                                                         <div class="deal_row">
                                                            <div class="deal_d1"> 
                                                            <div class="deal_text">How it Works?</div>
                                                            <div class="deal_text_b">
                                                            <?php echo $value->p_how; ?>
                                                            </div></div>
                                                        </div>
                                                        <div class="deal_row">
                                                            <div class="deal_d1"> 
                                                            <div class="deal_text">Terms of Use</div>
                                                            <div class="deal_text_b">
                                                            <?php echo $value->p_term; ?>
                                                            </div></div>
                                                        </div>
                                                        <div class="deal_row">
                                                            <div class="deal_d1"> 
                                                            <div class="deal_text">FAQs</div>
                                                            <div class="deal_text_b">
                                                            <?php echo $value->p_faq; ?>
                                                            </div></div>
                                                        </div>
                                                        <div class="deal_row">
                                                            
                                                            <div class="deal_d2"><div class="deal_text">Modified Date/Time : <span><?php echo $value->p_modifiedon; ?></span></div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            
                                        </tr>
                                    </div>

                                    <?php #$i++; 
                                } ?>               
                                    </tbody> 
                                    </table>
                                 <div class="pagi_nation">
                                  <ul class="pagination">
                                    <?php foreach ($links as $link) {
                                    //p($link); //exit();
                                    echo "<li class='newli'>". $link."</li>";
                                    } ?>
                                  </ul>
                                </div>   
                
            </div>

             
        </div>     
              
         </div>
        </div>
    </div>


</body>
 
</html>