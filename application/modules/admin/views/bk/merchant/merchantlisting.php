<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
 <link rel="stylesheet" href="<?php echo base_url();?>assets/css/viewer.css">
   <script src="<?php echo base_url();?>assets/js/viewer.js"></script>
  <script src="<?php echo base_url();?>assets/js/main.js"></script>

<!DOCTYPE html>

<html>

<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
            
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Merchant List</h2>
                       <?php 
                                  if($this->session->userdata('lw_login')->s_loginid==1){
                                  echo '<a href="'.base_url().'admin/merchant/addmerchant" style="float:right;margin-left: 10px;"><button type="button">ADD Merchant</button></a>';
                                    }
                                   ?>  </div>
                                   <p style="color:green"><?php  echo $this->session->flashdata('message'); ?></p>
                          
                           <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('#example').DataTable();
                                    $( ".grid_tbl" ).wrap( "<div class='new_width'></div>" );
                                    } );
                                </script>           
                        
                        
                
                            
      
 
        <div class="page_box" id="storelistingdiv">
    
             
          



             <table class="grid_tbl" id="example">
                                    <thead>
                                        <tr>
                                            <th bgcolor="red">S.No.</th>
                                               
                                                <th bgcolor="red">Picture</th>
                                                <th bgcolor="red">Enrollment Date</th>
                                                <th bgcolor="red">Merchant ID</th>
                                                <th bgcolor="red">Merchant Name</th>
                                                <th bgcolor="red">Company Name</th>
                                                <th bgcolor='red'>Contact No.</th>
                                                <th bgcolor="red">Email/Login ID</th>
                                                <th bgcolor="red">Manager</th> 
                                                <?php #if($this->session->userdata('lw_login')->s_logintype==1){ ?>
                                                <th bgcolor="red">Payment Type</th><?php #}?>
                                                <th bgcolor="red">Status</th>
                                               
                                        </tr>
                                    </thead>
                                    
                                    
                                    <tbody class="append"> 
                                    <?php 
                                        $i = 1;
                                            foreach ($list as $key => $value) { ?>
                                            <tr class="append_wrapper">


                                        <td><?php echo $i; ?></td>
                                        <td> <ul class="docs-pictures clearfix">
            <li><img data-original="<?php echo base_url().'public/merchant/'.$value->s_picture; ?>" src="<?php echo base_url().'public/merchant/'.$value->s_picture; ?>" alt="Merchant" width="50px;" height="50px;">
            </li>
            
          
          </ul>
                                         
                                        </td>
                                        <td><?php if($value->s_agreementdate==''){echo '';}else{ echo date("d-m-Y", strtotime($value->s_agreementdate));} ?></td>
                                        <td><a href=""><?php echo $value->s_uniqe_qr_code; ?></a></td>
                                        <td><?php echo $value->s_username; ?></td> 
                                        <td><?php echo $value->s_name; ?></td> 
                                         <td><?php echo $value->s_mobile; ?></td> 
                                        <td><?php echo $value->s_email; ?></td> 
                                        
                                         <td>
                                     <?php
                                      $parameter = array(
                                      'act_mode' => 'getleadmngr',
                                      'row_id'=>$value->s_msids,
                                      'counname'=>'',
                                      'coucode'=>'',
                                      'commid'=>''
                                      );
                                       //p($parameter); exit;
                             $response = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
                             echo $response->s_name.' ('.$response->code.')';
                                           ?></td> 
                                         
                                        <td>
                                        <?php echo $value->s_paytype; ?>
                                        <td>
                                        
                                        
                                            
                                        
                                    <a href="<?php echo base_url(); ?>admin/merchant/updatestatus/<?php echo $value->s_loginid.'/'.$value->s_status; ?>/M" >
                                        <?php if($value->s_status==1)  echo 'Active'; else echo '<span style="color:red">Inactive</span>'; ?>
                                    </a> |
                                        <a href="<?php echo base_url();?>admin/merchant/merchantoutletlist/<?php echo $value->s_loginid;?>">Outlets</a> |
                                        <a href="<?php echo base_url();?>admin/merchant/addmerchant/<?php echo $value->s_loginid;?>" ><i class="fa fa-pencil"></i> </a> 
                                         

                                         </td>
                                       
                                        
                                         
                                    </tr> 
                                    <?php $i++; } ?>               
                                    </tbody>
                                    </table>
                                    
                
            </div>

             
        </div>     
              
         </div>
        </div>
    </div>


</body>
 
</html>
