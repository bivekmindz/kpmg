<?php $id=decrypt($this->uri->segment(2)); ?>
<!DOCTYPE html>
<html>
<body>
    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Set Transaction Pin</h2>
                        </div>
                            <form action="" id="mgrForm" method="post" enctype="multipart/form-data" >
                            <div class="page_box"> 
                            <?php if($this->session->flashdata('message')!='') { ?>
                            <p class="msg"><?php echo $this->session->flashdata('message'); ?> </p><?php }?>                     
                                        <div class="sep_box">
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text"><?php if($id=='fdjhfs3rt363t782ewf7') echo 'Old'; else echo 'Merchant';?> Pin <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="password" name="oldpass" id="oldpass" placeholder="1111"/>
                                                <?php if($id=='fdjhfs3rt363t782ewf7'){?>
                                                <a href="javascript:void(0)" onclick="resetpin()">Reset PIN?</a><p id="reset"></p>
                                                <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        
                                        <div class="sep_box">                                        
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">New Pin<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            
                                                            <input type="password" name="newpass" id="newpass"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sep_box">                                        
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Confirm Pin </div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="password" name="cpass" id="cpass" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input  type="submit" name="resetpin" onclick="return Validate()" value="Submit" id="submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>                          
                            </div>
                            </form>                                  
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
   
</html>

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$("#mgrForm").validate({
      rules: {
        oldpass: "required",
        newpass: {required: true,maxlength: 4,minlength: 4,number: true},
        cpass:"required"
        
      },
      messages: {
        oldpass: "This field is required!",
        newpass: {required : "This field is required!", maxlength : "Max 4 digits!", minlength : "Min 4 digits!", number : "Only Numeric!"},
        cpass: "This field is required!"

      },
      submitHandler: function(form) {
        form.submit();
      }
  });
});
</script>
 <script type="text/javascript">
    function Validate() {
        var password = document.getElementById("newpass").value;
        var confirmPassword = document.getElementById("cpass").value;
        if (password != confirmPassword) {
            alert("PIN do not match.");
            return false;
        }
        return true;
    }

function resetpin()
{
    $.ajax({
        url: '<?php echo base_url();?>pin',
        type:'POST',
        data: {},
        success: function(data){
//alert(data);
           $('#reset').css('display','block');
           $('#reset').html('The link has been sent to your mail. Please check your inbox!');
           $('#reset').css('color','green');
           setTimeout(function(){ $('#reset').hide(); }, 5000);
           
       }

    });
}
</script>