<html>
<?php $rid = $this->uri->segment(4);?>  
<body>

    <div class="wrapper">
        <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
               
                    <div class="page_contant">
                        <div class="col-lg-12">
                            <div class="page_name">
                                <h2>Update Outlet Details</h2>
                            </div>
                          
                                    <p style="color:green;text-align:center;">
                                      <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?></p>
                                   

                                <style>
                                

                                .retailer_btn {
                                    outline: none;
                                    border: 0;
                                    padding: 10px 25px;
                                    background: #333;
                                    color: #FFF;
                                }   

                               
                            </style>
                         
                        
                        <form action="" id="storeForm" method="post" enctype="multipart/form-data">             


                            <div class="page_box" id="storediv" style="display:block;">

                                <div class="sep_box">
                                  <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Merchant Name<span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
<input type="hidden" name="merchantid" value="<?php echo $merchant->s_loginid; ?>"><?php echo $merchant->s_name; ?>
                                     </div>




                                 </div>


                             </div>
                         </div>



                         <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-4">
                                   <div class="tbl_text">Outlet Name <span style="color:red;font-weight: bold;">*</span></div>
                               </div>
                               <div class="col-lg-8">
                                <div class="tbl_input">
                                   <input id="r_res_name" type="text" value="<?php echo $vieww->s_name; ?>" name="s_username" />
                               </div>
                           </div>
                       </div>
                   </div>


               </div>







               <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Outlet Type <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                         <select name="operation" class="selectpicker" data-live-search="true">

                                                             <?php 
                                                             foreach ($storetype as $key => $value) {
                                                                 ?>
                                                                 <option value="<?php echo $value->store_id; ?>" <?php if($vieww->s_msids==$value->store_id) echo 'selected'; ?> ><?php echo $value->store_type; ?></option>
                                                                 <?php } ?>
                                                             </select>
                                                         </div>
                                                     </div>
                                                 </div>
                                             </div> 

    <div class="col-lg-6">
                                        <div class="row">
                                            <div class="col-lg-4">
                                              <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                          </div>
                                          <div class="col-lg-8">
                                            <div class="tbl_input">

                                               <select name="payment" class="selectpicker" data-live-search="true">
<option value="">Select Category</option>
                                                   <?php 
                                                   foreach ($category as $key => $value) {   ?>
                        <option value="<?php echo $value->cat_id ;?>" <?php if($vieww->s_payt_catid==$value->cat_id) echo 'selected'; ?> ><?php echo $value->cat_name; ?></option>
                                                       <?php } ?>
                                                   </select>


                                               </div>
                                           </div>
                                       </div>
                                   </div>



                                      </div>

                                      <div class="sep_box">


                                             <div class="col-lg-6">

                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Country <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <input type="hidden" name="countryid" value="<?php echo $merchant->s_countryid; ?>">
                                                          <?php foreach ($country as $key => $value) { 
                                                          if($value->conid==@$merchant->s_countryid){ 
                                                           echo $value->name; } } ?>
                                                           
                                                      </div>


                                                  </div>

                                              </div>
                                          </div> 
                                         <div class="col-lg-6">

                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">State/City <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <select id="stateid" onchange="selectcity()"  name="stateid" value="">
                                                              <option value="">Select State/City</option>
                                                              <?php foreach ($state as $key => $value) { ?>
                                                              <option <?php if($value->g_id==@$vieww->s_stateid){ echo "selected"; } ?> value="<?php echo $value->g_id; ?>"><?php echo $value->g_statename.', '.$value->g_cityname; ?></option>
                                                              <?php } ?>
                                                          </select>
                                                      </div>


                                                  </div>

                                              </div>
                                          </div> 



                                    


                           </div>


                           <div class="sep_box">

                           

                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="tbl_text">Address <span style="color:red;font-weight: bold;">*</span></div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="tbl_input">
                                          <textarea onblur="getlongituate()" id="address" name="companyaddress"><?php echo $vieww->s_address ; ?></textarea>  
                                          <span id="longituate"></span>
                                      </div>

                                  </div>

                              </div>
                          </div>
                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Pincode</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="text" name="pincode"  id="pincode" value="<?php echo $vieww->s_pincode ; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                             </div>

                                       <div class="sep_box">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                  <div class="tbl_text">Date of Agreement <span style="color:red;font-weight: bold;">*</span></div>
                                              </div>
                                              <div class="col-lg-8">
                                                <div class="tbl_input">

                                                   <input type="text" name="agrementsigndate" class="agrementsigndate" value="<?php echo $vieww->s_agreementdate ; ?>" />


                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                 
                                        
                       <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="tbl_text">Outlet Logo<span style="color:red;font-weight: bold;">*</span></div>
                            </div>
                            <div class="col-lg-8">
                                <div class="tbl_input">
                                    <input type="file" name="crpimgfilename" id="s_storeimg">
                                    <input type="hidden" name="fileToUploadhidden" value="<?php echo @$vieww->s_picture;  ?>">
<img height="50px;" withd="50px;" src="<?php echo  base_url(); ?>public/merchant/<?php echo @$vieww->s_picture;  ?>">
                                </div>
                            </div>

                            <span id="msg2" style="color:red"></span>
                            <span id="msg3" style="color:green"></span>
                        </div>
                    </div>



                </div>   

                <div class="sep_box">
                                             <fieldset>
                                            <legend>Bank Details</legend>
                                           <label>Copy Bank Details from Merchant. <span style="color:#ff0000"></span>:</label>
                                           <input type="checkbox" id="sameadd"  name="sameadd" onclick="sameaddress()">
                                            <div class="sep_box">
                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-lg-4">

                                                            <div class="tbl_text">
                                                               Account Holder Name
                                                               <span style="color:red;font-weight: bold;">*</span></div>
                                                           </div>
                                                           <div class="col-lg-8">
                                                            <div class="tbl_input">

                                                                <input id="accholder_name"  type="text" name="accountholder" value="<?php echo $vieww->s_achname ; ?>" />



                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Account Number<span style="color:red;font-weight: bold;">*</span></div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">




                                                                <input id="acc_number" type="text" name="accountnumber" value="<?php echo $vieww->s_acnumber ; ?>" />

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="sep_box">

                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">IFSC code<span style="color:red;font-weight: bold;">*</span></div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">
                                                                <input id="ifsc_code"  type="text" name="ifscode" value="<?php echo $vieww->s_ifsccode ; ?>" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-lg-6">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="tbl_text">Bank Name<span style="color:red;font-weight: bold;">*</span></div>
                                                        </div>
                                                        <div class="col-lg-8">
                                                            <div class="tbl_input">


                                                                <input id="bank_name"  type="text" name="bankname" value="<?php echo $vieww->s_bankname ; ?>" />



                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="sep_box">
                                              <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Branch Name<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input id="branch_name"  type="text" name="branchname" value="<?php echo $vieww->s_branchname ; ?>" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>

                                 

                                        <fieldset>
                                           <legend>Outlet Manager Details</legend>
                                           <div class="sep_box">
                                            <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                  <div class="tbl_text">Email/Login ID <span style="color:red;font-weight: bold;">*</span></div>
                                              </div>
                                              <div class="col-lg-8">
                                                <div class="tbl_input">


                                                   <input id="emailid" style="text-transform:lowercase;" type="text" name="s_loginemail" disabled value="<?php echo $vieww->s_email; ?>"/>            

                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                    <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="tbl_text">Contact Number <span style="color:red;font-weight: bold;">*</span></div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="tbl_input">
                                             <input id="managercontactno" value="<?php echo $vieww->s_mobile; ?>" type="text" 
                                             name="s_mobileno"/>
                                         </div>
                                     </div>
                                 </div>
                             </div>

                               </div>
                               <div class="sep_box">
                                <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-4">
                                      <div class="tbl_text">Manager Name<span style="color:red;font-weight: bold;">*</span></div>
                                  </div>
                                  <div class="col-lg-8">
                                    <div class="tbl_input">


                                       <input id="username"  type="text" name="owndername" value="<?php echo $vieww->s_username; ?>"/>            

                                   </div>
                               </div>
                           </div>
                       </div>

                       

                   </div>
  
         </fieldset>
          <div class="sep_box">


          <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Lattitude </div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                     <input id="lattitude"  type="text" name="lattitude" value="<?php echo $vieww->s_lat ; ?>" readonly/>
                 </div>
             </div>
         </div>
     </div>


     <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Logitude</div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input">
                 <input id="logitude"  type="text" name="logitude" value="<?php echo $vieww->s_long ; ?>" readonly/>
             </div>
         </div>
     </div>
 </div>



</div>    

<div class="sep_box">


  <div class="col-lg-12">
    <div class="row">
        <div class="col-lg-2">
            <div class="tbl_text">Terms And Condition </div>
        </div>
        <div class="col-lg-10">
            <div class="tbl_input">
              <textarea name='term_cond' rows="10"><?php echo $vieww->s_termcondition ; ?></textarea>
          </div>
      </div>
  </div>
</div>

</div> 


<div class="sep_box processingimg" style="display:none">
    <div class="col-lg-6">
    <div class="row">
        <div class="col-lg-2">
            <div class="tbl_text"></div>
        </div>
        <div class="col-lg-4">
            <div class="tbl_input">
                <img src="<?php echo base_url(); ?>loader.gif">

            </div>
        </div>
        </div>
    </div>
</div>



<div id="store_msg"></div>
<div class="col-lg-6">
    <div class="row">
        <div class="col-lg-4">
        </div>
        <div class="col-lg-8">
            <div class="tbl_input">

             <input id="submitBtn" type="submit"  name="outletsubmit" value="Submit" class="retailer_btn"/>
         </div>
     </div>
 </div>
</div>  

</div>

</form>
</div>




</div>


</div>
</div>
</div>
</body>

</html>

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />

<script type="text/javascript">


 $('.agrementsigndate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    endDate: '+0d',
    todayHighlight: true
    });

</script>   

<script>

$(document).ready(function(){
    $("#storeForm").validate({
          // Specify the validation rules
          rules: {

            s_username : "required",
            stateid : "required",
            companyaddress : "required",
            agrementsigndate : "required",
            accountholder : {required:true},
            accountnumber : {required:true},
            ifscode : "required",
            bankname : {required:true},
            branchname : {required:true},
            payment : "required",
            s_mobileno : {required:true, number:true},
            owndername : {required:true}
        },

        messages: {


            s_username : "Outlet name required",
            stateid : "State required",
            companyaddress : "Address required",
            agrementsigndate : "Agreement date required",
            accountholder :{required : "please enter account holder name!"},
            accountnumber : {required:"account number required"},
            ifscode : "ifsc code required",
            bankname : {required : "please enter bank name!"},
            branchname : {required : "please enter branch name!"},
            payment : "Category required",
            s_mobileno : {required:"contact no required", number:"Numbers Only"},
            owndername : {required : "Manager name required"},
  },

      
});
});

</script>

 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRQIujg1ucHWCiLAJqVO7RGYsL_lnwp-A&callback=initMap" type="text/javascript"></script>

<script type="text/javascript">
function getlongituate()
{



    var geocoder =  new google.maps.Geocoder();
   
//alert(geocoder);
 geocoder.geocode( { 'address': $('#address').val()}, function(results, status) {
//alert(results[0].geometry.location.lat());
 if(status=='OK')
{
    $('#longituate').text(results[0].geometry.location.lat()+'___'+results[0].geometry.location.lng());
        $("#lattitude").val(results[0].geometry.location.lat());

        $("#logitude").val(results[0].geometry.location.lng());
     }
        else
        {
            $('#longituate').text('Lat and Long cannot be found. please check address!').css('color','red');
           // alert('Lat and Long cannot be found!');
        }   
    });


}

</script>


<script>
 function sameaddress()
 {
   if($("#sameadd").is(':checked')){
   var rid = '<?php echo $rid ?>';
   //alert(rid); return false;
   $.ajax({
        url: "<?php echo base_url();?>admin/merchant/getbankdetailmerchant",
        data: {"rrid":rid},
        type: "POST",
        success: function(result){
          //alert(result); return false;  
          if(result!=''){
            var accholder_name = result.split("#")[0];
            var acc_number = result.split("#")[1];
            var bank_name = result.split("#")[2];
            var branch_name = result.split("#")[3];
            var ifsc_code = result.split("#")[4];
         
            $("#accholder_name").val(accholder_name);
            $("#acc_number").val(acc_number);
            $("#ifsc_code").val(ifsc_code); 
            $("#bank_name").val(bank_name);
            $("#branch_name").val(branch_name);


          }
        }
    }); 

   } else {

   $("#accholder_name").val('');
   $("#acc_number").val('');
   $("#ifsc_code").val(''); 
   $("#bank_name").val('');
   $("#branch_name").val('');
 
   


   } 
}
</script>

