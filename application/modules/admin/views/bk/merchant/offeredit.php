<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar');
    $getid=$this->uri->segment(4); 
?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Edit Offer Details</h2>
                      
                        </div>
                                    <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                          
         <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" id="offerform" method="post" enctype="multipart/form-data">
                                     <div class="sep_box">
                                            
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Category <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <?php 
                                                foreach ($category as $key => $value) 
                                                { 
                                                  if($value->cat_id==$view->p_catid)
                                                  {
                                                    echo $value->cat_name; 
                                                    echo '<input type="hidden" name="category" value="'.$value->cat_id.'"/>'; 
                                                  }
                                                }
                                               ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     
                                      
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Name <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div id="offerselectpicker">
                                                        <div class="tbl_input">
                                                           
                                                           <input type="text" name="oname_burnc" id="oname" value="<?php echo $view->p_oname_burnc; ?>">
                                                        </div>
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div>
                                              </div>
                                       <div class="sep_box">
                                        
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Image</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <input type="file" name="oimg" id="oimg" />
                                                    <img src="<?php echo base_url().'public/offer/'.$view->p_oimg_burnp; ?>" alt="doc" width="50px;" height="50px;">
                                                    <input type="hidden" name="oimg1" value="<?php echo $view->p_oimg_burnp; ?>"/>
                                                            <span id="msg"> 
                                                                
                                                            </span>

                                                        </div>
                                                    </div>                                      
                                                </div>
                                            </div>
                                        </div>

                                         <div class="sep_box">
                                            <div id="basicExample">
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Valid From <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="sdate" class="start" value="<?php echo $view->p_sdate; ?>" readonly/>
                                                        </div>
                                                    </div>                                           
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Valid To <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="edate" value="<?php echo $view->p_edate; ?>" class="end" readonly />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            </div> 
                                        </div>


                                        <div class="sep_box" > 
                                                                                      
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Quantity</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <select name="oqty_pcount" id="oqty" >
                                                                <?php for($i=0; $i<=100; $i++ ){ ?>
                                                                <option <?php if($view->p_oqty_pcount==$i) echo 'selected'; ?> value="<?php echo $i; ?>">
                                                        <?php if($i==0) echo 'Unlimited';else echo $i;?> </option>
                                                                <?php } ?>
                                                                    
                                                            </select>
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div> 
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >No. of Redemptions per User</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <select name="noofuser" id="noofuser" >
                                                                <?php for($i=0; $i<=10; $i++ ){ ?>
                                                                <option <?php if($view->p_nouser==$i) echo 'selected'; ?> value="<?php echo $i; ?>">
                                                        <?php if($i==0) echo 'Unlimited';else echo $i;?> </option>
                                                                <?php } ?>
                                                                    
                                                            </select>
                                                        
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div>  



                                        </div> 

                                           <div class="sep_box">                                        
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Days<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="col-lg-6">
                                                        <div class="tbl_input">
                                                            <input type="radio" name="oday_fcount" id="oday" value="1" <?php if($view->p_oday_fcount==1) echo 'checked';?>/> Fully
                                                            <input type="radio" id="oday" name="oday_fcount" value="2" <?php if($view->p_oday_fcount==2) echo 'checked';?>/> Partially
                                                        </div>
                                                    </div>
                                                        <div class="col-lg-6" id="partialdiv" style="display:<?php if($view->p_oday_fcount==2) echo 'block'; else echo 'none';?>;">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Days<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <select name="days[]" id="days" multiple="multiple">
<option value="Monday" <?php if(preg_match('/Monday/',$view->p_odayvalue_earnc)) echo "selected"; ?>>Monday</option>
<option value="Tuesday" <?php if(preg_match('/Tuesday/',$view->p_odayvalue_earnc)) echo "selected"; ?>>Tuesday</option>
<option value="Wednesday" <?php if(preg_match('/Wednesday/',$view->p_odayvalue_earnc)) echo "selected"; ?>>Wednesday</option>
<option value="Thursday" <?php if(preg_match('/Thursday/',$view->p_odayvalue_earnc)) echo "selected"; ?>>Thursday</option>
<option value="Friday" <?php if(preg_match('/Friday/',$view->p_odayvalue_earnc)) echo "selected"; ?>>Friday</option>
<option value="Saturday" <?php if(preg_match('/Saturday/',$view->p_odayvalue_earnc)) echo "selected"; ?>>Saturday</option>
<option value="Sunday" <?php if(preg_match('/Sunday/',$view->p_odayvalue_earnc)) echo "selected"; ?>>Sunday</option>
                                                            </select>
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div>

                                                    </div>                                     
                                                </div>
                                            </div>
                                            </div>
  <div class="sep_box"> 


                                              <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Offer Type</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="col-lg-6">
                                                        <div class="tbl_input">
                                                    <select name="of_pr_type">
                                                    <option <?php if($view->p_of_pr_type==0) echo 'selected';?> value="0">None</option>
                                                    <option <?php if($view->p_of_pr_type==1) echo 'selected';?> value="1">Discount (%)</option>
                                                    <option <?php if($view->p_of_pr_type==2) echo 'selected';?> value="2">Discount (Flat)</option>
                                                            
                                                             </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="tbl_input">
                                                    <input type="text" name="oftypevalue_earnp" value="<?php echo $view->p_oftypevalue_earnp; ?>"/></div></div>
                                                    </div>
                                                </div>
                                            </div>
                                          

                                        </div>

                                           <div class="sep_box">                                        
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Description<span style="color:red;font-weight: bold;">*</span> </div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="how" id="desc" value="<?php echo $view->p_how; ?>"/>
                                                           
                                                        </div>
                                                    </div>                                      
                                                </div>
                                            </div>                                        
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Terms & Conditions </div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <textarea name="term" id="term"><?php echo $view->p_term; ?></textarea>
                                                        </div>
                                                    </div>                                      
                                                </div>
                                            </div>                                             
                                        </div>

                                             <div class="sep_box">
                                             
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="program" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div>
                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />
<script src="<?php echo base_url();?>assets/js/datepair.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.datepair.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-multiselect.js">
</script>

<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-multiselect.css" type="text/css"/>

<script type="text/javascript">
$(document).ready(function(){
        $("#offerform").validate({ 

          rules: { 
              category :  "required",         
              oname  :   "required",
              sdate : "required",
              edate : "required",
              desc : "required"
              
          },
          
          messages: {
              category : "This field is required!",
              oname : "This field is required!",
              sdate : "This field is required!",
              edate : "This field is required!",
              desc : "This field is required!"
              
          },
          
      });
    
});


</script>

<script type="text/javascript">
       
 

    $('.start').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });
   
    $('.end').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });


</script>          

<script type="text/javascript">

 $(document).ready(function() {

   $('#days').multiselect({ 
    enableClickableOptGroups: true,
    enableFiltering: true,
    includeSelectAllOption: true,
  }); 
  });
 
 $(document).ready(function() {
    $("input[name=oday_fcount]").click(function(){
        if($("input[name=oday_fcount]:checked").val()==2) {
            $("#partialdiv").show();
        } else {
            $("#partialdiv").hide();
        }
    });
});

function offer()
{
    var offid=$('#offertype').val();
    if(offid==1)
    {
        $("#offervalue").show();
    } 
    else 
    {
        $("#offervalue").hide();
        $("#offvalue").val(0);
        
    }
    
}
 
</script>  

 <script>
                $(document).ready(function (){ 
                $("#oimg").change(function () { 
                  var val = $(this).val().toLowerCase(); 
                  var regex = new RegExp("(.*?)\.(jpeg|png|jpg)$"); 
                  if(!(regex.test(val))) { 
                  $(this).val(""); 
                  $("#msg").html("Unsupported file, only jpeg|png|jpg files are allowed!").css("color","red");
                  }
                }); 
                });
  </script> 