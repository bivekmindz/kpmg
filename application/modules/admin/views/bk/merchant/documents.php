<!DOCTYPE html>
<html>
<body>
 <link rel="stylesheet" href="<?php echo base_url();?>assets/css/viewer.css">
   <script src="<?php echo base_url();?>assets/js/viewer.js"></script>
  <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Document Details</h2>
                        </div>
                       
                                    <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            
                      
                      
 <style>
    .retailer_nav {
        width:100%;
        float:left;
    }
    .retailer_nav ul {
        padding:0;
        margin:0;
    }
    .retailer_nav ul li {
        list-style:none;
        float:left;
    }
    .retailer_nav ul li a {
        display:block;
        float:left;
        padding:10px 20px;
        background:#fff;
        margin-right:5px;
        color:#333;
        text-decoration:none;   
    }
   

 .add_btn {
    width: auto;
    height: auto;
    float: right;
    margin-right: 15px;
}
 
   .add_btn a {
       display:block;
       padding:10px 15px;
       background:#d84d28;
       color:#FFF;
       float:left;
       text-decoration:none;
       border-radius:5px;
       
   }
.add_100 {
    width:100%;
    float:left
}
 </style>
 
         
<div class="retailer_nav">
      <ul> <?php $getid=$this->uri->segment(4);
    
    echo '<li><a href="'.base_url('admin/merchant/addmerchant').'/'.@$getid.'">Merchant</a></li>';
    echo '<li><a href="'.base_url('admin/merchant/outlet').'/'.@$getid.'">Outlet</a></li>';
    echo '<li><a href="javascript:void(0)" class="active_color">Documents</a></li>';
    //echo '<li><a href="'.base_url('admin/program/currency').'/'.@$getid.'">Currency</a></li>';
    echo '<li><a href="'.base_url('admin/program/card').'/'.@$getid.'" >Card</a></li>';
    echo '<li><a href="'.base_url('admin/program/loyalty').'/'.@$getid.'" >Loyalty Program</a></li>';
    echo '<li><a href="'.base_url('admin/program/offer').'/'.@$getid.'" >Offers</a></li>';
    echo '<li><a href="'.base_url('admin/program/punch').'/'.@$getid.'" >Punch Card</a></li>';
      
?>
      </ul>
</div>
      
              <?php $logintype=$this->session->userdata('lw_login')->s_logintype;
                      if($logintype!=2) 
                      { 
                          $readonly='readonly'; 
                          $none='style="display:none;"'; 
                      ?>
              
               
                        <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" method="post" enctype="multipart/form-data">
                                    
                                             <div class="sep_box">
                                               
                                      <?php $i=1;
                                      foreach ($document as $key => $value) { $i++; ?>

                                                <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text"><?php echo $value->d_name; ?><span style="color:red;font-weight: bold;"></span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="hidden" name="docid[]" value="<?php echo $value->d_id; ?>"/>
                                                             <input type="file" name="docfilename[]" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                          <?php } ?>
                                        </div>
                                              <div class="sep_box">
                                             <div class="col-lg-2">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="documentsubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div> </div>

                            <?php } ?>


       <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Document Name</th>
                                               <th bgcolor='red'>Document Image</th>
                                                
                                                <th bgcolor='red'>Action</th>                                             
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                            foreach ($getdocument as $value) {
                                            $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->d_name; ?></td>
                                            <td> 
                                              <ul class="docs-pictures clearfix">
            <li><img data-original="<?php echo base_url().'public/document/'.$value->dc_image; ?>" src="<?php echo base_url().'public/document/'.$value->dc_image; ?>" alt="doc" width="50px;" height="50px;">
            </li></ul>
                                           
                                                
                                            </td>
                                            
                                            <td>
                                            <a <?php if($logintype==2){ echo 'style="display:none;"'; } ?> href="<?php echo base_url()?>admin/merchant/deletedocument/<?php echo $value->dc_id.'/'.$this->uri->segment(4);?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            
                                            <a href="<?php echo base_url().'public/document/'.$value->dc_image; ?>" download>Download</a>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>


<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>


<script type="text/javascript">

  $(document).ready(function(){
    $("#currency").validate({
          // Specify the validation rules
          rules: {
              country: "required",
              currency:{required:true, number:true}
          },
          
          messages: {
            
              country : "Please select country!",
              currency : {required:"Please enter values!", number:"Numbers only!"}
          },
          
          submitHandler: function(form) {
           
          }
      });
   });

</script>
