<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar');
    $getid=$this->uri->segment(4); 
    $logintype=$this->session->userdata('lw_login')->s_logintype;
    if($logintype==3)
    {
        $url=base_url('admin/program/programoutlet/OF');
    }
    else
    {
        $url=base_url('admin/program/programlist').'/OF/'.@$getid;
    }?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Offer Details</h2>
                       <a href="<?php echo $url;?>"><button type="button" style="float:right;">Offer List</button></a> 
                        </div>
                                    <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                          
                      
                      
 <style>
    .retailer_nav {
        width:100%;
        float:left;
    }
    .retailer_nav ul {
        padding:0;
        margin:0;
    }
    .retailer_nav ul li {
        list-style:none;
        float:left;
    }
    .retailer_nav ul li a {
        display:block;
        float:left;
        padding:10px 20px;
        background:#fff;
        margin-right:5px;
        color:#333;
        text-decoration:none;   
    }
   

 .add_btn {
    width: auto;
    height: auto;
    float: right;
    margin-right: 15px;
}
 
   .add_btn a {
       display:block;
       padding:10px 15px;
       background:#d84d28;
       color:#FFF;
       float:left;
       text-decoration:none;
       border-radius:5px;
       
   }
.add_100 {
    width:100%;
    float:left
}
 </style>
 
         
<div class="retailer_nav">
      <ul> <?php 
    if($logintype==3)
    {
        echo '<li><a href="'.base_url('admin/program/loyalty').'/'.@$getid.'" >Loyalty Program</a></li>';
        echo '<li><a href="javascript:void(0)" class="active_color">Offers</a></li>';
        echo '<li><a href="'.base_url('admin/program/punch').'/'.@$getid.'" >Punch Card</a></li>';
    }
    else
    {
        echo '<li><a href="'.base_url('admin/merchant/addmerchant').'/'.@$getid.'">Merchant</a></li>';
        echo '<li><a href="'.base_url('admin/merchant/outlet').'/'.@$getid.'">Outlet</a></li>';
        echo '<li><a href="'.base_url('admin/merchant/document').'/'.@$getid.'" >Documents</a></li>';
        //echo '<li><a href="'.base_url('admin/program/currency').'/'.@$getid.'" >Currency</a></li>';
        echo '<li><a href="'.base_url('admin/program/card').'/'.@$getid.'" >Card</a></li>';
        echo '<li><a href="'.base_url('admin/program/loyalty').'/'.@$getid.'" >Loyalty Program</a></li>';
        echo '<li><a href="javascript:void(0)" class="active_color">Offers</a></li>';
        echo '<li><a href="'.base_url('admin/program/punch').'/'.@$getid.'" >Punch Card</a></li>';
    }    
      
?>
      </ul>
</div>
      
               
                        <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" id="offerform" method="post" enctype="multipart/form-data">
                                     <div class="sep_box">
                                            
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <?php 
                                              if($logintype==3)
                                              {
                                                foreach ($category as $key => $value) 
                                                { 
                                                  if($value->cat_id==$this->session->userdata('lw_login')->catid)
                                                  {
                                                    echo $value->cat_name; 
                                                    echo '<input type="hidden" name="category" value="'.$value->cat_id.'"/>'; 
                                                  }
                                                }
                                              }
                                              else
                                              { ?>
                                                           <select id="category" name="category">
                                                            <option value="">Select Category</option>
                                                            <?php foreach ($category as $key => $value) { ?>

                                                            <option value="<?php echo $value->cat_id; ?>">
                                                             <?php echo $value->cat_name; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                             <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     
                                      
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Name <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div id="offerselectpicker">
                                                        <div class="tbl_input">
                                                           
                                                           <input type="text" name="oname" id="oname" >
                                                        </div>
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div>
                                              </div>
                                       <div class="sep_box">
                                           <!--  <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Tagline </div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <textarea name="tagline"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                              -->
                                            
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Image <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <input type="file" name="oimg" id="oimg" />
                                                            <span id="msg"> 
                                                                
                                                            </span>

                                                        </div>
                                                    </div>                                      
                                                </div>
                                            </div>
                                        </div>

                                         <div class="sep_box">
                                            <div id="basicExample">
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Valid From <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <!--  <input type="text" name="sdate" class="date start" id="o_start_date" readonly /> -->
                                                            <input type="text" name="sdate" class="start"readonly/>
                                                        </div>
                                                    </div>                                           
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Valid To <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <!-- <input type="text" name="edate" class="date end" id="o_end_date" readonly /> -->
                                                            <input type="text" name="edate" class="end" readonly />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            </div> 
                                        </div>


                                        <div class="sep_box" > 
                                                                                      
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Quantity</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <select name="oqty" id="oqty" >
                                                            
                                                            <option value="0" selected >Unlimited</option>
                                                                <?php for($i=1; $i<=100; $i++ ){ ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>
                                                                    
                                                            </select>
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div> 
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >No. of Redemptions per User</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <select name="noofuser" id="noofuser" >
                                                          
                                                            <option value="0" selected >Unlimited</option>
                                                                <?php for($i=1; $i<=10; $i++ ){ ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>
                                                                    
                                                            </select>
                                                        
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div>  



                                        </div> 

                                           <div class="sep_box">                                        
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Days<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="col-lg-6">
                                                        <div class="tbl_input">
                                                            <input type="radio" name="oday" id="oday" value="1" checked/> Fully
                                                            <input type="radio" id="oday" name="oday" value="2" /> Partially
                                                        </div>
                                                    </div>
                                                        <div class="col-lg-6" id="partialdiv" style="display:none;">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Days<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <select name="days[]" id="days" multiple="multiple">
                                                                <option value="Monday">Monday</option>
                                                                <option value="Tuesday">Tuesday</option>
                                                                <option value="Wednesday">Wednesday</option>
                                                                <option value="Thursday">Thursday</option>
                                                                <option value="Friday">Friday</option>
                                                                <option value="Saturday">Saturday</option>
                                                                <option value="Sunday">Sunday</option>
                                                            </select>
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div>

                                                    </div>                                     
                                                </div>
                                            </div>
                                           <!--  <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Offer Type<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="col-lg-6">
                                                        <div class="tbl_input">
                                                           <select id="offertype" name="offertype" onclick="offer();">
                                                            <option value="">Select Offer Type</option>
                                                            <?php foreach ($offertype as $key => $value) { ?>

                                                            <option value="<?php echo $value->offertype_id; ?>">
                                                             <?php echo $value->offer_title; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6" id="offervalue" style="display:none;"><div class="tbl_input">
                                                            <input type="text" name="offvalue" id="offvalue" value="" placeholder="0"/>
                                                        
                                                            <select name="vtype" id="vtype">
                                                                <option value="Flat">Flat</option>
                                                                <option value="%">%</option>
                                                            </select> </div></div>
                                                    </div>
                                                </div>
                                            </div> -->

                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Offer Type</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="col-lg-6">
                                                        <div class="tbl_input">
                                                    <select id="offertype" name="offertype">
                                                    <option value="0">None</option>
                                                    <option value="1">Discount (%)</option>
                                                    <option value="2">Discount (Flat)</option>
                                                            
                                                             </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6" id="offervalue">
                                                        <div class="tbl_input">
                                                    <input type="text" name="offvalue" id="offvalue" value="0" placeholder="0"/></div></div>
                                                    </div>
                                                </div>
                                            </div>
                         

                                        </div>

                                           <div class="sep_box">                                        
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Offer Description<span style="color:red;font-weight: bold;">*</span> </div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="desc" id="desc"placeholder="Buy 2 + Get 1 Free,Discount"/>
                                                           
                                                        </div>
                                                    </div>                                      
                                                </div>
                                            </div>                                        
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Terms & Conditions </div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <textarea name="term" id="term"></textarea>
                                                        </div>
                                                    </div>                                      
                                                </div>
                                            </div>                                             
                                        </div>

                                             <div class="sep_box">
                                             
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="offersubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div>
                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery-1.9.1.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
<script> CKEDITOR.replace('desc1'); </script>
<script> CKEDITOR.replace('term1'); </script>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />
<script src="<?php echo base_url();?>assets/js/datepair.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.datepair.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-multiselect.js">
</script>

<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-multiselect.css" type="text/css"/>

<script type="text/javascript">
$(document).ready(function(){
        $("#offerform").validate({ 

          rules: { 
              category :  "required",         
              oname  :   "required",
              oimg : "required",
              sdate : "required",
              edate : "required",
              desc : "required"
              
          },
          
          messages: {
              category : "This field is required!",
              oname : "This field is required!",
              oimg : "This field is required!",
              sdate : "This field is required!",
              edate : "This field is required!",
              desc : "This field is required!"
              
          },
          
      });
    
});


</script>

<script type="text/javascript">
       
   /* var date = new Date();
    date.setDate(date.getDate()+4);

    $('#basicExample .date').datepicker({
     'format': 'yyyy-mm-dd',
     'autoclose': true,
     startDate: date
     
    });

    var basicExampleEl = document.getElementById('basicExample');
    var datepair = new Datepair(basicExampleEl);*/

    $('.start').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });
   
    $('.end').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });


</script>          

<script type="text/javascript">

 $(document).ready(function() {

   $('#days').multiselect({ 
    enableClickableOptGroups: true,
    enableFiltering: true,
    includeSelectAllOption: true,
  }); 
  });
 
 $(document).ready(function() {
    $("input[name=oday]").click(function(){
        if($("input[name=oday]:checked").val()==2) {
            $("#partialdiv").show();
        } else {
            $("#partialdiv").hide();
        }
    });
});

function offer()
{
    var offid=$('#offertype').val();
    if(offid==1)
    {
        $("#offervalue").show();
    } 
    else 
    {
        $("#offervalue").hide();
        $("#offvalue").val(0);
        
    }
    
}
 
</script> 

 <script>
                $(document).ready(function (){ 
                $("#oimg").change(function () { 
                  var val = $(this).val().toLowerCase(); 
                  var regex = new RegExp("(.*?)\.(jpeg|png|jpg)$"); 
                  if(!(regex.test(val))) { 
                  $(this).val(""); 
                  $("#msg").html("Unsupported file, only jpeg|png|jpg files are allowed!").css("color","red");
                  }
                }); 
                });
  </script>  