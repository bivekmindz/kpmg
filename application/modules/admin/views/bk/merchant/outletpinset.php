<!DOCTYPE html>

<html>

<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
            
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Outlet List for PIN Set</h2>
                        </div>
                        
 
        <div class="page_box" id="storelistingdiv">
      <table class="grid_tbl" id="example">
                                    <thead>
                                        <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Outlet Name</th>
                                                <th bgcolor='red'>Address</th>
                                                <th bgcolor='red'>Category</th>
                                                <th bgcolor='red'>Manager Name</th>
                                                <th bgcolor='red'>Contact No.</th>
                                                <th bgcolor='red'>Email/Login ID</th>
                                                <th bgcolor='red'>Pin</th>
                                                <th bgcolor='red'>Set Pin</th>

                                               
                                        </tr>
                                    </thead>
                                    
                                    
                                    <tbody> 
                                    <?php 
                                        $i = 1;
                                            foreach ($list as $key => $value) { ?>
                                            <tr>


                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $value->s_name; ?><input type="hidden" id="g<?php echo $value->s_loginid; ?>" value="<?php echo encrypt($value->s_pin); ?>"/></td> 
                                        <td><?php echo $value->s_address; ?></td> 
                                        <td><?php
                                      $parameter = array(
                                      'act_mode' => 'getcatid',
                                      'row_id'=>$value->s_payt_catid,
                                      'counname'=>'',
                                      'coucode'=>'',
                                      'commid'=>''
                                      );
                                       //p($parameter); exit;
                             $resp = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
                             echo $resp->cat_name;
                                           ?></td> 
                                        
                                        <td><?php echo $value->s_username; ?></td> 
                                        <td><?php echo $value->s_mobile; ?></td> 
                                        <td><?php echo $value->s_email; ?></td>
                                        <td><span id="p<?php echo $value->s_loginid; ?>">**** <a href="javascript:void(0)" onclick="showpin('<?php echo $value->s_loginid; ?>')">Show PIN?</a></span></td> 
                                        <td><a href="<?php echo base_url('set-pin'); ?>/<?php echo encrypt($value->s_loginid); ?>"><i class="fa fa-lock" style="font-size: 20px;"></i> </a>
                                           </td>
                                       
                                        
                                         
                                    </tr>
                                    <?php $i++; } ?>               
                                    </tbody>
                                    </table>
                                    
                
            </div>

             
        </div>     
              

            </div>
        </div>
    </div>


</body>
</html>

<script type="text/javascript">
function showpin(ids)
{
    var getpin='Merchant PIN<input type="password" id="mer'+ids+'" size="4"/> <a href="javascript:void(0)" onclick="getpin('+ids+')">Get PIN</a>';
    $('#p'+ids).empty();
    $('#p'+ids).append(getpin);

}

function getpin(ids)
{
    var merpin=$('#mer'+ids).val();
    var outpin=$('#g'+ids).val();
   
    $.ajax({
        url: '<?php echo base_url();?>admin/merchant/getoutletpin',
        type:'POST',
        data: {'merpin':merpin,'outpin':outpin},
        success: function(data){
//alert(data);
        if(data==0)
        {
            $('#p'+ids).empty();
            $('#p'+ids).append('Merchant PIN Wrong!');
            setTimeout(function(){ 
                $('#p'+ids).empty(); 
                $('#p'+ids).append('**** <a href="javascript:void(0)" onclick="showpin('+ids+')">Show PIN?</a>');
            }, 4000);
        }
        else if(data==1)
        {
            $('#p'+ids).empty();
            $('#p'+ids).append('PIN Not Set!');
            setTimeout(function(){ 
                $('#p'+ids).empty(); 
                $('#p'+ids).append('**** <a href="javascript:void(0)" onclick="showpin('+ids+')">Show PIN?</a>');
            }, 4000);
        }
        else
        {
            $('#p'+ids).empty();
            $('#p'+ids).append(data+' <a href="javascript:void(0)" onclick="removepin('+ids+')">Remove</a>');
            setTimeout(function(){ 
                $('#p'+ids).empty(); 
                $('#p'+ids).append('**** <a href="javascript:void(0)" onclick="showpin('+ids+')">Show PIN?</a>');
            }, 30000);
        }
           
       }

    });
}

function removepin(ids)
{
    $('#p'+ids).empty(); 
    $('#p'+ids).append('**** <a href="javascript:void(0)" onclick="showpin('+ids+')">Show PIN?</a>');
}
</script>

