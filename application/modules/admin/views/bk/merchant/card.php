<!DOCTYPE html>
<html>
<body>
 <link rel="stylesheet" href="<?php echo base_url();?>assets/css/viewer.css">
   <script src="<?php echo base_url();?>assets/js/viewer.js"></script>
  <script src="<?php echo base_url();?>assets/js/main.js"></script>
    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Card Details</h2>
                        </div>
                        <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                           
                      
 <style>
    .retailer_nav {
        width:100%;
        float:left;
    }
    .retailer_nav ul {
        padding:0;
        margin:0;
    }
    .retailer_nav ul li {
        list-style:none;
        float:left;
    }
    .retailer_nav ul li a {
        display:block;
        float:left;
        padding:10px 20px;
        background:#fff;
        margin-right:5px;
        color:#333;
        text-decoration:none;   
    }
  

 .add_btn {
    width: auto;
    height: auto;
    float: right;
    margin-right: 15px;
}
 
   .add_btn a {
       display:block;
       padding:10px 15px;
       background:#d84d28;
       color:#FFF;
       float:left;
       text-decoration:none;
       border-radius:5px;
       
   }
.add_100 {
    width:100%;
    float:left
}
 </style>
 
         
<div class="retailer_nav">
      <ul> <?php $getid=$this->uri->segment(4);
    
    echo '<li><a href="'.base_url('admin/merchant/addmerchant').'/'.@$getid.'">Merchant</a></li>';
    echo '<li><a href="'.base_url('admin/merchant/outlet').'/'.@$getid.'">Outlet</a></li>';
    echo '<li><a href="'.base_url('admin/merchant/document').'/'.@$getid.'" >Documents</a></li>';
    echo '<li><a href="javascript:void(0)" class="active_color">Card</a></li>';
    //echo '<li><a href="'.base_url('admin/program/currency').'/'.@$getid.'">Currency</a></li>';
    echo '<li><a href="'.base_url('admin/program/loyalty').'/'.@$getid.'" >Loyalty Program</a></li>';
    echo '<li><a href="'.base_url('admin/program/offer').'/'.@$getid.'" >Offers</a></li>';
    echo '<li><a href="'.base_url('admin/program/punch').'/'.@$getid.'" >Punch Card</a></li>';
      
?>
      </ul>
</div>
      
              
              
               
                        <div class="page_box" id="retailerdiv">
                           <form action="" method="post" id="card" enctype="multipart/form-data">
                            <div class="sep_box">
                               <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Category<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                        <select id="category" name="category">
                                                            <option value="">Select Category</option>
                                                            <?php foreach ($category as $key => $value) { ?>

                                                            <option value="<?php echo $value->cat_id; ?>">
                                                             <?php echo $value->cat_name; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Card Name<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="text" name="cardname" id="cardname" placeholder="Card Name"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Card Image<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                             <input type="file" name="cardfilename" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                      
                                        </div>
                                              <div class="sep_box">
                                             <div class="col-lg-2">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="cardsubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>



       <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Category Name</th>
                                                <th bgcolor='red'>Card Name</th>
                                                <th bgcolor='red'>Card Image</th>
                                                
                                                <th bgcolor='red'>Action</th>                                             
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                            foreach ($getcard as $value) {
                                            $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->cat_name; ?></td>
                                             <td><?php echo $value->c_name; ?></td>
                                            <td> 
                                              <ul class="docs-pictures clearfix">
            <li><img data-original="<?php echo base_url().'public/card/'.$value->c_image; ?>" src="<?php echo base_url().'public/card/'.$value->c_image; ?>" alt="doc" width="50px;" height="50px;">
            </li></ul>
                                           
                                                
                                            </td>
                                            
                                            <td>
                                           <?php 
                                           $logintype=$this->session->userdata('lw_login')->s_logintype;
                                           if($logintype==2) 
                                                {  
                                                  if($value->c_status==1)  
                                                    echo 'Active'; 
                                                  else 
                                                    echo '<span style="color:red">Inactive</span>'; 
                                                  //echo '|<a href="'.base_url().'admin/merchant/outletedit/'.$value->c_id.'"><i class="fa fa-pencil"></i></a>';
                                                     }
                                                     else
                                                      {
?>
<a href="<?php echo base_url(); ?>admin/program/cardstatus/<?php echo $value->c_id.'/'.$value->c_status.'/'.$value->c_merchantid.'/'.$value->c_catid; ?>">
<?php if($value->c_status==1)  echo 'Active'; else echo '<span style="color:red">Inactive</span>'; ?></a> 
<!-- |<a href="<?php echo base_url(); ?>admin/program/cardedit/<?php echo $value->c_id; ?>"><i class="fa fa-pencil"></i></a> -->
  <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>


<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>


<script type="text/javascript">

  $(document).ready(function(){
    $("#card").validate({
          // Specify the validation rules
          rules: {
              category: "required",
              cardname: "required",
              cardfilename: "required"
          },
          
          messages: {
            
              category : "Please select category!",
              cardname : "Please card name!",
              cardfilename : "Please upload card image!"
          },
          
         
      });
   });

</script>
