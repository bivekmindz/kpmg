<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); 
    $getid=$this->uri->segment(4);
    $logintype=$this->session->userdata('lw_login')->s_logintype;
    if($logintype==3)
    {
        $url=base_url('admin/program/programoutlet/PC');
    }
    else
    {
        $url=base_url('admin/program/programlist').'/PC/'.@$getid;
    }?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Punch Card Details</h2>
                        <a href="<?php echo $url; ?>"><button type="button" style="float:right;">Punch Card List</button></a></div>
                                    <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                            
                       
                      
                      
 <style>
    .retailer_nav {
        width:100%;
        float:left;
    }
    .retailer_nav ul {
        padding:0;
        margin:0;
    }
    .retailer_nav ul li {
        list-style:none;
        float:left;
    }
    .retailer_nav ul li a {
        display:block;
        float:left;
        padding:10px 20px;
        background:#fff;
        margin-right:5px;
        color:#333;
        text-decoration:none;   
    }
   

 .add_btn {
    width: auto;
    height: auto;
    float: right;
    margin-right: 15px;
}
 
   .add_btn a {
       display:block;
       padding:10px 15px;
       background:#d84d28;
       color:#FFF;
       float:left;
       text-decoration:none;
       border-radius:5px;
       
   }
.add_100 {
    width:100%;
    float:left
}
 </style>
 
         
<div class="retailer_nav">
      <ul> <?php 
    if($logintype==3)
    {
        echo '<li><a href="'.base_url('admin/program/loyalty').'/'.@$getid.'" >Loyalty Program</a></li>';
        echo '<li><a href="'.base_url('admin/program/offer').'/'.@$getid.'" >Offers</a></li>';
        echo '<li><a href="javascript:void(0)" class="active_color">Punch Card</a></li>';
    }
    else
    {
        echo '<li><a href="'.base_url('admin/merchant/addmerchant').'/'.@$getid.'">Merchant</a></li>';
        echo '<li><a href="'.base_url('admin/merchant/outlet').'/'.@$getid.'">Outlet</a></li>';
        echo '<li><a href="'.base_url('admin/merchant/document').'/'.@$getid.'" >Documents</a></li>';
        //echo '<li><a href="'.base_url('admin/program/currency').'/'.@$getid.'" >Currency</a></li>';
        echo '<li><a href="'.base_url('admin/program/card').'/'.@$getid.'" >Card</a></li>';
        echo '<li><a href="'.base_url('admin/program/loyalty').'/'.@$getid.'" >Loyalty Program</a></li>';
        echo '<li><a href="'.base_url('admin/program/offer').'/'.@$getid.'" >Offers</a></li>';
        echo '<li><a href="javascript:void(0)" class="active_color">Punch Card</a></li>';
    }
      
?>
      </ul>
</div>
      
              
              
               
                        <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" id="punch" method="post" enctype="multipart/form-data">
                                     <div class="sep_box">
                                            
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                             <?php 
                                              if($logintype==3)
                                              {
                                                foreach ($category as $key => $value) 
                                                { 
                                                  if($value->cat_id==$this->session->userdata('lw_login')->catid)
                                                  {
                                                    echo $value->cat_name; 
                                                    echo '<input type="hidden" name="category" value="'.$value->cat_id.'"/>'; 
                                                  }
                                                }
                                              }
                                              else
                                              { ?>
                                                           <select id="category" name="category">
                                                            <option value="">Select Category</option>
                                                            <?php foreach ($category as $key => $value) { ?>

                                                            <option value="<?php echo $value->cat_id; ?>">
                                                             <?php echo $value->cat_name; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                             <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Product Type<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          
                                                            <input type="text" class="product" name="product" placeholder="Coffee,Tea">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                                <!-- <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Product Type<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <select id="product" name="product">
                                                            <option value="">Select Product Type</option>
                                                            <?php foreach ($protype as $key => $value) { ?>

                                                            <option value="<?php echo $value->p_id; ?>">
                                                             <?php echo $value->p_name; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->

                                              
                                    </div>

                                    <div class="sep_box">
                                            
                                        <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Paid Count<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                        <select id="pcount" name="pcount">
                                                            <option value="">Select Paid Count</option>
                                                             <?php for($i=1; $i<=20; $i++ ){ ?>
                                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Free Count <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select id="fcount" name="fcount">
                                                            <option value="">Select Free Count</option>
                                                             <?php for($i=1; $i<=20; $i++ ){ ?>
                                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             

                                        </div> 
                                        
                                            <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Start Date<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                               <input type="text" name="sdate" class="sdate" readonly/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">End Date<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                             <input type="text" class="edate" name="edate" readonly>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>

                                            <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Use Image</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="radio" name="imgaccess" value="1" checked/> Card
                                                            <input type="radio" name="imgaccess" value="2" /> Punch
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                            
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Punch Image</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <input type="file" name="oimg" id="oimg" />
                                                            <span id="msg"> 
                                                                
                                                            </span>

                                                        </div>
                                                    </div>                                      
                                                </div>
                                            </div>
                                        </div>

                                             <div class="sep_box">
                                               <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">No. of Punch per User<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <select name="noofpunch" id="noofpunch" >
                                                            <option value="0">Unlimited</option>
                                                            <?php for($i=1; $i<=10; $i++ ){ ?>
                                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">How it Works?<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="how"  id="how"></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
    
                                            </div>

                                             <div class="sep_box">
                                               <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Terms of Use<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <textarea name="term"  id="term"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">FAQs<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="faq"  id="faq"></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         
    
                                            </div>

                                             <div class="sep_box">
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="punchsubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div>
                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>





<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />

<script type="text/javascript">
   

    $('.sdate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });
   
    $('.edate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });


    


</script>   



<script type="text/javascript">

  $(document).ready(function(){
    $("#punch").validate({
          // Specify the validation rules
          rules: {
              category: "required",
              product:"required",
              pcount : "required",
              fcount : "required",
              sdate: "required",
              edate : "required",
              how : "required",
              term : "required",
              faq : "required"

          },
          
          messages: {
            
              category : "Please select category!",
              product : "Please select product!" ,
              pcount : "Please select paid count!",
              fcount : "Please select free count!",
              sdate : "please select start date!",
              edate : "please select end date!",
              how : "please enter how it works!",
              term : "please enter term of use!",
              faq : "please enter FAQs!"
              
          },
          
         
      });
   });

</script>

 <script>
                $(document).ready(function (){ 
                $("#oimg").change(function () { 
                  var val = $(this).val().toLowerCase(); 
                  var regex = new RegExp("(.*?)\.(jpeg|png|jpg)$"); 
                  if(!(regex.test(val))) { 
                  $(this).val(""); 
                  $("#msg").html("Unsupported file, only jpeg|png|jpg files are allowed!").css("color","red");
                  }
                }); 
                });
  </script>
