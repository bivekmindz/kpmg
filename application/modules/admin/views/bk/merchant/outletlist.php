<link rel="stylesheet" href="<?php echo base_url();?>assets/css/viewer.css">
   <script src="<?php echo base_url();?>assets/js/viewer.js"></script>
  <script src="<?php echo base_url();?>assets/js/main.js"></script>


<style type="text/css">

    .alert-close {
    background: rgba(255,255,255,0.1);
    
    color: #000000;
    cursor: pointer;
    float: right;
    font-size: 25px;
  
}
    </style>
    <style type="text/css">
.deal_detail{
    float:left;
    width:100%;
    height:auto;
    background:#f1f1f1;
    padding:10px 10px;
}
.deal_left{
    float:left;
    width:25%;
}
.d_image{
    float:left;
    width:100%;
    background: #fff;
    padding:6px 6px;
}
.d_image img{
    width:100%;
}
.deal_right{
    float:left;
    width: 75%;
    padding: 0px 20px;
}
.deal_row{
    float:left;
    width:100%;
    padding:7px 0px;
}
.deal_d2
{
    float:left;
    width:50%;
    padding:0px 10px;
}
.deal_d1
{
    float:left;
    width:100%;
    padding:0px 10px;
}
.deal_text
{
    float:left;
    width:100%;
    font-weight:600;
}
.deal_text_b
{
    float:left;
    width:100%;
    font-weight:400;
    padding:8px 0;
}
.deal_text span{
    font-weight:400!important;
    margin-left:15px;
}
.approve_btn
{
    float:right;
    padding:8px 12px;
    background: green;
    color:#fff!important;
}
.issue_btn
{
    float:right;
    padding:8px 12px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.t_area{
    float:left;
    width:60%;
}
.sbt_btn{
    /*float:left;*/
    padding:6px 15px;
    background: red;
    color:#fff!important;
    margin-left:8px;
}
.issuediv{
    display:none;
}

.d_image img {
    width: 200px;
    height: 252px;
    text-align: center;
}

 </style>
    
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    </script>
    
    <script type="text/javascript">
    $(document).ready(function(c) {
        $('.alert-close').on('click', function(c){
            $(this).parent().fadeOut('slow', function(c){
                //location.reload();
            });
        }); 
    });
    </script>
    <script type="text/javascript">
  
  function showdealdetailstr(id)
  {
    $('.dealdetailstr').closest('tr').addClass('str');
    $('#sho_'+id).closest('tr').removeClass('str');
  }

</script>
<!DOCTYPE html>

<html>

<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
            
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Outlet List</h2>
                        </div>
                     
                                <p style="color:green"><?php  echo $this->session->flashdata('message'); ?></p>
                                 
                         
                      
                                 <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('#example').DataTable();
                                    $( ".grid_tbl" ).wrap( "<div class='new_width'></div>" );
                                    } );
                                </script>           
     <div class="page_box" id="storelistingdiv">
     <table class="grid_tbl" id="example">
                                    <thead>
                                        <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Logo</th>
                                               <?php $logintype=$this->session->userdata('lw_login')->s_logintype;
                                                if($logintype!=2) 
                                                { ?>
                                                <th bgcolor='red'>Merchant ID</th>
                                                <?php } ?>
                                                <th bgcolor='red'>Outlet ID</th>
                                                <th bgcolor='red'>Outlet Name</th>
                                                <th bgcolor='red'>Address</th>
                                                <th bgcolor='red'>Category</th>
                                                <th bgcolor='red'>Manager Name</th>
                                                <th bgcolor='red'>Contact No.</th>
                                                <th bgcolor='red'>Email/Login ID</th>
                                                <th bgcolor='red'>Date of Agreement</th> 
                                                <th bgcolor='red'>Action</th>

                                               
                                        </tr>
                                    </thead>
                                    
                                    
                                    <tbody class="append"> 
                                    <?php 
                                        $i = 1;
                                            foreach ($list as $key => $value) { ?>
                                            <tr class="append_wrapper">


                                        <td><?php echo $i; ?></td>
                                        <td> <ul class="docs-pictures clearfix">
            <li><img data-original="<?php echo base_url().'public/merchant/'.$value->s_picture; ?>" src="<?php echo base_url().'public/merchant/'.$value->s_picture; ?>" alt="Merchant" width="50px;" height="50px;">
            </li>
            
          
          </ul>
                                         
                                        </td>
                                        <?php if($logintype!=2) 
                                                { ?>
                                        <td> <?php
                                      $parameter = array(
                                      'act_mode' => 'getleadmngr',
                                      'row_id'=>$value->s_merchantid,
                                      'counname'=>'',
                                      'coucode'=>'',
                                      'commid'=>''
                                      );
                                       //p($parameter); exit;
                             $response = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
                             echo $response->code;
                                           ?></td>
                                           <?php } ?>
                                        <td><a href="javascript:void(0)" onclick="showdealdetailstr('<?php echo $i; ?>')"><?php echo $value->s_uniqe_qr_code; ?></a></td>
                                        <td><?php echo $value->s_name; ?></td> 
                                        <td><?php echo $value->s_address; ?></td> 
                                        <td><?php
                                      $parameter = array(
                                      'act_mode' => 'getcatid',
                                      'row_id'=>$value->s_payt_catid,
                                      'counname'=>'',
                                      'coucode'=>'',
                                      'commid'=>''
                                      );
                                       //p($parameter); exit;
                             $resp = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
                             echo $resp->cat_name;
                                           ?></td> 
                                        
                                        <td><?php echo $value->s_username; ?></td> 
                                        <td><?php echo $value->s_mobile; ?></td> 
                                         
                                        <td><?php echo $value->s_email; ?></td> 
                                        
                                        <td><?php if($value->s_agreementdate==''){echo '';}else{ echo date("d-m-Y", strtotime($value->s_agreementdate));} ?></td>
                                            
                                        <td>
                                          <?php $rowid = $this->uri->segment(4);
                                            if($logintype!=2) 
                                                { ?>
                                    <a href="<?php echo base_url(); ?>admin/merchant/updatestatus/<?php echo $value->s_loginid.'/'.$value->s_status.'/O/'.$rowid; ?>" >
                                        <?php if($value->s_status==1)  echo 'Active'; else echo '<span style="color:red">Inactive</span>'; ?>
                                    </a> |
                                        <a href="<?php echo base_url(); ?>admin/merchant/outletedit/<?php echo $value->s_loginid; ?>"><i class="fa fa-pencil"></i> </a> 
                                         
<?php }else { if($value->s_status==1) {?>
 <a href="<?php echo base_url(); ?>admin/merchant/updatestatus/<?php echo $value->s_loginid.'/'.$value->s_status.'/O/'.$rowid; ?>" >Active</a> <?php } else { echo '<span style="color:red">Inactive</span>'; ?>|<a href="<?php echo base_url(); ?>admin/merchant/outletedit/<?php echo $value->s_loginid; ?>"><i class="fa fa-pencil"></i> </a><?php } ?>
                                        
<?php } ?>
                                         </td>
                                       
                                        
                                         
                                    </tr>

                                    <style>.str{display:none;}</style>
                                        <div id="showdiv">

                                          
                                        <tr class="dealdetailstr str"  id="sho_<?php echo $i; ?>">

                                            <td colspan="12">
                                                 <!-- <div class="alert-close">×</div> -->
                                                <div class="deal_detail" id="deal_detail" >
                    <div class="deal_left">
                    <ul class="docs-pictures clearfix">
                      <li><img data-original="<?php echo base_url().'public/merchant/'.$value->s_picture; ?>" src="<?php echo base_url().'public/merchant/'.$value->s_picture; ?>" alt="Outlet" width="200px;" height="250px;"></li>
                    </ul>
                    </div>
                                                    <div class="deal_right">
                                                       <div class="deal_row">
                                                            <div class="deal_d2"><div class="deal_text">Outlet Type : <span><?php echo $value->store_type; ?></span></div></div>
                                                            <div class="deal_d2"><div class="deal_text">State/City : <span><?php echo $value->g_statename.', '.$value->g_cityname; ?></span></div></div>
                                                        </div>
                                                    <div class="deal_row">
                                                            <div class="deal_d2"><div class="deal_text">Account Holder Name : <span><?php echo $value->s_achname; ?></span></div></div>
                                                            <div class="deal_d2"><div class="deal_text">Account Number : <span><?php echo $value->s_acnumber; ?></span></div></div>
                                                        </div>
                                                        <div class="deal_row">
                                                            <div class="deal_d2"><div class="deal_text">IFSC Code : <span><?php echo $value->s_ifsccode; ?></span></div></div>
                                                            <div class="deal_d2"><div class="deal_text">Bank Name  : <span><?php echo $value->s_bankname; ?></span></div></div>
                                                        </div>
                                                        <div class="deal_row">
                                                            <div class="deal_d2"><div class="deal_text">Branch Name : <span><?php echo $value->s_branchname; ?></span></div></div>
                                                            <div class="deal_d2"><div class="deal_text">Pin Code : <span><?php echo $value->s_pincode; ?></span></div></div>
                                                        </div>

                                                        <div class="deal_row">
                                                            <div class="deal_d1"> 
                                                            <div class="deal_text">Term & Condotion</div>
                                                            <div class="deal_text_b">
                                                            <?php echo $value->s_termcondition; ?>
                                                            </div></div>
                                                        </div>
                                                        <div class="deal_row">
                                                            
                                                            <div class="deal_d2"><div class="deal_text">Modified Date/Time : <span><?php echo $value->s_modifiedon; ?></span></div></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            
                                        </tr>
                                    </div>


                                    <?php $i++; } ?>               
                                    </tbody>
                                    </table>
                                    
                
            </div>

             
        </div>     
              

            </div>
        </div>
    </div>


</body>
</html>

