<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); 
    $getid=$this->uri->segment(4);
    $logintype=$this->session->userdata('lw_login')->s_logintype;
?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Edit Loyalty Program Details</h2></div>
                                    <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>

                        <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" id="loyalty" method="post" enctype="multipart/form-data">
                                     <div class="sep_box">
                                            
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Category <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <?php 
                                                foreach ($category as $key => $value) 
                                                { 
                                                  if($view->p_catid==$value->cat_id)
                                                  {
                                                    echo $value->cat_name; 
                                                    echo '<input type="hidden" name="category" value="'.$value->cat_id.'"/>'; 
                                                  }
                                                }
                                               ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     

                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Valid Till<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="edate" class="sdate" value="<?php echo $view->p_edate; ?>" readonly/>
                                                            
                                                        </div>
                                                    </div>
                                                  
                                                </div>
                                            </div>

                                              
                                    </div>

                                    <div class="sep_box">
                                            
                                        <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Earn<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="tbl_input">
                                                         <input type="text" name="odayvalue_earnc" id="getval" value="<?php echo $view->p_odayvalue_earnc; ?>"/>
                                                         <?php foreach ($country as $key => $value) { 
                                                            echo $value->csign; } ?>
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="tbl_input">
                                                        
                                                         <input type="text" name="oftypevalue_earnp" id="putval" value="<?php echo $view->p_oftypevalue_earnp; ?>"/>Points
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 

                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Redeem<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="tbl_input">
                                                         <input type="text" name="oname_burnc" id="oname_burnc" value="<?php echo $view->p_oname_burnc; ?>"/>
                                                         <?php foreach ($country as $key => $value) { 
                                                            echo $value->csign; } ?>
                                                       
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="tbl_input">
                                                        
                                                         <input type="text" name="oimg_burnp" id="oimg_burnp" value="<?php echo $view->p_oimg_burnp; ?>"/>Points
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 

                                            
                                            
                                        </div> 
                                        
                                         

                                             <div class="sep_box">
                                              <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Terms of Use<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <textarea name="term"><?php echo $view->p_term; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">How it Works?<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="how"><?php echo $view->p_how; ?></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                          
                                         
    
                                            </div>

                                             <div class="sep_box">
                                                 <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">FAQs<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="faq"><?php echo $view->p_faq; ?></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="program" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div>
                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />

<script type="text/javascript">
   

    $('.sdate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });
   
   
</script>   

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
    $("#loyalty").validate({
          // Specify the validation rules
          rules: {
              category: "required",
              edate: "required",
              odayvalue_earnc:{required:true},
              oftypevalue_earnp:{required:true, number:true},
              oname_burnc:{required:true},
              oimg_burnp:{required:true, number:true},
              how : "required",
              term : "required",
              faq : "required"

          },
          
          messages: {
            
              category : "Please select category!",
              edate : "Please select valid Date!",
              odayvalue_earnc : {required:"Please enter currency!"},
              oftypevalue_earnp : {required:"Please enter points!", number:"Numbers only!"},
              oname_burnc : {required:"Please enter currency!"},
              oimg_burnp : {required:"Please enter points!", number:"Numbers only!"},
              how : "please enter how it works!",
              term : "please enter term of use!",
              faq : "please enter FAQs!"
              
          },
          
          
      });
   });
                                                      
</script>
