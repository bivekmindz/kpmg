<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); 
    $getid=$this->uri->segment(4);
    $logintype=$this->session->userdata('lw_login')->s_logintype;
    if($logintype==3)
    {
        $url=base_url('admin/program/programoutlet/LP');
    }
    else
    {
        $url=base_url('admin/program/programlist').'/LP/'.@$getid;
    }?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Loyalty Program Details</h2>
                       <a href="<?php echo $url; ?>"><button type="button" style="float:right;">Loyalty Program List</button></a> </div>
                                    <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                           
                       
                      
                      
 <style>
    .retailer_nav {
        width:100%;
        float:left;
    }
    .retailer_nav ul {
        padding:0;
        margin:0;
    }
    .retailer_nav ul li {
        list-style:none;
        float:left;
    }
    .retailer_nav ul li a {
        display:block;
        float:left;
        padding:10px 20px;
        background:#fff;
        margin-right:5px;
        color:#333;
        text-decoration:none;   
    }
   

 .add_btn {
    width: auto;
    height: auto;
    float: right;
    margin-right: 15px;
}
 
   .add_btn a {
       display:block;
       padding:10px 15px;
       background:#d84d28;
       color:#FFF;
       float:left;
       text-decoration:none;
       border-radius:5px;
       
   }
.add_100 {
    width:100%;
    float:left
}
 </style>
 
         
<div class="retailer_nav">
      <ul> <?php 
    if($logintype==3)
    {
        echo '<li><a href="javascript:void(0)" class="active_color">Loyalty Program</a></li>';
        echo '<li><a href="'.base_url('admin/program/offer').'/'.@$getid.'" >Offers</a></li>';
        echo '<li><a href="'.base_url('admin/program/punch').'/'.@$getid.'" >Punch Card</a></li>';
    }
    else
    {
        echo '<li><a href="'.base_url('admin/merchant/addmerchant').'/'.@$getid.'">Merchant</a></li>';
        echo '<li><a href="'.base_url('admin/merchant/outlet').'/'.@$getid.'">Outlet</a></li>';
        echo '<li><a href="'.base_url('admin/merchant/document').'/'.@$getid.'" >Documents</a></li>';
        //echo '<li><a href="'.base_url('admin/program/currency').'/'.@$getid.'" >Currency</a></li>';
        echo '<li><a href="'.base_url('admin/program/card').'/'.@$getid.'" >Card</a></li>';
        echo '<li><a href="javascript:void(0)" class="active_color">Loyalty Program</a></li>';
        echo '<li><a href="'.base_url('admin/program/offer').'/'.@$getid.'" >Offers</a></li>';
        echo '<li><a href="'.base_url('admin/program/punch').'/'.@$getid.'" >Punch Card</a></li>';
    }
      
?>
      </ul>
</div>
      
            
                        <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" id="loyalty" method="post" enctype="multipart/form-data">
                                     <div class="sep_box">
                                            
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <?php 
                                              if($logintype==3)
                                              {
                                                foreach ($category as $key => $value) 
                                                { 
                                                  if($value->cat_id==$this->session->userdata('lw_login')->catid)
                                                  {
                                                    echo $value->cat_name; 
                                                    echo '<input type="hidden" name="category" value="'.$value->cat_id.'"/>'; 
                                                  }
                                                }
                                              }
                                              else
                                              { ?>
                                                           <select id="category" name="category">
                                                            <option value="">Select Category</option>
                                                            <?php foreach ($category as $key => $value) { ?>

                                                            <option value="<?php echo $value->cat_id; ?>">
                                                             <?php echo $value->cat_name; ?></option>
                                                          
                                                            <?php } ?>
                                                             </select>
                                                             <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     

                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Valid Till<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="edate" class="sdate" placeholder="Valid till 31st Dec,2018" readonly/>
                                                            
                                                        </div>
                                                    </div>
                                                  
                                                </div>
                                            </div>

                                              
                                    </div>

                                    <div class="sep_box">
                                            
                                        <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Earn<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="tbl_input">
                                                         <input type="text" name="odayvalue_earnc" id="getval" placeholder="Currency" onkeyup="setpoint1()"/>
                                                         <?php foreach ($country as $key => $value) { 
                                                            echo $value->csign; } ?>
                                                        
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="tbl_input">
                                                        
                                                         <input type="text" name="oftypevalue_earnp" id="putval" placeholder="Points"/>Points
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 

                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Redeem<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="tbl_input">
                                                         <input type="text" name="oname_burnc" id="oname_burnc" placeholder="Currency" onkeyup="setpoint1()"/>
                                                         <?php foreach ($country as $key => $value) { 
                                                            echo $value->csign; } ?>
                                                       
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="tbl_input">
                                                        
                                                         <input type="text" name="oimg_burnp" id="oimg_burnp" placeholder="Points"/>Points
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 

                                            
                                            
                                        </div> 
                                        
                                         

                                             <div class="sep_box">
                                              <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Terms of Use<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                          <textarea name="term"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">How it Works?<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="how"></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                           
                                          
                                         
    
                                            </div>

                                             <div class="sep_box">
                                                 <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">FAQs<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">


                                                            <textarea name="faq"></textarea>
                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="loyaltysubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div>
                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />

<script type="text/javascript">
   

    $('.sdate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });
   
   
</script>   

<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
    $("#loyalty").validate({
          // Specify the validation rules
          rules: {
              category: "required",
              edate: "required",
              odayvalue_earnc:{required:true},
              oftypevalue_earnp:{required:true, number:true},
              oname_burnc:{required:true},
              oimg_burnp:{required:true, number:true},
              how : "required",
              term : "required",
              faq : "required"

          },
          
          messages: {
            
              category : "Please select category!",
              edate : "Please select valid Date!",
              odayvalue_earnc : {required:"Please enter currency!"},
              oftypevalue_earnp : {required:"Please enter points!", number:"Numbers only!"},
              oname_burnc : {required:"Please enter currency!"},
              oimg_burnp : {required:"Please enter points!", number:"Numbers only!"},
              how : "please enter how it works!",
              term : "please enter term of use!",
              faq : "please enter FAQs!"
              
          },
          
          
      });
   });


function setpoint()
{
    var cur='<?php echo $value->cs_currency; ?>';
    var point='<?php echo $value->cs_value; ?>';
    var get=$('#getval').val();
    var fc=get/cur;
    var putval=fc*point;
    var get=$('#putval').val(putval.toFixed(2));
    //alert(cur);
}
                                                         
</script>
