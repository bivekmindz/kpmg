<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Promotion</h2>
                        </div>
                     
                                    <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                        

    
                        <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" id="cardpro1" method="post" enctype="multipart/form-data">

                                      <div class="sep_box">
                                            <?php //for admin and adminemployees
                                                if($this->session->userdata('lw_login')->s_logintype!=2){
                                            ?> 
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Select Country <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <select name="country" id="country" onchange="selectmerchant();" >
                                                                <option value="">Select Country</option>
                                                                <?php foreach ($viewcountry as $value) { ?>
                                                                    <option value="<?php echo $value->conid ; ?>"><?php echo $value->name ; ?></option>
                                                                <?php } ?>
                                                                
                                                            </select>
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div>
                                            <?php } else { //for merchant & outlet ?>
                                                    <input type="hidden" name="country" id="country" value="<?php echo $this->session->userdata('lw_login')->s_countryid; ?>" />

                                              <?php } ?>


                                            <?php //for admin and adminemployees
                                              if($this->session->userdata('lw_login')->s_logintype!=2) { ?>
                                            <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text" >Company Name<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                    
                                                            <select onchange="merchantcategory()" name="merchantid" id="merchantid">
                                                                <option value="">Select Company Name</option>
                                                                                                                         
                                                            </select>
                                                        </div>
                                                    </div>                                     
                                                </div>
                                            </div>
                                            <?php } else { ?>
                                              
                                            <input type="hidden" name="merchantid" id="merchantid" value="<?php echo $this->session->userdata('lw_login')->s_loginid; ?>" />
                                            
                                            <?php  } ?>

                                        </div>


                                     <div class="sep_box">
                                            
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select Category <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select id="category" name="category" required>
                                                            <option value="">Select Category</option>
                                                            <?php 
                                                        if($this->session->userdata('lw_login')->s_logintype==2) {
                                                            foreach ($category as $key => $value) { ?>

                                                            <option value="<?php echo $value->cat_id; ?>">
                                                             <?php echo $value->cat_name; ?></option>
                                                          
                                                            <?php } } ?>
                                                             </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     

                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Paid Amount</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="amount" id="amount"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                              
                                    </div>

                                    <div class="sep_box">
                                            
                                        <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Promotion Description</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <textarea name="cdesc"  id="cdesc"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Promotion Image<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="file" name="cardimage" id="cardimage" required/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             

                                        </div> 
                                        
                                            <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Start Date<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                               <input type="text" name="sdate" class="sdate" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">End Date<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                             <input type="text" class="edate" name="edate">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>

                                            
                                          

                                             <div class="sep_box">
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="cardsubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div>
                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>





<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />

<script type="text/javascript">
   

    $('.sdate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });
   
    $('.edate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });


    


</script>   



<script type="text/javascript">

  $(document).ready(function(){
    $("#cardpro").validate({
          // Specify the validation rules
          rules: {
              category: "required",
              amount:{required:true, number:true},
              //cardimage :{required: true,accept: "jpg,png,jpeg"},
              sdate: "required",
              edate : "required"
          },
          
          messages: {
            
              category : "Please select category!",
              amount : {required:"Please enter paid amt.!", number:"Numbers only!"},
             // cardimage :  {required: 'please choose card image!', accept: 'Only images with type jpg/png/jpeg are allowed'},
              sdate : "please select start date!",
              edate : "please select end date!"
          },
          
          submitHandler: function(form) {
           
          }
      });
   });


function selectmerchant(){
    var countryid= $('#country').val();
    //alert(countryid);
    $.ajax({
        url: '<?php echo base_url(); ?>admin/offer/selectmerchant',
        type: 'POST',
        data: { 'countryid':countryid },
        success: function(data){
            //alert(data);
            var  option_brand = '<option value="">Select Merchant</option>';
            $('#merchantid').empty();
            $('#merchantid').append(option_brand+data); 
        }
    });
 }

 function merchantcategory() {
    var retailerid = $('#merchantid').val();
    $.ajax({
    url: '<?php echo base_url(); ?>admin/offer/merchantcategory',
    type: 'POST',
    data: {'retailerid':retailerid},
    success: function(data){
        
    var  option_brand = '<option value="">Select Category</option>';
        //alert(data);
        $('#category').empty();
        $('#category').append(option_brand+data); 
        

    }
    });  
 }
</script>
