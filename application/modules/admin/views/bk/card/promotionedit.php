<!DOCTYPE html>
<html>
<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                
               
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Update Promotion</h2>
                        </div>
                       <p style="color:green;text-align:center;">
                                    
                                    <?php
                                      if($this->session->flashdata('message')){
                                        echo $this->session->flashdata('message'); 
                                      }
                                    ?>
                                </p>
                           
 
    
                        <div class="page_box" id="retailerdiv">
                            <div class="sep_box">
                                <div class="col-lg-12">
                                    <div class="row">
                                   <form action="" id="cardpro1" method="post" enctype="multipart/form-data">


                                     <div class="sep_box">
                                            
                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Category</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <?php echo $card->cat_name; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     

                                                <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Paid Amount</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="text" name="amount" value="<?php echo $card->ppc_paidamt; ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                              
                                    </div>

                                    <div class="sep_box">
                                            
                                        <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Promotion Description</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <textarea name="cdesc"  id="cdesc"><?php echo $card->ppc_desc; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Promotion Image</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input type="file" name="cardimage" id="cardimage" />
                                                            <input type="hidden" name="cardimage1" value="<?php echo $card->ppc_image; ?>"/>
                                                 <img src="<?php echo base_url().'public/card/'.$card->ppc_image; ?>" alt="Merchant" width="50px;" height="50px;">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             

                                        </div> 
                                        
                                            <div class="sep_box">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Start Date</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                               <input type="text" name="sdate" value="<?php echo $card->ppc_sdate; ?>" class="sdate" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">End Date</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                             <input type="text" class="edate" value="<?php echo $card->ppc_edate; ?>" name="edate">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>

                                            
                                          

                                             <div class="sep_box">
                                             <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <input type="submit" name="cardsubmit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  

                                        </div>  
                                          </form>                          

                                    </div>

                                </div>


                            </div>
                        </div>
                        
                        
                    </div>
  </div>
                                        
                                           
 </div>
  
            </div>
        </div>
    </div>
</body>
   
</html>





<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>  
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>

<script src="<?php echo base_url();?>assets/js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.timepicker.css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-datepicker.standalone.css" />

<script type="text/javascript">
   

    $('.sdate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });
   
    $('.edate').datepicker({
    'format': 'dd-mm-yyyy',
    'autoclose': true,
    todayBtn: "linked",
    startDate: '+0d',
    todayHighlight: true
    });


    


</script>   

