<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>
 <link rel="stylesheet" href="<?php echo base_url();?>assets/css/viewer.css">
   <script src="<?php echo base_url();?>assets/js/viewer.js"></script>
  <script src="<?php echo base_url();?>assets/js/main.js"></script>

<!DOCTYPE html>

<html>

<body>

    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
            
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Promotion List</h2>
                       
                                    <?php $logintype=$this->session->userdata('lw_login')->s_logintype;
                      if($logintype==2) 
                      { 
                                  echo '<a href="'.base_url('admin/card/addpromotion').'" style="float:right;margin-left: 10px;"><button type="button">ADD Promotion</button></a>';
                                   
                                  } ?> </div>
                                   <p style="color:green"><?php  echo $this->session->flashdata('message'); ?></p>
                           
                      
           
               <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('#example').DataTable();
                                    $( ".grid_tbl" ).wrap( "<div class='new_width'></div>" );
                                    } );
                                </script>           
                        
                        
                
                            
      
 
        <div class="page_box" id="storelistingdiv">
    
             
          



             <table class="grid_tbl" id="example">
                                    <thead>
                                        <tr>
                                            <th bgcolor="red">S.No.</th>
                                               
                                                <th bgcolor="red">Image</th>
                                                <?php if($this->session->userdata('lw_login')->s_logintype!=2){ ?>
                                                <th bgcolor="red">Merchant ID</th>
                                                <!-- <th bgcolor="red">Company Name</th>
                                                <th bgcolor='red'>Contact No.</th>
                                                <th bgcolor="red">Email ID</th> -->
                                                <th bgcolor="red">Country</th>
                                                <?php } ?>
                                                <th bgcolor="red">Category</th>
                                                <th bgcolor="red">Paid Amt.</th> 
                                                <th bgcolor="red">Start/End Date</th>
                                                <th bgcolor="red">Create Date</th>
                                                <!-- <th bgcolor="red">Status</th> -->
                                                <th bgcolor="red">Action</th>
                                               
                                        </tr>
                                    </thead>
                                    
                                    
                                    <tbody class="append"> 
                                    <?php 
                                        $i = 1;
                                            foreach ($list as $key => $value) { ?>
                                            <tr class="append_wrapper">


                                       <td><?php echo $i; ?></td>
                                        <td> <ul class="docs-pictures clearfix">
            <li><img data-original="<?php echo base_url().'public/card/'.$value->ppc_image; ?>" src="<?php echo base_url().'public/card/'.$value->ppc_image; ?>" alt="Merchant" width="50px;" height="50px;">
            </li>
            
          
          </ul>
                                         
                                        </td>
                                         <?php if($this->session->userdata('lw_login')->s_logintype!=2){ ?>
                                        <td><?php echo $value->mrid; ?></td> 
                                       <!--  <td><?php echo $value->s_name; ?></td> 
                                        <td><?php echo $value->s_mobile; ?></td> 
                                        <td><?php echo $value->s_email; ?></td>  -->
                                        <td><?php echo $value->name; ?></td> 
                                        <?php } ?>
                                        <td><?php echo $value->cat_name; ?></td> 
                                        <td><?php echo $value->ppc_paidamt; ?></td> 
                                        <td><?php echo $value->ppc_sdate.'/'.$value->ppc_edate; ?></td> 
                                        <td><?php echo $value->ppc_createon; ?></td> 
                                        <td> 
<?php if($value->ppc_status==2) 
      {  
        if($this->session->userdata('lw_login')->s_logintype!=2)
        { ?>
          <a href="<?php echo base_url();?>admin/card/promotionstatus/<?php echo $value->ppc_id;?>/P" ><span style="color:red">Unpublish</span></a> 
          <a href="<?php echo base_url();?>admin/card/editpromotion/<?php echo $value->ppc_id;?>" >Edit</a>
<?php   }
        else
        { 
          echo '<span style="color:red">Unpublish</span> ';
          echo '<a href="'.base_url().'admin/card/editpromotion/'.$value->ppc_id.'">Edit</a>';
        }
      } 
      else
      { 
        if($this->session->userdata('lw_login')->s_logintype!=2)
        { ?>
          <a href="<?php echo base_url();?>admin/card/promotionstatus/<?php echo $value->ppc_id;?>/U" ><span style="color:green">Published</span></a>
<?php   }
        else
        { 
          echo '<span style="color:green">Published</span>'; 
        } 
      } ?> 

                                         </td>
                                         
                                    </tr> 
                                    <?php $i++; } ?>               
                                    </tbody>
                                    </table>
                                    
                
            </div>

             
        </div>     
              
         </div>
        </div>
    </div>


</body>
 
</html>
