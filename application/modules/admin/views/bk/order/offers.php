<!DOCTYPE html>
<html>
<body>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/dataTables.bootstrap.min.js"></script>


     <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('#example').DataTable();
                                    } );
                                </script> 


    <div class="wrapper">
    <?php  $this->load->view('helper/sidebar'); 
    $logintype=$this->session->userdata('lw_login')->s_logintype; ?>   

        <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <div class="page_contant">
                    <div class="col-lg-12">
                        <div class="page_name">
                            <h2>Offers History</h2>
                        </div>
                       
                        <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Order ID</th>
                                               <!--  <th bgcolor='red'>User ID</th>
                                                <th bgcolor='red'>User Name</th> -->
                                                <th bgcolor='red'>Offer Type</th> 
                                                <?php if($logintype==2){ ?>
                                                <th bgcolor='red'>Outlet ID</th>
                                                <?php } else if($logintype==3){ }else { ?>
                                                <th bgcolor='red'>Merchant ID</th>
                                                <th bgcolor='red'>Outlet ID</th>
                                                <?php } ?>
                                                <th bgcolor='red'>Date/Time</th>            
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                            foreach ($vieww as $value) {
                                            $i++;
                                            
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->or_orderid; ?></td>
                                          <!--   <td><?php echo $value->user_uid; ?></td>
                                            <td><?php echo $value->username; ?></td> -->
                                            <td><?php echo $value->or_desc; ?></td>
                                            <?php if($logintype==2){ ?>
                                            <td><?php echo $value->outletid; ?></td>
                                            <?php } else if($logintype==3){ }else { ?>
                                            <td><?php echo $value->merid; ?></td>
                                            <td><?php echo $value->outletid; ?></td>
                                            <?php } ?>
                                            <td><?php echo date('d-m-Y h:i:s A',strtotime($value->or_createdon)); ?></td>
                                            

                                            
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
</body>  
</html>