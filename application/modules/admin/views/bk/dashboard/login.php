<!DOCTYPE HTML>
<html>
<head>
<link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<style type="text/css">
.form_1{ width:100%; float:left; padding: 7% 0% 6% 0%;}   
.form_1 .loginPageMain1{ width:100%; float:left;}

    
    
    .form-group{ width:100%; float:left;}
    .form-group form{ width:100%; float:left;}
    .form-signin {
    max-width: 400px;
    background-color: rgb(255, 255, 255);
    padding: 15px 35px 45px;
    margin: 0px auto;
    border-width: 1px;
    border-style: solid;
    border-color: rgba(0, 0, 0, 0.0980392);
    border-image: initial;
        border-radius: 5px;
        box-shadow: 0px 0px 4px 4px #e4e4e4;
}

.form-control{
    margin: 15px 0px 15px 0px;
        height: 50px !important;
}

.btn-primary{    color: #fff;
    background-color: #de1e43 !important;
    border-color: #424041 !important;
    }
.btn-primary:hover{color: #fff;
    background-color: #028543;
    }

    .radio_but{ width: 20px; height: 20px;     position: absolute;
    top: -4px;} 
    .spane{    padding: 0px 26px;}
    .posti{ width: 100%; float: left; position: relative;     padding: 0px 0px 20px 0px;}
</style>    
    <title>Kpmg - Login</title>

</head>
<body>


<section class="form_1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
    
     <form method="post" class="form-signin">         
      <h2 class="form-signin-heading to_p"><img src="<?php echo base_url();?>assets/KPMG.png" class="adminLogo">
      </h2>
        <input type="text" class="form-control" name="email" value="<?php echo set_value('email');?>" placeholder="Email Address">
        <input type="password" class="form-control" name="password" placeholder="Password">      
        <input type="submit" name="submit" value="Login" class="btn btn-lg btn-primary btn-block" > 
        <a href="<?php echo base_url('kpmg-forgot-password')?>"><span> Forgot Password?</span></a>
        <p><?php echo $this->session->flashdata('message'); ?>
            <?php echo validation_errors();  if(isset($result) && $result!=''){echo ($result);}?></p>
    </form>

            </div>
        </div>
    </div>
</section>
    
</body>
</html>