<div class="wrapper">
<?php $this->load->view('helper/sidebar')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    <h2>Modify Document Type</h2>
    </div>
        <div class="page_box">
            <div class="sep_box">
        <div class="col-lg-12">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/master/documenttype"><button>CANCEL</button></a>
        </div>
       
        </div></div>
        <form action="" id="addCont" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-6">
            <div class="row">
                <div class="col-lg-4">
                    <div class="tbl_text">Select Country<span style="color:red;font-weight: bold;">*</span></div>
                </div>
                <div class="col-lg-8">
                    <div class="tbl_input">
                       <!-- <select name="countryid" id="countryid" required>
                           <option value="">Select  Country</option>
                          <?php foreach ($viewcountry as $value){  ?>
                             <option value="<?php echo $value->conid; ?>" <?php if($vieww->d_country==$value->conid) echo 'selected'; ?> ><?php echo $value->name.' (+'.$value->code.')'; ?></option>                  
                          <?php } ?>
                       </select>  -->
                       <input  type="text" readonly name="countryid"   id="countryid" value="<?php echo $vieww->name.' (+'.$value->code.')'; ?>" />
                    </div>
                </div>
            </div>
        </div>    
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-4">
                <div class="tbl_text">Document Type <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input  type="text" name="storetype"   id="storetype" value="<?php echo $vieww->d_name; ?>" /></div>
            </div>
        </div>
        </div>

        
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-7">
                <div class="submit_tbl">
                    <input id="Submit1" type="submit" name="submit" value="Submit" class="retailer_btn"/>
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>