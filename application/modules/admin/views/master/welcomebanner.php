<div class="col-lg-10 col-lg-push-2">
            <div class="row">
            
            <div class="page_contant">

           
            

                    <div class="col-lg-12">
                        <div class="page_name">
                           <h2>Pre Login Screen</h2>
                        </div>
                   
                                <?php if($this->session->flashdata('message')) { ?>
                                    <div class="alert alert-success">
                                        <?php echo $this->session->flashdata('message'); ?>
                                    </div>
                                <?php } ?>

                        <div class="page_box">
                            <div class="sep_box">

                                <div class="col-lg-12">
                                 
                                    <div class="row">
                                

                          

 <form action="" id="addCont" method="post" enctype="multipart/form-data" >

  <!--    <div class="sep_box">
                                 
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                         <div class="tbl_text">Title</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">

                                                             
                                                <input type="text" name="title"/>                                                      
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                   <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                         <div class="tbl_text">Description</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">

                                                             
                                                <input type="text" name="descr"/>                                                      
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                             <div class="sep_box">
                                 
                                            <div class="col-lg-5">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                         <div class="tbl_text">Upload Screen <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">

                                                             
                                                <input type="file" name="image" id="banner_image" required/>              <span id="msg"></span>                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                             <div class="col-lg-5">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                         <div class="tbl_text">Screen Sequence</div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="tbl_input">
                                                                <select name="sequence">
                                                                <?php for($i=1; $i<=10; $i++ ){ ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php } ?>
                                                                    
                                                            </select>                                                      
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                   
                                            <div class="col-lg-2">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text"></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                            <input id="submitBtn" type="submit" name="submit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
 </form>

                       


                                    </div>

                                </div>


                            </div>
                        </div>

                        
                    </div>
                   


                    <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                             <!--    <th bgcolor='red'>Title</th>
                                                <th bgcolor='red'>Description</th> -->
                                                <th bgcolor='red'>Screen</th>
                                                <th bgcolor='red'>Screen Sequence</th>
                                                <th bgcolor='red'>Action</th>
                                                                             
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                            foreach ($images as $value) {
                                            $i++;
                                        ?>
                                        <tr>
                                           <td><?php echo $i; ?></td>
                                          <!--  <td><?php echo $value->w_title; ?></td>
                                           <td><?php echo $value->w_desc; ?></td> -->
                                          
                                           <td><img height="80px;" width="50px;" src="<?php echo base_url().'public/homebanner/'.$value->w_name ;?>" ></td>
                                          
                                            
                                                <td><?php echo $value->w_sequence; ?></td>
                                               <td><a href="<?php echo base_url()?>admin/master/delete_banner/<?php echo $value->w_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i> </a>
                                                <a href="<?php echo base_url()?>admin/master/banneredit/<?php echo $value->w_id?>"> <i class="fa fa-pencil fa-lg"></i></a>
                                                <?php if($value->w_status==1) echo '<span style="color:green">Active</span>'; else echo '<span style="color:red">Inactive</span>';?>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        </div>

        <script>
                $(document).ready(function (){ 
                $("#banner_image").change(function () { 
                  var val = $(this).val().toLowerCase(); 
                  var regex = new RegExp("(.*?)\.(jpeg|png|jpg)$"); 
                  if(!(regex.test(val))) { 
                  $(this).val(""); 
                  $("#msg").html("Unsupported file, only jpeg|png|jpg files are allowed!").css("color","red");
                  }
                }); 
                });
  </script>