 <div class="col-lg-10 col-lg-push-2">
            <div class="row">
               
            <div class="page_contant">
            <form action="" id="" method="post" enctype="multipart/form-data" >
             <div class="col-lg-12">
                        <div class="page_name">
                           <h2>Document Type </h2>
                        </div>
                       
                                <?php echo validation_errors(); ?> 
                                  <p style="color:red"><?php  echo $this->session->flashdata('message'); ?></p>
                          
                        <div class="page_box">
                            <div class="sep_box">

                                <div class="col-lg-12">
                                 
                                    <div class="row">
                                        <div class="sep_box">


                                          <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Select Country<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                           <select name="countryid" id="countryid" required>
                                                               <option value="">Select  Country</option>
                                                              <?php foreach ($viewcountry as $value){  ?>
                                                                 <option value="<?php echo $value->conid; ?>"><?php echo $value->name.' (+'.$value->code.')'; ?></option>                  
                                                              <?php } ?>
                                                           </select> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                      <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Document Type <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                <div class="tbl_input">
                                                <input type="text" name="store_name" id="store_name" />

                                                </div>
                                        </div>
                                          </div>
                                      </div>

                                    </div>  

                                        <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text"></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                              <input id="submitBtn" type="submit" name="submit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>

                                </div>


                            </div>
                        </div>

                        
                    </div>
                    </form>

                    <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Country</th>
                                                <th bgcolor='red'>Document Type</th>
                                              <th bgcolor='red'>Action</th>                                             
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                            foreach ($vieww as $value) {
                                            $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->name.' (+'.$value->code.')'; ?></td>
                                            <td><?php echo $value->d_name; ?></td>
                                         
                                            <td><a href="<?php echo base_url()?>admin/master/documenttypeupdate/<?php echo $value->d_id?>" ><i class="fa fa-pencil"></a></i>
                                   <!--  <a href="<?php echo base_url()?>admin/master/documenttypedelete/<?php echo $value->d_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a> -->
                                            <a href="<?php echo base_url(); ?>admin/master/documenttypestatus/<?php echo $value->d_id.'/'.$value->d_status; ?>">
                                                <?php if($value->d_status==1) echo "Active"; else echo "Inactive"; ?></a>

                                            </td>
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
        </div>
   