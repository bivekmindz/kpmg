<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            attname : "required",
         
        },
        // Specify the validation error messages
        messages: {
            attname : "Category name is required",
           
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>

 <div class="col-lg-10 col-lg-push-2">
            <div class="row">
                <!-- <form action="" method="post" enctype="multipart/form-data" > -->
            <div class="page_contant">
            <form action="" id="addCont" method="post" enctype="multipart/form-data" >
          <?php 

$attributes = array('class' => 'form', 'id' => 'myform');


?>

                    <div class="col-lg-12">
                        <div class="page_name">
                           <h2>Edit Category </h2>
                        </div>
                      
                        <div class="page_box">
                            <div class="sep_box">

                                <div class="col-lg-12">
                                 
                                    <div class="row">
                                        <div class="sep_box">


                                           <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Category Name<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                <div class="tbl_input">
                                                <input type="text" name="categorydeal_name" id="categorydeal_name" value="<?php echo $vieww->cat_name; ?>" />

                                                </div>
                                        </div>
                                          </div>
                                      </div>

                                        <div class="col-lg-6">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Category Image <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                <div class="tbl_input">
                    <img height="50px;" width="50px;" src="<?php echo base_url(); ?>public/cat_image/<?php echo $vieww->cat_image; ?>">
            <input type="hidden" name="catogary_Icon1" id="catogary_Icon"  value="<?php echo $vieww->cat_image; ?>" />     

                                                <input type="file" name="catogary_Icon" id="catogary_Icon" />

                                                </div>
                                        </div>
                                          </div>
                                      </div>

                          


                             <div class="sep_box">
                                            <div class="col-lg-8">
                                                  <div class="row ">
                                                    <div class="col-lg-3">
                                                        <div class="tbl_text">Location Map Icon<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-9">
                                                <div class="tbl_input">
                    <img height="50px;" width="50px;" src="<?php echo base_url(); ?>public/cat_image/<?php echo $vieww->cat_icon; ?>">
            <input type="hidden" name="location_icon1" value="<?php echo $vieww->cat_icon; ?>" />     

                                                <input type="file" name="location_icon" id="location_icon1" />

                                                </div>
                                        </div>
                                          </div>
                                            </div>
                                           
                                            <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text"></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                              <input id="submitBtn" type="submit" name="submit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>  


                                          <?php  ?>

                       


                                    </div>

                                </div>


                            </div>
                        </div>

                        
                    </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
   