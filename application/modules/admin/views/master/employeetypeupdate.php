<div class="wrapper">
<?php $this->load->view('helper/sidebar')?> 
<div class="col-lg-10 col-lg-push-2">
 <div class="row">
  <div class="page_contant">
    <div class="col-lg-12">
    <div class="page_name">
    <h2>Modify Employee Type</h2>
    </div>
        <div class="page_box">
            <div class="sep_box">
        <div class="col-lg-12">
        <div style="text-align:right;">
            <a href="<?php echo base_url();?>admin/master/employeetype"><button>CANCEL</button></a>
        </div>
        <!--  session flash message  -->
        <div class='flashmsg'>
            
            <?php echo validation_errors(); ?>
            <?php
              if($this->session->flashdata('message')){
                echo $this->session->flashdata('message'); 
              }
            ?>
        </div>
        </div></div>
        <form action="" id="addCont" method="post" enctype="multipart/form-data" >
        <div class="sep_box">
        <div class="col-lg-8">
        <div class="row">
            <div class="col-lg-2">
                <div class="tbl_text">Employee Type <span style="color:red;font-weight: bold;">*</span></div>
            </div>
            <div class="col-lg-8">
                <div class="tbl_input"><input  type="text" name="storetype"   id="storetype" value="<?php echo $vieww->et_name; ?>" /></div>
            </div>
        </div>
        </div>

        
        </div>
        <div class="sep_box">
        <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-7">
                <div class="submit_tbl">
                    <input id="Submit1" type="submit" name="submit" value="Submit" class="retailer_btn"/>
                </div>
            </div>
        </div>
        </div>

        </div>
        </form>
        </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>