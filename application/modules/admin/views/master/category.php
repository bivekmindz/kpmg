  <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url()?>assets/js/jquery-1.9.1.js"></script>
  <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
  
  <!-- jQuery Form Validation code -->
  <style>
    input.error{border:1px solid red;}
    label.error{border:0px solid red; color:red; font-weight: normal; display:inline; }
  </style>

  <script language='javascript'>
  
  // When the browser is ready...
  $(document).ready(function() {
  
  // Setup form validation on the #register-form element
    $("#addCont").validate({
        // Specify the validation rules
        rules: {
            attname : "required",
         
        },
        // Specify the validation error messages
        messages: {
            attname : "Category name is required",
          
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>

 <div class="col-lg-10 col-lg-push-2">
            <div class="row">
             
            <div class="page_contant">
            <form action="" id="addCont" method="post" enctype="multipart/form-data" >
          <div class="col-lg-12">
                        <div class="page_name">
                           <h2>Add Category </h2>
                        </div>
                        
                                <?php echo validation_errors(); ?> 
                                  <p style="color:red"><?php  echo $this->session->flashdata('message'); ?></p>
                           
                        <div class="page_box">
                            <div class="sep_box">

                                <div class="col-lg-12">
                                 
                                    <div class="row">
                                        <div class="sep_box">
                                         <div class="col-lg-5">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Category Name<span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                <div class="tbl_input">
                                                <input type="text" name="categorydeal_name" id="categorydeal_name" required/>

                                                </div>
                                        </div>
                                          </div>
                                      </div>

                                         <div class="col-lg-5">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Image <span style="color:red;font-weight: bold;">*</span></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                <div class="tbl_input">
    <input type="file" name="catogary_Icon" id="catogary_Icon" required/>

                                                </div>
                                <span id="msg3" style="color:green"></span>
                                    <span id="msg2" style="color:red"></span>
                                        </div>
                                          </div>
                                      </div>

                                       <div class="col-lg-5">
                                                <div class="row ">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text">Location Map Icon</div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                <div class="tbl_input">
    <input type="file" name="location_icon" id="location_icon"/>

                                                </div>
                                <span id="msg3" style="color:green"></span>
                                    <span id="msg2" style="color:red"></span>
                                        </div>
                                          </div>
                                      </div>

                                            <div class="col-lg-2">
                                                <div class="row">
                                                    <div class="col-lg-4">
                                                        <div class="tbl_text"></div>
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <div class="tbl_input">
                                                              <input id="submitBtn" type="submit" name="submit" value="Submit" class="retailer_btn"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>  


                                    </div>

                                </div>


                            </div>
                        </div>

                        
                    </div>
                    </form>

                    <div class="page_box">
                            <div class="col-lg-12">
                                <div class="gridview">
                                    <table class="grid_tbl" id="example">
                                        <thead>
                                            <tr>
                                                <th bgcolor='red'>S.No.</th>
                                                <th bgcolor='red'>Category Name</th>
                                               <th bgcolor='red'>Category Image</th>
                                               <th bgcolor='red'>Location Map Icon</th>
                                                
                                                <th bgcolor='red'>Action</th>                                             
                                            </tr>
                                        </thead>
                                        <tbody>

                                        <?php $i=0;
                                            foreach ($vieww as $value) {
                                            $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $value->cat_name; ?></td>
                                            <td> <?php if($value->cat_id!=1){ ?>
                                            <img width="50px;" height="50px;" src="<?php echo base_url().'public/cat_image/'.$value->cat_image; ?>" >
                                                 <?php } ?>
                                            </td>
                                            <td> <?php if($value->cat_id!=1){ ?>
                                            <img width="50px;" height="50px;" src="<?php echo base_url().'public/cat_image/'.$value->cat_icon; ?>" >
                                                 <?php } ?>
                                            </td>
                                            
                                            <td><?php if($value->cat_id!=1){ ?>
                                                <a href="<?php echo base_url()?>admin/master/updatecategory/<?php echo $value->cat_id?>" ><i class="fa fa-pencil"></a></i>
                                            <a href="<?php echo base_url()?>admin/master/deletcategory/<?php echo $value->cat_id?>" onclick="return confirm('Are you sure to delete?');"><i class="fa fa-trash fa-lg"></i></a>
                                            <?php } ?>
                                            <a href="<?php echo base_url(); ?>admin/master/statuscatogery/<?php echo $value->cat_id.'/'.$value->cat_status; ?>"><?php if($value->cat_status==1) echo 'Active'; else echo '<span style="color:red;">Inactive</span>'; ?></a>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
        </div>
   
<script>
    
$("#msg2").hide();
$("#msg3").hide();

$(document).ready(function(){
    

$("#catogary_Icon").change(function(){
    var filename = $("#catogary_Icon").val();
    var extension = filename.replace(/^.*\./, '');
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    var size = $("#catogary_Icon")[0].files[0].size;
    
        if(size > 200000){
            $("#msg3").hide();
            $("#msg2").show();
            $("#msg2").text('Image Must be less than 200kb.');
             
            $("#catogary_Icon").val('');
        }else if($.inArray(extension, fileExtension) == -1) {
            $("#msg3").hide();
            $("#msg2").show();
            $("#msg2").text('Image file is not valid!.');
            
            $("#catogary_Icon").val('');
        }else{
            $("#msg3").show();
            $("#msg2").hide();
            $("#msg3").text('Image is ok.');
            //alert('in array');
        }
     

})

})
    
</script>