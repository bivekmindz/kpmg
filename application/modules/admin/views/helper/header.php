<!DOCTYPE html>
<html>
<head>

    <title>KPMG</title>

    <link rel='shortcut icon' href="<?php echo base_url();?>assets/website/images/ngage-logo-big2.png" />
    <link href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/fontawsome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?php echo base_url() ?>assets/css/StyleSheet.css" rel="stylesheet" />
   <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/JavaScript.js"></script>


    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/jquery-ui.css">
    <script src="<?php echo base_url() ?>assets/js/jquery-ui.js"></script>

   
    
    
    <style type="text/css">
        .left_n{
            position: absolute;
            
        }

        .new_width{
          float:left;
          width:100%;
          overflow-x:auto;
          padding-bottom: 10px; 
        }

        .loader {
          position: fixed;  
          left: 0px;  
          top: 0px;  
          width: 100%;  
          height: 100%;  
          z-index: 9999;  
          background: url("<?php echo base_url(); ?>loding.gif") 50% 50% no-repeat rgb(249,249,249);}

          .active_subnav{
 position: relative;

}
.active_subnav:before{
        content: "";
    position: absolute;
    border-right: solid 18px #f5f5f5;
    border-top: solid 12px transparent;
    border-bottom: solid 12px transparent;
    right: 0px;
    top: 50%;
    transform: translateY(-50%);
}

.submit_btn {
    outline: none;
    border: 0;
    padding: 10px 25px;
    background: #333;
    color: #FFF;
}   
.msg{    color: green;
    border-radius: 5px;
    text-align: center;
    border-style: double;}
</style>
   
</head>
<body>
 <!--  <div class="loader"></div> -->
    <div class="wrapper">
        <div class="col-lg-12">
            <div class="row">
                <div class="top_header">
                    <div class="col-lg-4">
                        <span class="menu-sm hidden-lg"> <i class="fa fa-align-justify menu-sm-b"></i></span>
                        <a href="<?php echo base_url('kpmg-dashboard') ?>" class="logo_admin">
                            
                                <img src="<?php echo base_url();?>assets/KPMG.png" class="adminLogo">
                        </a>
                    </div>
                    <div class="col-lg-8">

                    
                    <a href="<?php echo base_url('kpmg-change-password'); ?>" style="color: #444;font-size: 13px;padding: 20px 0px;float: right;text-decoration:none;" id="chpwd"><i class="fa fa-key"></i> Change Password</a>


                    <?php $logintype=$this->session->userdata('lw_login')->s_logintype;


                   // $emptype=$this->session->userdata('lw_login')->s_merchantid;
  //                 $emp= empType($emptype);
//p($emp);
                   ?>
                        

                        <a class="user" href="<?php echo base_url('kpmg-logout') ?>" style="color: #444;font-size: 13px;padding: 20px 0px;float: right;text-decoration:none;margin-left:30px; margin-right:30px;">
                            <i class="fa fa-user" style="margin-right:5px;"></i>Logout
                        </a>
                        
   </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
  $( document ).ready(function() {
   //debugger;
    $('.active_subnav').parent().parent().addClass('active_m');
       var all_height = [];
        var total = 8;

        $(".active_m > .sub_menu > li").each(function () {
            var li_height = $(this).height();
            all_height.push(li_height);
            total += li_height << 0;
        });
       // console.log(all_height);
       // console.log(total);
  
        $(".active_m > .sub_menu").height(total+ "px");
});
</script>
