<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Master extends MX_Controller{
  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    //$this->load->library('PHPExcel');
    //$this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }
 

public function offertypeadd(){  
    $this->userfunction->loginAdminvalidation();
    
    if($this->input->post('submit')){
   
       $name      = $this->input->post('couname');

       $parameter = array('act_mode'=>'offertypeexists','row_id'=>'','counname'=>$name,'coucode'=>'','commid'=>'');
                 
     
      $record['record1']= $this->supper_admin->call_procedureRow('proc_master',$parameter);

      if(empty($record['record1']))
      {
        $parameter = array('act_mode'=>'offertypeadd','row_id'=>'','counname'=>$name,'coucode'=>'','commid'=>'');
        
          
          $record['data'] = $this->supper_admin->call_procedure('proc_master',$parameter);
        
       
       @$this->session->set_flashdata('message', 'Your information was successfully Saved.');
       @redirect(base_url().'admin/master/offertypeadd');
     }
     else
     {
      @$this->session->set_flashdata('message', 'Offer type already exists.');
       @redirect(base_url().'admin/master/offertypeadd');
     }

  }
    
    $parameter = array('act_mode'=>'offertypelist','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
        
  
    $record['data'] = $this->supper_admin->call_procedure('proc_master',$parameter);


    $this->load->view('helper/header');

    $this->load->view('helper/sidebar');
    $this->load->view('master/offertypeadd',$record);

  }


 public function offertypedelete($id){  
    $this->userfunction->loginAdminvalidation();
    $parameter = array('act_mode'=>'offertypedelete','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
    $record= $this->supper_admin->call_procedureRow('proc_master',$parameter);
   
    $this->session->set_flashdata('message', 'Your information was successfully Delete.');
       redirect(base_url().'admin/master/offertypeadd');
  }

  public function offertypeupdate($id){  
    $this->userfunction->loginAdminvalidation();

    if($this->input->post('submit')){
   
       $name      = $this->input->post('couname');
       $parameter = array('act_mode'=>'offertypeupdate','row_id'=>$id,'counname'=>$name,'coucode'=>'','commid'=>'');
      
       $record['data'] = $this->supper_admin->call_procedureRow('proc_master',$parameter);
        
       
       $this->session->set_flashdata('message', 'Your information was successfully Saved.');
       redirect(base_url().'admin/master/offertypeadd');

  }  
    $parameter = array('act_mode'=>'offertypeget','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
      
   
    $record['data'] = $this->supper_admin->call_procedureRow('proc_master',$parameter);
    //p($record['data']); die;
    $this->load->view('helper/header');

    $this->load->view('helper/sidebar');
    $this->load->view('master/offertypeupdate',$record);
    
  }
  public function offertypestatus (){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  //p($rowid);
  //p($status); die;
  $act_mode      = $status==1?'offertypeinactive':'offertypeactive';

  $parameter = array('act_mode'=>$act_mode,'row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
  $record['data'] = $this->supper_admin->call_procedureRow('proc_master',$parameter);
  
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/master/offertypeadd');
}


public function outlettype(){
  
    if($this->input->post('submit')){

     // Validation form fields.
     $this->form_validation->set_rules('store_name','Outlet Type Name','trim|required|xss_clean');

     if($this->form_validation->run() != false){
      $storename = $this->input->post('store_name');
      
       $parameter = array('act_mode'=>'countstoretype','row_id'=>'','counname'=>$storename,'coucode'=>'','commid'=>'');

     //  p($parameter); exit;
      
      $record= $this->supper_admin->call_procedureRow('proc_master',$parameter);
      //p($record->count); exit;
      if($record->count==0){
     

            $parameter = array('act_mode'=>'storeinsert','row_id'=>'','counname'=>$storename,'coucode'=>'','commid'=>'');
           //p($parameter); exit;

          $recor= $this->supper_admin->call_procedureRow('proc_master',$parameter);
          
          $this->session->set_flashdata("message", "Your information has been saved successfully!");
          @redirect("admin/master/outlettype");

      }
      else{
            
        $this->session->set_flashdata("message", "Outlet Type Already Exists");
         @redirect("admin/master/outlettype");
       
      }
     }
    }
    
  
   
      $parameter = array('act_mode'=>'liststoretype','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_master', $parameter);
   //p($response['vieww']); exit;

    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('master/outlettype',$response);
    
  }//End function.viewcatogary


  public function storetypedelete($id){
    $parameter = array('act_mode'=>'storetypedelete','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
    $response  = $this->supper_admin->call_procedure('proc_master',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect(base_url().'admin/master/outlettype');
}


public function storetypeupdate($id){
  if($this->input->post('submit')){

   // Validation form fields.
  $this->form_validation->set_rules('storetype','Outlet Type','trim|required|xss_clean');
  
     
   if($this->form_validation->run() != false){
     $name      = $this->input->post('storetype');
    
     $parameter = array('act_mode'=>'storeupdate','row_id'=>$id,'counname'=>$name,'coucode'=>'','commid'=>'');
     $response  = $this->supper_admin->call_procedure('proc_master',$parameter);
     $this->session->set_flashdata('message', 'Your information was successfully Update.');
     redirect(base_url().'admin/master/outlettype');
   }
  
  }

  $parameter         = array('act_mode'=>'storetypelisting','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww'] = $this->supper_admin->call_procedureRow('proc_master',$parameter);
 
  //p($response['vieww']); exit;
  $this->load->view('helper/header');
  $this->load->view('master/outlettypeedit',$response);
}


public function storetypestatus(){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='Active'?'activeandinactive':'inactiveandinactive';
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
    
//p($parameter); exit;
  $response      = $this->supper_admin->call_procedure('proc_master',$parameter);
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/master/outlettype');
}

public function documenttype(){

    $parameter             = array('act_mode'=>'viewactivecountry', 'row_id'=>'', 'counname'=>'', 'coucode'=>'', 'commid'=>'');
    $response['viewcountry'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  
    if($this->input->post('submit')){

     // Validation form fields.
     $this->form_validation->set_rules('store_name','Document Type Name','trim|required|xss_clean');

     if($this->form_validation->run() != false){
      $storename = $this->input->post('store_name');
      $countryid = $this->input->post('countryid');
       
       $parameter = array('act_mode'=>'count_document','row_id'=>'','counname'=>$storename,'coucode'=>$countryid,'commid'=>'');

     //  p($parameter); exit;
      
      $record= $this->supper_admin->call_procedureRow('proc_master',$parameter);
      //p($record->count); exit;
      if($record->count==0){
     

            $parameter = array(  'act_mode'=>'insert_document',
                                 'row_id'=>'',
                                 'counname'=>$storename,
                                 'coucode'=>$countryid,
                                 'commid'=>''
                                 );
           //p($parameter); exit;

          $recor= $this->supper_admin->call_procedureRow('proc_master',$parameter);
          
          $this->session->set_flashdata("message", "Your information has been saved successfully!");
          @redirect("admin/master/documenttype");

      }
      else{
            
        $this->session->set_flashdata("message", "Document Type Already Exists");
         @redirect("admin/master/documenttype");
       
      }
     }
    }
    
  
   
      $parameter = array('act_mode'=>'list_document','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_master', $parameter);
   //p($response['vieww']); exit;

    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('master/documenttype',$response);
    
  }


  public function documenttypedelete($id){
    $parameter = array('act_mode'=>'delete_document','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
    $response  = $this->supper_admin->call_procedure('proc_master',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect(base_url().'admin/master/documenttype');
}


public function documenttypeupdate($id){

  $parameter             = array('act_mode'=>'viewactivecountry', 'row_id'=>'', 'counname'=>'', 'coucode'=>'', 'commid'=>'');
    $response['viewcountry'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    //p( $response['viewcountry']);
  if($this->input->post('submit')){

   // Validation form fields.
  $this->form_validation->set_rules('storetype','Document Type','trim|required|xss_clean');
  
     
   if($this->form_validation->run() != false){
      $name      = $this->input->post('storetype');
      $countryid = $this->input->post('countryid');
     $parameter = array('act_mode'=>'update_document','row_id'=>$id,'counname'=>$name,'coucode'=>$countryid,'commid'=>'');
     $response  = $this->supper_admin->call_procedure('proc_master',$parameter);
     $this->session->set_flashdata('message', 'Your information was successfully Update.');
     redirect(base_url().'admin/master/documenttype');
   }
  
  }

  $parameter         = array('act_mode'=>'listing_document','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww'] = $this->supper_admin->call_procedureRow('proc_master',$parameter);
  //p($response['vieww']);
 
  //p($response['vieww']); exit;
  $this->load->view('helper/header');
  $this->load->view('master/documenttypeedit',$response);
}


public function documenttypestatus(){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='1'?'activeandinactive_document':'inactiveandinactive_document';
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
    
//p($parameter); exit;
  $response      = $this->supper_admin->call_procedure('proc_master',$parameter);
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/master/documenttype');
}

  public function employeetype(){
  
    if($this->input->post('submit')){

     // Validation form fields.
     $this->form_validation->set_rules('store_name','Employee Type Name','trim|required|xss_clean');

     if($this->form_validation->run() != false){
      $storename = $this->input->post('store_name');
     
     

       $parameter = array('act_mode'=>'couneemptype','row_id'=>'','counname'=>$storename,'coucode'=>'','commid'=>'');

   
      $record['record']= $this->supper_admin->call_procedureRow('proc_master',$parameter);
     
     //p($record['record']->counts); exit;

      if($record['record']->counts<=0){
     

            $parameter = array('act_mode'=>'emptypeinsert','row_id'=>'','counname'=>$storename,'coucode'=>'','commid'=>'');
          
            //p($parameter); exit;
          $record['record']= $this->supper_admin->call_procedureInsert('proc_master',$parameter);
          
          $this->session->set_flashdata("message", "Your information has been saved successfully!");
          @redirect("admin/master/employeetype");

      }
      else{
            
        $this->session->set_flashdata("message", "Employee Type Name Already Exists");
         @redirect("admin/master/employeetype");
       
      }
     }
    }
    
  
   
    $parameter = array('act_mode'=>'emptypelist','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
   

    $response['vieww'] = $this->supper_admin->call_procedure('proc_master', $parameter);
    // p($response['vieww']); exit;

    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('master/employeetype',$response);
    
  }



public function employeetypeupdate($id){
  if($this->input->post('submit')){

   // Validation form fields.
  $this->form_validation->set_rules('storetype','Employee Type','trim|required|xss_clean');
  
     
   if($this->form_validation->run() != false){
     $name      = $this->input->post('storetype');
    $parameter = array('act_mode'=>'couneemptypeupdate','row_id'=>$id,'counname'=>$name,'coucode'=>'','commid'=>'');
   $record['record']= $this->supper_admin->call_procedureRow('proc_master',$parameter);
     if($record['record']->counts<=0){
    
     $parameter = array('act_mode'=>'emptypeupdate','row_id'=>$id,'counname'=>$name,'coucode'=>'','commid'=>'');
     $response  = $this->supper_admin->call_procedure('proc_master',$parameter);
     $this->session->set_flashdata('message', 'Your information was successfully Update.');
     redirect(base_url().'admin/master/employeetype');
   }
   else
   {
      $this->session->set_flashdata("message", "Employee Type Name Already Exists");
      @redirect("admin/master/employeetypeupdate/".$id);
   }
   }
  
  }

  $parameter         = array('act_mode'=>'updateemptypelist','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww'] = $this->supper_admin->call_procedureRow('proc_master',$parameter);
 
  $this->load->view('helper/header');
  $this->load->view('master/employeetypeupdate',$response);
}

public function add_category(){
    $this->userfunction->loginAdminvalidation();
    if($this->input->post('submit')){

     // Validation form fields.
     $this->form_validation->set_rules('categorydeal_name','Category Name','trim|required|xss_clean');

     if($this->form_validation->run() != false){

      $typedeal= $this->input->post('categorydeal_name');
      
      $parameter= array(
        'act_mode'=>'categoryexit',
        'row_id'=>'',
        'p_name'=>$typedeal,
        'Param4'=>'',
        'Param5'=>'',
        'Param6'=>'',
        'Param7'=>''
        );

    $record['record']= $this->supper_admin->call_procedureRow('typedeal',$parameter);
     
      if(empty($record['record'])){

        if(!empty($_FILES['catogary_Icon']['name'])) {
                  $upload_name =  time().str_replace(' ', '_', strtolower($_FILES['catogary_Icon']['name']));
                  $filePath = FCPATH.'public/cat_image/'.$upload_name;
                  $tmpFilePath = $_FILES['catogary_Icon']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $image = $upload_name;
                  
              } else {
                    $image = "";
                  } 
              }

                if(!empty($_FILES['location_icon']['name'])) {
                  $upload_name =  time().str_replace(' ', '_', strtolower($_FILES['location_icon']['name']));
                  $filePath = FCPATH.'public/cat_image/'.$upload_name;
                  $tmpFilePath = $_FILES['location_icon']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $icon = $upload_name;
                  
              } else {
                    $icon = "";
                  } 
              }
              

         $parameter= array(
        'act_mode'=>'addcategory',
        'row_id'=>'',
        'p_name'=>$typedeal,
        'Param4'=>$image,
        'Param5'=>$icon,
        'Param6'=>'',
        'Param7'=>''
        );
          // p($parameter); exit;

          $record['record']= $this->supper_admin->call_procedureRow('typedeal',$parameter);
         

          $this->session->set_flashdata("message", "Your information has been saved successfully!");
          @redirect("admin/master/add_category");

      }
      else{
            
        $this->session->set_flashdata("message", "Catogary Name Already Exists");
         @redirect("admin/master/add_category");
       
      }
  
     }

    }//if submit
  
     $parameter = array(

        'act_mode'=>'viewcategoryadmin',
        'row_id'=>'',
        'p_name'=>'',
        'Param4'=>'',
        'Param5'=>'',
        'Param6'=>'',
        'Param7'=>''
                      );
     
      $response['vieww'] = $this->supper_admin->call_procedure('typedeal', $parameter);

   //p($response['vieww']); exit;

    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('master/category',$response);
    
  }




  public function deletcategory($id) {

      $parameter = array(

        'act_mode'=>'deletecatogary',
        'row_id'=>$id,
        'p_name'=>'',
        'Param4'=>'',
        'Param5'=>'',
        'Param6'=>'',
        'Param7'=>''
                      );
      $response['vieww'] = $this->supper_admin->call_procedure('typedeal', $parameter);

      $this->session->set_flashdata('message', 'Record has been deleted successfully!');
      redirect("admin/master/add_category");
  } 


  public function statuscatogery($id,$status) {

      $act_mode = $status=='1'?'categoreydeactive':'categoreyactive'; 
      $parameter = array(
                        'act_mode' => $act_mode, 
                        'row_id' => $id,
                        'p_name'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>''
                      );
      $response = $this->supper_admin->call_procedure('typedeal', $parameter);

      $this->session->set_flashdata('message', 'Status has been changed successfully!');
      redirect("admin/master/add_category");
  }

  public function updatecategory($id) {

      if($this->input->post('submit')) 
      {
        $this->form_validation->set_rules('categorydeal_name','catogery name','trim|required');

        if($this->form_validation->run() != false) {

           if(!empty($_FILES['catogary_Icon']['name'])) {
                 
                  $upload_name = time().str_replace(' ', '_', strtolower($_FILES['catogary_Icon']['name']));
                  $filePath = FCPATH.'public/cat_image/'.$upload_name;
                  $tmpFilePath = $_FILES['catogary_Icon']['tmp_name'];
                  move_uploaded_file($tmpFilePath, $filePath);
                
                $image = $upload_name;
              }
              else
              {
                $image = $this->input->post('catogary_Icon1');
              }

               if(!empty($_FILES['location_icon']['name'])) {
                 
                  $upload_name = time().str_replace(' ', '_', strtolower($_FILES['location_icon']['name']));
                  $filePath = FCPATH.'public/cat_image/'.$upload_name;
                  $tmpFilePath = $_FILES['location_icon']['tmp_name'];
                  move_uploaded_file($tmpFilePath, $filePath);
                
                $icon = $upload_name;
              }
              else
              {
                $icon = $this->input->post('location_icon1');
              }


              $parameter = array(
                        'act_mode' => 'updatecatogery', 
                        'row_id' => $id,
                        'p_name' => $this->input->post('categorydeal_name'),
                        'Param4' => $image,
                        'Param5' => $icon,
                        'Param6'=>'',
                        'Param7'=>''
                        
                      );
              

            $record= $this->supper_admin->call_procedure('typedeal',$parameter);
          
         


              $this->session->set_flashdata('message', 'Your information has been updated successfully!');
              redirect("admin/master/add_category");
        }
      }

      $parameter = array(
                        'act_mode' =>'fetchcatogery', 
                        'row_id' =>$id,
                        'p_name' =>'',
                        'Param4' =>'',
                        'Param5' =>'',
                        'Param6'=>'',
                        'Param7'=>''
                      );
      $response['vieww'] = $this->supper_admin->call_procedureRow('typedeal', $parameter);
      //p($response['vieww']); exit;
      $this->load->view('helper/header');
      $this->load->view('helper/sidebar');
      $this->load->view('master/categoryedit',$response);
      
  }
public function maincategory(){
    $this->userfunction->loginAdminvalidation();
  
     $parameter = array(

        'act_mode'=>'viewmaincategory',
        'row_id'=>'',
        'p_name'=>'',
        'Param4'=>'',
        'Param5'=>'',
        'Param6'=>'',
        'Param7'=>''
                      );
     
      $response['vieww'] = $this->supper_admin->call_procedure('typedeal', $parameter);

   //p($response['vieww']); exit;

    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('master/maincategory',$response);
    
  }
 public function statusmaincatogery($id,$status) {

      $act_mode = $status=='1'?'catdeactive':'catactive'; 
      $parameter = array(
                        'act_mode' => $act_mode, 
                        'row_id' => $id,
                        'p_name'=>'',
                        'Param4'=>'',
                        'Param5'=>'',
                        'Param6'=>'',
                        'Param7'=>''
                      );
      $response = $this->supper_admin->call_procedure('typedeal', $parameter);

      $this->session->set_flashdata('message', 'Status has been changed successfully!');
      redirect("admin/master/maincategory");
  }

public function welcome_banner()
{ 
   $this->userfunction->loginAdminvalidation();
    if($this->input->post('submit')){
       
        //p($_FILES);exit;
         if(!empty($_FILES['image']['name'])) 
         {
            $img=str_replace(' ', '-', $_FILES['image']['name']);
            $upload_name =  time().strtolower($img);
            $filePath = FCPATH.'public/homebanner/'.$upload_name;
            $tmpFilePath = $_FILES['image']['tmp_name'];
            if(move_uploaded_file($tmpFilePath, $filePath)) 
            {
                $parameter= array(
                  'act_mode'=>'bannerinsert',
                  'Param2'=>'',
                  'Param3'=>'',
                  'Param4'=>'',
                  'Param5'=>'',         
                  'Param6'=>$upload_name,
                  'Param7'=>'',
                  'Param8'=>'',
                  'Param9'=>$this->input->post('sequence'),
                  'Param10'=>$this->input->post('title'),
                  'Param11'=>$this->input->post('descr'),
                  'Param12'=>'',
                  'Param13'=>'',
                  'Param14'=>'',         
                  'Param15'=>'',
                  'Param16'=>'',
                  'Param17'=>'',
                  'Param18'=>''
                );
                $record= $this->supper_admin->call_procedureRow('proc_welcome',$parameter);
                $msg="Your information has been saved successfully!";
            } 
            else 
            {
                $msg = "please try again!";
            } 
          }
          else
          {
               $msg = "please try again!";
          }    
  
          $this->session->set_flashdata("message", $msg);
          redirect("admin/master/welcome_banner");
    }
        $parameter= array(
          'act_mode'=>'fetchbanner',
          'Param2'=>'',
          'Param3'=>'',
          'Param4'=>'',
          'Param5'=>'',         
          'Param6'=>'',
          'Param7'=>'',
          'Param8'=>'',
          'Param9'=>'',
          'Param10'=>'',
          'Param11'=>'',
          'Param12'=>'',
          'Param13'=>'',
          'Param14'=>'',         
          'Param15'=>'',
          'Param16'=>'',
          'Param17'=>'',
          'Param18'=>''
        );
      

        $record['images']= $this->supper_admin->call_procedure('proc_welcome',$parameter);

    
    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('master/welcomebanner',$record);
    

}


public function banneredit()
{ 
   $this->userfunction->loginAdminvalidation();
   $id=$this->uri->segment(4);
    if($this->input->post('submit')){
       
        //p($_FILES);exit;
         if(!empty($_FILES['image']['name'])) 
         {
            $img=str_replace(' ', '-', $_FILES['image']['name']);
            $upload_name =  time().strtolower($img);
            $filePath = FCPATH.'public/homebanner/'.$upload_name;
            $tmpFilePath = $_FILES['image']['tmp_name'];
            if(move_uploaded_file($tmpFilePath, $filePath)) 
            {
               $img=$upload_name;
            } 
            else 
            {
                $img=$this->input->post('image1');
            } 
          }
          else
          {
               $img=$this->input->post('image1');
          }    

          $parameter= array(
                  'act_mode'=>'bannerupdate',
                  'Param2'=>$id,
                  'Param3'=>'',
                  'Param4'=>'',
                  'Param5'=>'',         
                  'Param6'=>$img,
                  'Param7'=>'',
                  'Param8'=>'',
                  'Param9'=>$this->input->post('sequence'),
                  'Param10'=>$this->input->post('title'),
                  'Param11'=>$this->input->post('descr'),
                  'Param12'=>$this->input->post('status'),
                  'Param13'=>'',
                  'Param14'=>'',         
                  'Param15'=>'',
                  'Param16'=>'',
                  'Param17'=>'',
                  'Param18'=>''
                );

          $record= $this->supper_admin->call_procedureRow('proc_welcome',$parameter);
          $msg="Your information has been successfully updated!";
  
          $this->session->set_flashdata("message", $msg);
          redirect("admin/master/welcome_banner");
    }

        $parameter= array(
          'act_mode'=>'editfetchbanner',
          'Param2'=>$id,
          'Param3'=>'',
          'Param4'=>'',
          'Param5'=>'',         
          'Param6'=>'',
          'Param7'=>'',
          'Param8'=>'',
          'Param9'=>'',
          'Param10'=>'',
          'Param11'=>'',
          'Param12'=>'',
          'Param13'=>'',
          'Param14'=>'',         
          'Param15'=>'',
          'Param16'=>'',
          'Param17'=>'',
          'Param18'=>''
        );
      

        $record['images']= $this->supper_admin->call_procedureRow('proc_welcome',$parameter);

    
    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('master/welcome-edit',$record);
    

}


public function delete_banner($id) {
     $parameter= array(
          'act_mode'=>'deletebanner',
          'Param2'=>$id,
          'Param3'=>'',
          'Param4'=>'',
          'Param5'=>'',         
          'Param6'=>'',
          'Param7'=>'',
          'Param8'=>'',
          'Param9'=>'',
          'Param10'=>'',
          'Param11'=>'',
          'Param12'=>'',
          'Param13'=>'',
          'Param14'=>'',         
          'Param15'=>'',
          'Param16'=>'',
          'Param17'=>'',
          'Param18'=>''
        );
      

      

    //p($parameter); exit;
    $response = $this->supper_admin->call_procedureRow('proc_welcome', $parameter);
    $this->session->set_flashdata("message", "Deleted Successfully!");
     redirect("admin/master/welcome_banner");
}

public function producttypeadd(){  
    $this->userfunction->loginAdminvalidation();
    
    if($this->input->post('submit')){
   
       $name      = $this->input->post('couname');

       $parameter = array('act_mode'=>'producttypeexists','row_id'=>'','counname'=>$name,'coucode'=>'','commid'=>'');
                 
     
      $record['record1']= $this->supper_admin->call_procedureRow('proc_master',$parameter);

      if(empty($record['record1']))
      {
        $parameter = array('act_mode'=>'producttypeadd','row_id'=>'','counname'=>$name,'coucode'=>'','commid'=>'');
        
          
          $record = $this->supper_admin->call_procedure('proc_master',$parameter);
        
       
       @$this->session->set_flashdata('message', 'Your information was successfully Saved.');
       @redirect(base_url().'admin/master/producttypeadd');
     }
     else
     {
      @$this->session->set_flashdata('message', 'Product type already exists.');
       @redirect(base_url().'admin/master/producttypeadd');
     }

  }
    
    $parameter = array('act_mode'=>'producttypelist','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
        
  
    $record['data'] = $this->supper_admin->call_procedure('proc_master',$parameter);


    $this->load->view('helper/header');

    $this->load->view('helper/sidebar');
    $this->load->view('master/producttypeadd',$record);
    
  }


  public function producttypeupdate($id){  
    $this->userfunction->loginAdminvalidation();

    if($this->input->post('submit')){
   
       $name      = $this->input->post('couname');
       $parameter = array('act_mode'=>'producttypeupdate','row_id'=>$id,'counname'=>$name,'coucode'=>'','commid'=>'');
     // p($parameter);exit;
       $record = $this->supper_admin->call_procedureRow('proc_master',$parameter);
        
       
       $this->session->set_flashdata('message', 'Your information was successfully updated.');
       redirect(base_url().'admin/master/producttypeupdate/'.$id);

  }  
    $parameter = array('act_mode'=>'producttypeget','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
      
   
    $record['data'] = $this->supper_admin->call_procedureRow('proc_master',$parameter);
    //p($record['data']); die;
    $this->load->view('helper/header');

    $this->load->view('helper/sidebar');
    $this->load->view('master/producttypeupdate',$record);
    
  }
  public function producttypestatus (){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);

  $s      = $status==1?'0':'1';

  $parameter = array('act_mode'=>'producttypestatus','row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>$s);
  $record['data'] = $this->supper_admin->call_procedureRow('proc_master',$parameter);
  
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/master/producttypeadd');
}


}
?>
