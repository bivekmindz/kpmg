<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Merchant extends MX_Controller {
  public $data   =   array();
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->userfunction->loginAdminvalidation();
  }

public function document() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'merchantdocumenttype',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['document'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

      if($this->input->post('documentsubmit')=='Submit')
      {
        $check='';
        foreach ($_FILES['docfilename']['name'] as $key => $value) 
        {
          if(!empty($value))
          {
            $check=$check+1;
          }
          else
          {
            $check=$check+0;
          }
        }


        if($check > 0) 
        {
            foreach ($_FILES['docfilename']['name'] as $key => $value) 
            {
              if(!empty($value))
              {
                $filename =  time().strtolower(str_replace(' ', '_', $_FILES['docfilename']['name'][$key]));
                $filePath = FCPATH.'public/document/'.$filename;
                $tmpFilePath = $_FILES['docfilename']['tmp_name'][$key];
                move_uploaded_file($tmpFilePath, $filePath);
           
                $parameter= array(
                'act_mode'=>'adddocument',
                'row_id'=>$id,
                'counname'=>$this->input->post('docid')[$key],
                'coucode'=>$filename,
                'commid'=>$loginid
                );
                $response = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
              }
            }
            $this->session->set_flashdata('message', 'Your information was successfully saved.');
            redirect("admin/merchant/document/".$id);
        }
         else
          {
            
            $this->session->set_flashdata('message', 'Please atleast one document upload!');
            redirect("admin/merchant/document/".$id);
          }
      }

      $parameter= array(
      'act_mode'=>'getdocument',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['getdocument'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);



      $this->load->view('helper/header');
      $this->load->view('merchant/documents',$response);
}

public function deletedocument() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(5);
    $parameter= array(
      'act_mode'=>'deletedocument',
      'row_id'=>$this->uri->segment(4),
      'counname'=>'',
      'coucode'=>'',
      'commid'=>$loginid
      );
     $response = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);

    $this->session->set_flashdata('message', 'Your information was successfully delete.');
     redirect("admin/merchant/document/".$id);

    }

  public function addmerchant() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'allcountry',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    

      $parameter= array(
      'act_mode'=>'allcity',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['city'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

      $parameter= array(
      'act_mode'=>'alllocation',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['location'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
 

       $parameter = array(

                        'act_mode' => 'editviewmerchant', 
                        'param2' => '',
                        'param3' => $id,
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => '',
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''                             

                      );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedureRow('proc_merchant', $parameter);
   
      $countryid          = $response['vieww']->s_countryid;
      $stateid            = $response['vieww']->s_stateid;
  $parameter = array('act_mode'=>'countrydata','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['state']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);

$param= array('act_mode'=>'getcoumanager','row_id'=>$countryid,'counname'=>$stateid,'coucode'=>'','commid'=>'');

$response['salesmngr']  = $this->supper_admin->call_procedure('proc_geographic',$param);

      if($this->input->post('retailersubmit')=='Submit')
      {
       

        if(!empty($_FILES['crpimgfilename']['name'])) {
                  $upload_name =  strtolower($_FILES['crpimgfilename']['name']);
                  $filePath = FCPATH.'public/merchant/'.$upload_name;
                  $tmpFilePath = $_FILES['crpimgfilename']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $filename = $upload_name;
                  
              } else {
                    $filename = $this->input->post('fileToUploadhidden');
                  } 
              }else{
                
                
                 $filename = $this->input->post('fileToUploadhidden');
              }   
        
                 $param = array(

                        'act_mode' => 'merchant_store_update', 
                        'param2' => $this->input->post('s_username'),//company/store name
                        'param3' => '',#$this->input->post('s_loginemail'),
                        'param4' => $this->input->post('s_mobileno'),
                        'param5' => $this->input->post('countryid'),
                        'param6' => $this->input->post('stateid'),
                        'param7' => $this->input->post('cityname'),
                        'param8' => $this->input->post('location'),
                        'param9' => $this->input->post('pincode'),
                        'param10' => $this->input->post('owndername'),//owner/manager name
                        'param11' => $this->input->post('companyaddress'),
                        'param12' => $this->input->post('docrecdate'),// document received
                        'param13' => $this->input->post('agrementsigndate'),//agreement
                        'param14' => $filename,
                        'param15' => $this->input->post('operation'),//Assigned Manager ID/Store Type ID
                        'param16' => $this->input->post('accountholder'),
                        'param17' => $this->input->post('accountnumber'),
                        'param18' => $this->input->post('ifscode'),
                        'param19' => $this->input->post('bankname'),
                        'param20' => $this->input->post('branchname'),
                        'param21' => '',#$this->input->post('payment'),//Payment (1=Merchant,2=Store)/Category ID
                        'param22' => '',#base64_encode($pwd),
                        'param23' => '2',//Login type
                        'param24' => '0',//Merchant ID/Employee Type ID
                        'param25' => 'MERCHANT',// Unique/QR code
                        'param26' => '',//latitude
                        'param27' => '',//longitude
                        'param28' => '',//term & condition
                        'param29' => 'Offline',//register type (Offline=Admin Panel,Online=Front end)
                        'param30' => $loginid,//Login ID
                        'param31' => $id,//Update ID
                        'param32' => $this->input->post('paymenttype'),//Payment (Card,Cheque,Online,ECS)
                        'param33' => '5,'.implode(',', $this->input->post('protype')),//(Loyalty,Offers,Punch),
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
              $this->session->set_flashdata('message', 'Your information was successfully updated.');
              redirect("admin/merchant/addmerchant/".$id);
      }



      $this->load->view('helper/header');
      $this->load->view('merchant/merchant',$response);
  } 



public function getbankdetailmerchant(){

  
      $parameter = array(
      'act_mode' => 'getbankdetailmerchant',
      'row_id'=>'',
      'counname'=>$this->input->post('rrid'),
      'coucode'=>'',
      'commid'=>''
      );
       //p($parameter); exit;
     $response  = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
    
     $str.=$response->s_achname.'#'.$response->s_acnumber.'#'.$response->s_bankname.'#'.$response->s_branchname.'#'.$response->s_ifsccode;
     echo $str; exit();
     
}

public function merchantlist(){

  $logintype=$this->session->userdata('lw_login')->s_logintype;
  $countryid=$this->session->userdata('lw_login')->s_countryid;
  $stateid=$this->session->userdata('lw_login')->s_stateid;

      $parameter = array(
      'act_mode' => 'merchantlist',
      'row_id'=>'2',
      'counname'=>$logintype,
      'coucode'=>$countryid,
      'commid'=>$stateid
      );
       //p($parameter); exit;
     $response['list']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    //p($response['list']);
     $this->load->view('helper/header');
     $this->load->view('merchant/merchantlisting', $response);
     
}

public function outletlist(){

  
      $parameter = array(
      'act_mode' => 'outletlist',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
       //p($parameter); exit;
     $response['list']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    //p($response['list']);
     $this->load->view('helper/header');
     $this->load->view('merchant/outletlist', $response);
     
}

public function outlet() {

        $id=$this->uri->segment(4);
  
     $parameter= array(
      'act_mode'=>'allcountry',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    
   
       $parameter1 = array(
          'act_mode'=>'outletcatgory',
          'row_id'=>'',
          'counname'=>'',
          'coucode'=>'',
          'commid'=>''
          );
          $response['category'] = $this->supper_admin->call_procedure('proc_geographic', $parameter1);

           $parameter1 = array(
          'act_mode'=>'getmerchantname',
          'row_id'=>$id,
          'counname'=>'',
          'coucode'=>'',
          'commid'=>''
          );
          $response['merchant'] = $this->supper_admin->call_procedureRow('proc_geographic', $parameter1);
//p($response['merchant']);
          /* $countryid          = $response['merchant']->s_countryid;
  $parameter= array('act_mode'=>'countrystate','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['state']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);*/

  $countryid          = $response['merchant']->s_countryid;
  $parameter = array('act_mode'=>'countrydata','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['state']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);


    $parameter= array('act_mode'=>'getmercategory','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
    $response['getcategory']  = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
//p($response['getcategory']);
     $parameter1 = array(
          'act_mode'=>'liststoretype',
          'row_id'=>'',
          'counname'=>'',
          'coucode'=>'',
          'commid'=>''
          );
          $response['storetype'] = $this->supper_admin->call_procedure('proc_geographic', $parameter1);



      $this->load->view('helper/header');
      $this->load->view('merchant/outlet',$response);
  } 

public function outletedit() {

        $id=$this->uri->segment(4);
  
     $parameter= array(
      'act_mode'=>'allcountry',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

      
       $parameter1 = array(
          'act_mode'=>'outletcatgory',
          'row_id'=>'',
          'counname'=>'',
          'coucode'=>'',
          'commid'=>''
          );
          $response['category'] = $this->supper_admin->call_procedure('proc_geographic', $parameter1);

           
          
     $parameter1 = array(
          'act_mode'=>'liststoretype',
          'row_id'=>'',
          'counname'=>'',
          'coucode'=>'',
          'commid'=>''
          );
          $response['storetype'] = $this->supper_admin->call_procedure('proc_geographic', $parameter1);


  $parameter = array(

                        'act_mode' => 'editviewmerchant', 
                        'param2' => '',
                        'param3' => $id,
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => '',
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''                             

                      );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedureRow('proc_merchant', $parameter);
       //p($response['vieww']);

       $parameter1 = array(
          'act_mode'=>'getmerchantname',
          'row_id'=>$response['vieww']->s_merchantid,
          'counname'=>'',
          'coucode'=>'',
          'commid'=>''
          );
          $response['merchant'] = $this->supper_admin->call_procedureRow('proc_geographic', $parameter1);
 $countryid          = $response['merchant']->s_countryid;
  $parameter = array('act_mode'=>'countrydata','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['state']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);


        if($this->input->post('outletsubmit')=='Submit')
      {
       

        if(!empty($_FILES['crpimgfilename']['name'])) {
                  $upload_name =  strtolower($_FILES['crpimgfilename']['name']);
                  $filePath = FCPATH.'public/merchant/'.$upload_name;
                  $tmpFilePath = $_FILES['crpimgfilename']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $filename = $upload_name;
                  
              } else {
                    $filename = $this->input->post('fileToUploadhidden');
                  } 
              }else{
                
                
                 $filename = $this->input->post('fileToUploadhidden');
              }   
        
                 $param = array(

                        'act_mode' => 'merchant_store_update', 
                        'param2' => $this->input->post('s_username'),//company/store name
                        'param3' => '',#$this->input->post('s_loginemail'),
                        'param4' => $this->input->post('s_mobileno'),
                        'param5' => $this->input->post('countryid'),
                        'param6' => $this->input->post('stateid'),
                        'param7' => $this->input->post('cityname'),
                        'param8' => $this->input->post('location'),
                        'param9' => $this->input->post('pincode'),
                        'param10' => $this->input->post('owndername'),//owner/manager name
                        'param11' => $this->input->post('companyaddress'),
                        'param12' => $this->input->post('docrecdate'),// document received
                        'param13' => $this->input->post('agrementsigndate'),//agreement
                        'param14' => $filename,
                        'param15' => $this->input->post('operation'),//Assigned Manager ID/Store Type ID
                        'param16' => $this->input->post('accountholder'),
                        'param17' => $this->input->post('accountnumber'),
                        'param18' => $this->input->post('ifscode'),
                        'param19' => $this->input->post('bankname'),
                        'param20' => $this->input->post('branchname'),
                        'param21' => $this->input->post('payment'),//Category ID
                        'param22' => '',#base64_encode($pwd),
                        'param23' => '3',//Login type
                        'param24' => '',//Merchant ID/Employee Type ID
                        'param25' => 'OUTLET',// Unique/QR code
                        'param26' => $this->input->post('lattitude'),//latitude
                        'param27' => $this->input->post('logitude'),//longitude
                        'param28' => $this->input->post('term_cond'),//term & condition
                        'param29' => '',
                        'param30' => $loginid,//Login ID
                        'param31' => $id,//Update ID
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
              $this->session->set_flashdata('message', 'Your information was successfully updated.');
              redirect("admin/merchant/outletedit/".$id);
      }


      $this->load->view('helper/header');
      $this->load->view('merchant/outletedit',$response);
  } 



  public function getstate(){
  $countryid          = $this->input->post('countryid');
  $parameter          = array('act_mode'=>'countrystate','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  //p($response['vieww']);exit();
  $str = '';

  foreach($response['vieww'] as $k=>$v){   
      $str .= "<option value=".$v->stateid.">".$v->statename."</option>";
  }
  echo $str;
 } 

  public function getcountrydata(){
  $countryid          = $this->input->post('countryid');
  $parameter          = array('act_mode'=>'countrydata','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  //p($response['vieww']);exit();
  $str = '';

  foreach($response['vieww'] as $k=>$v){   
      $str .= "<option value=".$v->g_id.">".$v->g_statename.', '.$v->g_cityname."</option>";
  }
  echo $str;
 } 

  public function getcity(){
  $stateid          = $this->input->post('stateid');
  $parameter          = array('act_mode'=>'countrycity','row_id'=>$stateid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww1']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $str = '';
  foreach($response['vieww1'] as $k=>$v){   
      $str .= "<option value=".$v->cityid.">".$v->cityname."</option>";
  }
  echo $str;
 } 


 
 public function getlocation(){
  $cityid          = $this->input->post('cityid');
  $parameter          = array('act_mode'=>'countrylocation','row_id'=>$cityid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $str = '';
  foreach($response['vieww'] as $k=>$v){   
      $str .= "<option value=".$v->streetid.">".$v->location."</option>";
  }
  echo $str;
 } 

 public function getmanager(){
  $countryid          = $this->input->post('countryid');
  $stateid          = $this->input->post('stateid');
  $parameter          = array('act_mode'=>'getcoumanager','row_id'=>$countryid,'counname'=>$stateid,'coucode'=>'','commid'=>'');

  $response['vieww']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $str = '';
  foreach($response['vieww'] as $k=>$v){   
      $str .= "<option value=".$v->s_loginid.">".$v->s_name."</option>";
  }
  echo $str;
 } 


  public function insertmerchant() {

     $loginid = $this->session->userdata('lw_login')->s_loginid;

    $email=trim($this->input->post('s_loginemail'));

         

              $parameter = array(

                        'act_mode' => 'checkmerchant', 
                        'param2' => '',
                        'param3' => $email,
                        'param4' => '2',#login type
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => '',
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''                             

                      );



      $record  = $this->supper_admin->call_procedureRow('proc_merchant', $parameter);
        
            if($record->flag == 0)
            {      

            $pwd=$this->input->post('s_loginpassword');

           if(!empty($_FILES['crpimgfilename']['name'])) {
            $upload_name =  time().str_replace(' ', '-', strtolower($_FILES['crpimgfilename']['name']));
            $filePath = FCPATH.'public/merchant/'.$upload_name;
            $tmpFilePath = $_FILES['crpimgfilename']['tmp_name'];
            if(move_uploaded_file($tmpFilePath, $filePath)) {
              $filename = $upload_name;
                  
              } 
              else 
              {
                    $filename = "";
              } 
              } 
              else 
              {
                    $filename = "";
              }     


              $param = array(

                        'act_mode' => 'merchant_store_add', 
                        'param2' => $this->input->post('s_username'),//company/store name
                        'param3' => $this->input->post('s_loginemail'),
                        'param4' => $this->input->post('s_mobileno'),
                        'param5' => $this->input->post('countryid'),
                        'param6' => $this->input->post('stateid'),
                        'param7' => $this->input->post('cityname'),
                        'param8' => $this->input->post('location'),
                        'param9' => $this->input->post('pincode'),
                        'param10' => $this->input->post('owndername'),//owner/manager name
                        'param11' => $this->input->post('companyaddress'),
                        'param12' => $this->input->post('docrecdate'),// document received
                        'param13' => $this->input->post('agrementsigndate'),//agreement
                        'param14' => $filename,
                        'param15' => $this->input->post('operation'),//Assigned Manager ID/Store Type ID
                        'param16' => $this->input->post('accountholder'),
                        'param17' => $this->input->post('accountnumber'),
                        'param18' => $this->input->post('ifscode'),
                        'param19' => $this->input->post('bankname'),
                        'param20' => $this->input->post('branchname'),
                        'param21' => $this->input->post('payment'),//Payment (1=Merchant,2=Store)/Category ID
                        'param22' => base64_encode($pwd),
                        'param23' => 2,//Login type
                        'param24' => 0,//Merchant ID/Employee Type ID
                        'param25' => 'MERCHANT',// Unique/QR code
                        'param26' => '',//latitude
                        'param27' => '',//longitude
                        'param28' => '',//term & condition
                        'param29' => 'Offline',//register type (Offline=Admin Panel,Online=Front end)
                        'param30' => $loginid,//Login ID
                        'param31' => $this->input->post('paymenttype'),//Payment (Card,Cheque,Online,ECS)
                        'param32' => '5,'.implode(',', $this->input->post('protype')),//(Loyalty,Offers,Punch)
                        'param33' => 0,#MLM
                        'param34' => base64_encode(1111),
                        'param35' => '',
                        'param36' => '1'  
                        
                   );
            //p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
          $lastid = $response->last_id;
          //p($response); exit;
       $content='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{background-color:#16369d; width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{    width: 100%;
    float: left;color:#444;}
.body_contain ul.top-list{ padding:0px;list-style-type:none;}
.body_contain ul.top-list li:first-child{font-size:16px; color:#444; line-height:22px;}
.body_contain ul.top-list li.second{font-size:13px; color:#444; line-height:20px;}
.body_contain ul.top-list li:last-child{font-size:13px; color:#444; line-height:20px;}
.content{ width:100%; float:left;}
.content .left_div{ width:60%; float:left;}
.content .left_div ul{ padding:0px; width:100%; float:left;list-style-type:none;}
.content .left_div ul li{ width:100%; float:left;}
.content .left_div ul li .list-img{ float:left; padding:0 10px 0 0;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#16369d; margin-top:15px;}
.left-f{padding-left:10px; color:#FFF; width:60%; float:left;}
.right-f{float:right; color:#fff; padding-right:5px;}
ul{ padding:0px;}
ul li{font-size:13px; color:#404040; list-style-type:none; padding:5px 0;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}

}
</style>
</head>

<body>
<div class="main_div">
  <div class="header">
    <div class="container">
        <div class="logo">
         <img src="'.base_url().'assets/website/images/ngage-logo-big1.png" style="width:100%;">
      </div>
        <div class="right_div">
        <div class="right-txt"></div>
        </div>
    </div>
  </div>

<div class="body_contain">
  <div class="container">
      <ul>
          <li>Welcome to Loyalty Wallet, '.$this->input->post('s_username').'</li>
            <li>Thank You for Signing Up & Being a Part of Loyalty Wallet</li>
            <li>Your Account has been created Successfully & your credentials are :-</li>
        </ul>     
        <div class="user-form">
          <table>
            <tr>
              <td class="left">User Email :</td>
                <td class="right">'.$email.'</td>
            </tr>
             <tr>
              <td class="left">Password :</td>
                <td class="right">'.$pwd.'</td>
            </tr>
          </table>
        </div>    
    <p class="help">Login URL <a href="'.base_url().'">start using the dashboard</a></p>
   </div>
</div>  
<div class="footer">
  <div class="container">
    <p class="left-f">Looking forward to do Business with You..!!</p>
    <p class="right-f">Loyalty Wallet Team.</p>
    </div>
</div>    
    
</div>

</body>
</html>
';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from(mailfrome(),mailfromn());
                  $this->email->to($email); 
                  if(mailfromccstatus()==1){ $this->email->cc(mailfromcc()); } 
                  $this->email->subject('Merchant Registered!');
                  $this->email->message($content);
                  $this->email->send();

        

                echo $lastid;
               
            } 
            else
            {
              echo 0;
              
            }      
       
  }

  public function insertoutlet()
  {
     $loginid = $this->session->userdata('lw_login')->s_loginid;
    $merchantid=$this->input->post('merchantid');

    $logintype=$this->session->userdata('lw_login')->s_logintype;
    if($logintype==2) 
    { 
        $status=0;
    }
    else
    {
        $status=1;
    }

    $parameter= array('act_mode'=>'getmerchantemail','row_id'=>$merchantid,'counname'=>'','coucode'=>'','commid'=>'');
  $merchantemail  = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
  $emailmerchant=$merchantemail->s_email;

    $email=trim($this->input->post('s_loginemail'));

          if($email=="")
          {
            echo "Please enter your emailid!"; exit;
          }

              $parameter = array(

                        'act_mode' => 'checkoutlet', 
                        'param2' => '',
                        'param3' => $email,
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => '',
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''                             

                      );

 //p($parameter); exit;
          $record  = $this->supper_admin->call_procedureRow('proc_merchant', $parameter);
        
            if($record->flag == 0)
            {      

            $pwd=$this->input->post('s_loginpassword');

           if(!empty($_FILES['crpimgfilename']['name'])) {
            $upload_name =  time().str_replace(' ', '-', strtolower($_FILES['crpimgfilename']['name']));
            $filePath = FCPATH.'public/merchant/'.$upload_name;
            $tmpFilePath = $_FILES['crpimgfilename']['tmp_name'];
            if(move_uploaded_file($tmpFilePath, $filePath)) {
              $filename = $upload_name;
                  
              } 
              else 
              {
                    $filename = "";
              } 
              } 
              else 
              {
                    $filename = "";
              }     

              $param = array(

                        'act_mode' => 'merchant_store_add', 
                        'param2' => $this->input->post('s_username'),//company/store name
                        'param3' => $this->input->post('s_loginemail'),
                        'param4' => $this->input->post('s_mobileno'),
                        'param5' => $this->input->post('countryid'),
                        'param6' => $this->input->post('stateid'),
                        'param7' => $this->input->post('cityname'),
                        'param8' => $this->input->post('location'),
                        'param9' => $this->input->post('pincode'),
                        'param10' => $this->input->post('owndername'),//owner/manager name
                        'param11' => $this->input->post('companyaddress'),
                        'param12' => $this->input->post('docrecdate'),// document received
                        'param13' => $this->input->post('agrementsigndate'),//agreement
                        'param14' => $filename,
                        'param15' => $this->input->post('operation'),//Assigned Manager ID/Store Type ID
                        'param16' => $this->input->post('accountholder'),
                        'param17' => $this->input->post('accountnumber'),
                        'param18' => $this->input->post('ifscode'),
                        'param19' => $this->input->post('bankname'),
                        'param20' => $this->input->post('branchname'),
                        'param21' => $this->input->post('payment'),//Payment (1=Merchant,2=Store)/Category ID
                        'param22' => base64_encode($pwd),
                        'param23' => '3',//Login type
                        'param24' => $merchantid,//Merchant ID/Employee Type ID
                        'param25' => 'OUTLET',// Unique/QR code
                        'param26' => $this->input->post('lattitude'),//latitude
                        'param27' => $this->input->post('logitude'),//longitude
                        'param28' => $this->input->post('term_cond'),//term & condition
                        'param29' => 'Offline',//register type (Offline=Admin Panel,Online=Front end)
                        'param30' => $loginid,//Login ID
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => $status  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
              $lastid = $response->last_id;

      
        echo $merchantid;
      }
      else
      {
        echo 0;
      }
   

  }

  public function updatestatus (){
  $rowid       = $this->uri->segment(4);
  $status      = $this->uri->segment(5);
  $type        = $this->uri->segment(6);
  $mrid        = $this->uri->segment(7);
  $act_mode    = $status==1?'updatestatusinactive':'updatestatusactive';

  $parameter = array('act_mode'=>$act_mode,'row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
  $record = $this->supper_admin->call_procedureRow('proc_master',$parameter);
  
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  if($type=='M')
  {
      redirect(base_url('admin/merchant/merchantlist'));
  }
  else if ($type=='O') 
  {
      redirect(base_url().'admin/merchant/merchantoutletlist/'.$mrid);
  }
  else if ($type=='Employee') 
  {
      redirect(base_url('admin/user/employeelist/'));
  }
  /*else 
  {
      redirect(base_url().'admin/merchant/merchantoutletlist/'.$type);
  }*/
  
}

public function merchantoutletlist(){

  $rowid         = $this->uri->segment(4);
  
  $parameter = array('act_mode'=>'merchantoutletlist','row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['list']= $this->supper_admin->call_procedure('proc_master',$parameter);
    //p($response['list']);
     $this->load->view('helper/header');
     $this->load->view('merchant/outletlist', $response);
     
}

public function setpin(){

  $id=decrypt($this->uri->segment(2));
  if($id=='fdjhfs3rt363t782ewf7')
  {
      $changepinid = $this->session->userdata('lw_login')->s_loginid;
      $merid = $this->session->userdata('lw_login')->s_loginid;
      //$url='set-pin/'.$id;
      $m='Old';
      $m1='Merchant';
  }
  else
  {
      $changepinid = $id;
      $merid = $this->session->userdata('lw_login')->s_loginid;
      //$url='set-pin/'.$id;
      $m='Merchant';
      $m1='Outlet';
  }
  $url='set-pin/'.$this->uri->segment(2);
  if($this->input->post('resetpin')=='Submit')
  {
      $newpin=$this->input->post('newpass');

      $parameter = array('act_mode'=>'setpinmrout',
                        'Param2'=>$changepinid,
                        'Param3'=>$merid,
                        'Param4'=>base64_encode($this->input->post('oldpass')),
                        'Param5'=>base64_encode($newpin),
                        'Param6'=>'',
                        'Param7'=>'',
                        'Param8'=>'',
                        'Param9'=>'',
                        'Param10'=>'');

      $response= $this->supper_admin->call_procedureRow('proc_other',$parameter);
      if($response->res==0)
      {
          $msg=$m.' Pin wrong! Please try again!';
      }
      else if ($response->res==1) 
      {
          $msg='Your Pin already use another outlets!';
      } 
      else 
      {
          $msg='Your Pin was successfully set!';
          $email=$response->s_email;

          $content='<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{background-color:#16369d; width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{width: 100%;float: left;color:#444;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#16369d; margin-top:15px;}
.left-f{padding-left:10px; color:#FFF; width:60%; float:left;}
.right-f{float:right; color:#fff; padding-right:5px;}
.para-1{padding-left:10px; font-size:18px; color:#404040;}
.para-2{padding-left:10px; font-size:13px; color:#404040;}
th{ background:#16369d; color:#fff; width:25%;}
table{ width:100%; float:left;}
.para-t{padding-left:10px; margin-top:25px; width:100%; float:left; font-size:13px; color:#404040;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}


}
</style>
</head>
<body>
<div class="main_div">
  <div class="header">
      <div class="logo"><img src="'.base_url().'assets/website/images/ngage-logo-big1.png" style="width:100%;"></div>
        <div class="right_div">
</div> 
    </div>
    
<div class="body_contain">
<div class="container">

  <p class="para-1">Dear '.$m1.',</p>
  <p class="para-2"></p>

<table width="100%" border="1">
      
       <tr>
        <th>Your Pin</th>
        <td>'.$newpin.'</td>
       </tr>
       
  </table>

</div>    
    

<div class="footer" style="width:100%; float:left; background:#16369d; height:45px; margin-top:35px;">
<p class="left-f">Looking forward to do Business with You..!!</p>
<p class="right-f">Loyalty Wallet Team.</p>
</div>    
    
</div>

</body>
</html>
';

                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from(mailfrome(),mailfromn());
                  $this->email->to($email); 
                  if(mailfromccstatus()==1){ $this->email->cc(mailfromcc()); } 
                  $this->email->subject($m1.' Pin');
                  $this->email->message($content);
                  $this->email->send();
      }

      $this->session->set_flashdata('message', $msg);
      redirect(base_url().$url);

  }
     $this->load->view('helper/header');
     $this->load->view('merchant/pinset');
     
}

public function outletsetpin(){

  $loginid = $this->session->userdata('lw_login')->s_loginid;
  
  $parameter = array('act_mode'=>'merchantoutletlist','row_id'=>$loginid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['list']= $this->supper_admin->call_procedure('proc_master',$parameter);
    //p($response['list']);
     $this->load->view('helper/header');
     $this->load->view('merchant/outletpinset', $response);
     
}

public function resetpin_1(){

  $loginid = $this->session->userdata('lw_login')->s_loginid;
  $email = $this->session->userdata('lw_login')->s_email;
  $pin=rand(1000,9999);
  $parameter = array('act_mode'=>'setpin','row_id'=>$loginid,'counname'=>base64_encode($pin),'coucode'=>'','commid'=>'');
  $response= $this->supper_admin->call_procedureRow('proc_master',$parameter);
  //p($response->r);exit;

   $content='<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{background-color:#fff; width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{width: 100%;float: left;color:#444;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#16369d; margin-top:15px;}
.left-f{padding-left:10px; color:#FFF; width:60%; float:left;}
.right-f{float:right; color:#fff; padding-right:5px;}
.para-1{padding-left:10px; font-size:18px; color:#404040;}
.para-2{padding-left:10px; font-size:13px; color:#404040;}
th{ background:#16369d; color:#fff; width:25%;}
table{ width:100%; float:left;}
.para-t{padding-left:10px; margin-top:25px; width:100%; float:left; font-size:13px; color:#404040;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}


}
</style>
</head>
<body>
<div class="main_div">
  <div class="header">
      <div class="logo"><img src="'.base_url().'assets/website/images/ngage-logo-big1.png" style="width:100%;"></div>
        <div class="right_div">
</div> 
    </div>
    
<div class="body_contain">
<div class="container">

  <p class="para-1">Dear Merchant,</p>
  <p class="para-2"></p>

<table width="100%" border="1">
      
       <tr>
        <th>Your Pin</th>
        <td>'.$pin.'</td>
       </tr>
       
  </table>

</div>    
    

<div class="footer" style="width:100%; float:left; background:#16369d; height:45px; margin-top:35px;">
<p class="left-f">Looking forward to do Business with You..!!</p>
<p class="right-f">Loyalty Wallet Team.</p>
</div>    
    
</div>

</body>
</html>
';

                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from(mailfrome(),mailfromn());
                  $this->email->to($email); 
                  if(mailfromccstatus()==1){ $this->email->cc(mailfromcc()); } 
                  $this->email->subject('Merchant Pin');
                  $this->email->message($content);
                  $this->email->send();

     
}

public function resetpin(){

  $loginid = $this->session->userdata('lw_login')->s_loginid;
  $email = $this->session->userdata('lw_login')->s_email;
  
   $content='<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{background-color:#fff; width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{width: 100%;float: left;color:#444;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#16369d; margin-top:15px;}
.left-f{padding-left:10px; color:#FFF; width:60%; float:left;}
.right-f{float:right; color:#fff; padding-right:5px;}
.para-1{padding-left:10px; font-size:18px; color:#404040;}
.para-2{padding-left:10px; font-size:13px; color:#404040;}
th{ background:#16369d; color:#fff; width:25%;}
table{ width:100%; float:left;}
.para-t{padding-left:10px; margin-top:25px; width:100%; float:left; font-size:13px; color:#404040;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}


}
</style>
</head>
<body>
<div class="main_div">
  <div class="header">
      <div class="logo"><img src="'.base_url().'assets/website/images/ngage-logo-big1.png" style="width:100%;"></div>
        <div class="right_div">
</div> 
    </div>
    
<div class="body_contain">
<div class="container">

  <p class="para-1">Dear Merchant,</p>
  <p class="para-2"></p>

<table width="100%" border="1">
      
       <tr>
        <th>Your Link</th>
        <td><a href="'.base_url().'reset-pin/'.encrypt($loginid).'/'.encrypt($email.$loginid).'">Reset Pin</a></td>
       </tr>
       
  </table>

</div>    
    

<div class="footer" style="width:100%; float:left; background:#16369d; height:45px; margin-top:35px;">
<p class="left-f">Looking forward to do Business with You..!!</p>
<p class="right-f">Loyalty Wallet Team.</p>
</div>    
    
</div>

</body>
</html>
';

                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from(mailfrome(),mailfromn());
                  $this->email->to($email); 
                  if(mailfromccstatus()==1){ $this->email->cc(mailfromcc()); } 
                  $this->email->subject('Merchant Pin');
                  $this->email->message($content);
                  $this->email->send();

     
}

public function getoutletpin(){

  $loginid = $this->session->userdata('lw_login')->s_loginid;
  
  $parameter = array('act_mode'=>'checkmerpin','row_id'=>$loginid,'counname'=>base64_encode($this->input->post('merpin')),'coucode'=>'','commid'=>'');
  $response= $this->supper_admin->call_procedureRow('proc_master',$parameter);
  //p($response);
    if($response->res==1)
    {
      if(base64_decode(decrypt($this->input->post('outpin')))=='')
      {
          echo 1;
      }
      else
      {
          echo base64_decode(decrypt($this->input->post('outpin')));
      }
    }
    else
    {
      echo 0;
    }
 
     
}


}