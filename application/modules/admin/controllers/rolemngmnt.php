<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Rolemngmnt extends MX_Controller {
  public $data   =   array();
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->userfunction->loginAdminvalidation();
  }

  public function roleassign() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

     $parameter = array('act_mode'=>'getemptype','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
     $response['emp']      = $this->supper_admin->call_procedure('proc_geographic',$parameter);

     $parameter = array('act_mode'=>'mainmenu','Param2'=>'','Param3'=>'','Param4'=>'','Param5'=>'','Param6'=>'','Param7'=>'','Param8'=>'');
     $response['mainmenu']      = $this->supper_admin->call_procedure('proc_assignrole',$parameter);

      if($this->input->post('rolesubmit')=='Submit')
      {
        if(!empty($this->input->post('menu')))
        {
          foreach ($this->input->post('menu') as $key => $value) 
          { 
            $menu=explode('#', $value);

            $parameter = array( 'act_mode'=>'assignrole',
                                'Param2'=>$this->input->post('empid'),
                                'Param3'=>$menu[0],
                                'Param4'=>$menu[1],
                                'Param5'=>'',
                                'Param6'=>'',
                                'Param7'=>'',
                                'Param8'=>'');
            //p($parameter);//exit;
            $respons = $this->supper_admin->call_procedureRow('proc_assignrole',$parameter);
          }
          $msg='Your information was successfully saved.';
        }
        else
        {
            $msg='Please atleast one menu checked!!';
        }
           
              $this->session->set_flashdata('message', $msg);
              redirect("admin/rolemngmnt/roleassign");
      }

    
      $this->load->view('helper/header');
      $this->load->view('rolemanagement/roleassign',$response);
  } 

public function getemp(){

  $parameter = array( 'act_mode'=>'getemp',
                                'Param2'=>$this->input->post('emptypeid'),
                                'Param3'=>'',
                                'Param4'=>'',
                                'Param5'=>'',
                                'Param6'=>'',
                                'Param7'=>'',
                                'Param8'=>'');
       
  $response['vieww'] = $this->supper_admin->call_procedure('proc_assignrole',$parameter);
  $str = '';
  foreach($response['vieww'] as $k=>$v){   
      $str .= "<option value=".$v->s_loginid.">".$v->s_name.' ('.$v->name.')'."</option>";
  }
  echo $str;
 } 


 public function getassignmenu(){

  $parameter = array( 'act_mode'=>'getassignmenu',
                                'Param2'=>$this->input->post('empid'),
                                'Param3'=>'',
                                'Param4'=>'',
                                'Param5'=>'',
                                'Param6'=>'',
                                'Param7'=>'',
                                'Param8'=>'');
       
  $vieww = $this->supper_admin->call_procedure('proc_assignrole',$parameter);

  $parameter = array('act_mode'=>'mainmenu','Param2'=>'','Param3'=>'','Param4'=>'','Param5'=>'','Param6'=>'','Param7'=>'','Param8'=>'');
  $mainmenu     = $this->supper_admin->call_procedure('proc_assignrole',$parameter);

$str = ''; 
$ma='';
$masu='';
foreach ($vieww as $key => $v)   
{ 
  $ma .=$v->main_menu.',';
  $masu .=$v->sub_menu.',';
}
$mainm=explode(',', trim($ma,','));
$submenu=explode(',', trim($masu,','));

foreach ($mainmenu as $key => $value) 
{ 
    if($value->parentid==0)
    {
        if(in_array($value->id,$mainm))
        {
            $str .='<p><input type="checkbox" checked disabled>'.$value->menuname.' <i class="fa fa-check-square-o fo" aria-hidden="true"></i></p>'; 
        }
        else
        {
           $str .='<p><input type="checkbox" disabled>'.$value->menuname.'</p>'; 
        }
      
        foreach ($mainmenu as $key => $val) 
        { 
          if($val->parentid==$value->id)
          {
            if(in_array($val->id,$submenu))
            {
                $str .='<p style="margin-left:30px;"><input type="checkbox" id="'.$val->id.'" checked disabled>'.$val->menuname.' <i class="fa fa-check-square fo" aria-hidden="true" id="c'.$val->id.'"></i> <a href="javascript:void(0)" onclick="removemenu('.$val->id.')"><i class="fa fa-trash" aria-hidden="true" id="r'.$val->id.'"></i></a><span id="m'.$val->id.'"></span> </p>'; 
            }
            else
            {
                $str .='<p style="margin-left:30px;"><input type="checkbox" disabled>'.$val->menuname.'</p>';
            }
          } 
        }
    } 
} 
                              
echo $str;
} 
 
 public function removeassignmenu(){

  $parameter = array( 'act_mode'=>'removeassignmenu',
                                'Param2'=>$this->input->post('empid'),
                                'Param3'=>$this->input->post('menuid'),
                                'Param4'=>'',
                                'Param5'=>'',
                                'Param6'=>'',
                                'Param7'=>'',
                                'Param8'=>'');
       
  $response = $this->supper_admin->call_procedureRow('proc_assignrole',$parameter);
  echo 1;
 } 

  public function lead() {

  $loginid  =$this->session->userdata('lw_login')->s_loginid;
  $logintype=$this->session->userdata('lw_login')->s_logintype;
  $countryid=$this->session->userdata('lw_login')->s_countryid;
  $stateid  =$this->session->userdata('lw_login')->s_stateid;
  $emptype  =$this->session->userdata('lw_login')->s_merchantid;

      //----------------  start pagination ------------------------// 

   $parameter = array(

                        'act_mode' => 'lead', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '1',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => $emptype );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedure('proc_order', $parameter);

      $config['base_url']         = base_url()."/admin/rolemngmnt/lead?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter = array(

                        'act_mode' => 'lead', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '0',
                        'param7' => $page,
                        'param8' => $second,
                        'param9' => '',
                        'param10' => $emptype );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedure('proc_order', $parameter);


      $this->load->view('helper/header');      
      $this->load->view('rolemanagement/lead',$response);
  }


  public function editlead() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;
    $emptype  =$this->session->userdata('lw_login')->s_merchantid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'allcountry',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

       $parameter = array(

                        'act_mode' => 'editviewmerchant', 
                        'param2' => '',
                        'param3' => $id,
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => '',
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''                             

                      );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedureRow('proc_merchant', $parameter);
   
      $countryid          = $response['vieww']->s_countryid;
      $stateid            = $response['vieww']->s_stateid;
  $parameter = array('act_mode'=>'countrydata','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['state']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);

$param= array('act_mode'=>'getcoumanager','row_id'=>$countryid,'counname'=>$stateid,'coucode'=>'','commid'=>'');

$response['salesmngr']  = $this->supper_admin->call_procedure('proc_geographic',$param);

      
      if($this->input->post('retailersubmit')=='Submit')
      {
        $verify=$this->input->post('verify');
        if(empty($this->input->post('operation')))
        {
            $ltype=4;
        }
        else
        {
            if($verify==0)
            {
              $ltype=5;
            }
            else
            {
              $email=$response['vieww']->s_email;
              $ltype=2;

              /*================= Email =====================*/
$content='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{background-color:#16369d; width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{    width: 100%;
    float: left;color:#444;}
.body_contain ul.top-list{ padding:0px;list-style-type:none;}
.body_contain ul.top-list li:first-child{font-size:16px; color:#444; line-height:22px;}
.body_contain ul.top-list li.second{font-size:13px; color:#444; line-height:20px;}
.body_contain ul.top-list li:last-child{font-size:13px; color:#444; line-height:20px;}
.content{ width:100%; float:left;}
.content .left_div{ width:60%; float:left;}
.content .left_div ul{ padding:0px; width:100%; float:left;list-style-type:none;}
.content .left_div ul li{ width:100%; float:left;}
.content .left_div ul li .list-img{ float:left; padding:0 10px 0 0;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#16369d; margin-top:15px;}
.left-f{padding-left:10px; color:#FFF; width:60%; float:left;}
.right-f{float:right; color:#fff; padding-right:5px;}
ul{ padding:0px;}
ul li{font-size:13px; color:#404040; list-style-type:none; padding:5px 0;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}

}
</style>
</head>

<body>
<div class="main_div">
  <div class="header">
    <div class="container">
        <div class="logo">
         <img src="'.base_url().'assets/KMPG.png" style="width:100%;">
      </div>
        <div class="right_div">
        <div class="right-txt"></div>
        </div>
    </div>
  </div>

<div class="body_contain">
  <div class="container">
      <ul>
          <li>Congratulation! Convert lead to merchant successful!</li>
        </ul>  
   </div>
</div>  
<div class="footer">
  <div class="container">
    <p class="left-f">Looking forward to do Business with You..!!</p>
    <p class="right-f">Loyalty Wallet Team.</p>
    </div>
</div>    
    
</div>

</body>
</html>
';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from(mailfrome(),mailfromn());
                  $this->email->to($email); 
                  if(mailfromccstatus()==1){ $this->email->cc(mailfromcc()); } 
                  $this->email->subject('Lead Process!');
                  $this->email->message($content);
                  $this->email->send();

              /*================= End =======================*/

            }
            
        }

        if(!empty($_FILES['crpimgfilename']['name'])) {
                  $upload_name =  strtolower($_FILES['crpimgfilename']['name']);
                  $filePath = FCPATH.'public/merchant/'.$upload_name;
                  $tmpFilePath = $_FILES['crpimgfilename']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $filename = $upload_name;
                  
              } else {
                    $filename = $this->input->post('fileToUploadhidden');
                  } 
              }else{
                
                
                 $filename = $this->input->post('fileToUploadhidden');
              }   
        
                 $param = array(

                        'act_mode' => 'lead_update', 
                        'param2' => $this->input->post('s_username'),//company/store name
                        'param3' => '',#$this->input->post('s_loginemail'),
                        'param4' => $this->input->post('s_mobileno'),
                        'param5' => $this->input->post('countryid'),
                        'param6' => $this->input->post('stateid'),
                        'param7' => '',
                        'param8' => '',
                        'param9' => $this->input->post('pincode'),
                        'param10' => $this->input->post('owndername'),//owner/manager name
                        'param11' => $this->input->post('companyaddress'),
                        'param12' => $this->input->post('docrecdate'),// document received
                        'param13' => $this->input->post('agrementsigndate'),//agreement
                        'param14' => $filename,
                        'param15' => $this->input->post('operation'),//Assigned Manager ID/Store Type ID
                        'param16' => $this->input->post('accountholder'),
                        'param17' => $this->input->post('accountnumber'),
                        'param18' => $this->input->post('ifscode'),
                        'param19' => $this->input->post('bankname'),
                        'param20' => $this->input->post('branchname'),
                        'param21' => '',
                        'param22' => '',
                        'param23' => $ltype,//Login type
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => 'Online',//register type (Offline=Admin Panel,Online=Front end)
                        'param30' => $loginid,//Login ID
                        'param31' => $id,//Update ID
                        'param32' => $this->input->post('paymenttype'),//Payment (Card,Cheque,Online,ECS)
                        'param33' => implode(',', $this->input->post('protype')),//(Loyalty,Offers,Punch),
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
              $this->session->set_flashdata('message', 'Your information was successfully updated.');
              if($emptype==1)
              {
                  redirect("admin/rolemngmnt/lead");
              }
              else
              {
                  redirect("admin/rolemngmnt/assignedlead");
              }
      }



      $this->load->view('helper/header');
      $this->load->view('rolemanagement/leadedit',$response);
  } 

  public function assignedlead() {

  $loginid  =$this->session->userdata('lw_login')->s_loginid;
  $logintype=$this->session->userdata('lw_login')->s_logintype;
  $countryid=$this->session->userdata('lw_login')->s_countryid;
  $stateid  =$this->session->userdata('lw_login')->s_stateid;
  $emptype  =$this->session->userdata('lw_login')->s_merchantid;

      //----------------  start pagination ------------------------// 

   $parameter = array(

                        'act_mode' => 'leadassign', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '1',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => $emptype );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedure('proc_order', $parameter);

      $config['base_url']         = base_url()."/admin/rolemngmnt/lead?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter = array(

                        'act_mode' => 'leadassign', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '0',
                        'param7' => $page,
                        'param8' => $second,
                        'param9' => '',
                        'param10' => $emptype );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedure('proc_order', $parameter);


      $this->load->view('helper/header');      
      $this->load->view('rolemanagement/leadassign',$response);
  }

  public function document() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'merchantdocumenttype',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['document'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

      if($this->input->post('documentsubmit')=='Submit')
      {
        $check='';
        foreach ($_FILES['docfilename']['name'] as $key => $value) 
        {
          if(!empty($value))
          {
            $check=$check+1;
          }
          else
          {
            $check=$check+0;
          }
        }


        if($check > 0) 
        {
            foreach ($_FILES['docfilename']['name'] as $key => $value) 
            {
              if(!empty($value))
              {
                $filename =  time().strtolower(str_replace(' ', '_', $_FILES['docfilename']['name'][$key]));
                $filePath = FCPATH.'public/document/'.$filename;
                $tmpFilePath = $_FILES['docfilename']['tmp_name'][$key];
                move_uploaded_file($tmpFilePath, $filePath);
           
                $parameter= array(
                'act_mode'=>'adddocument',
                'row_id'=>$id,
                'counname'=>$this->input->post('docid')[$key],
                'coucode'=>$filename,
                'commid'=>$loginid
                );
                $response = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
              }
            }
            $this->session->set_flashdata('message', 'Your information was successfully saved.');
            redirect("admin/rolemngmnt/document/".$id);
        }
         else
          {
            
            $this->session->set_flashdata('message', 'Please atleast one document upload!');
            redirect("admin/rolemngmnt/document/".$id);
          }
      }

      $parameter= array(
      'act_mode'=>'getdocument',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['getdocument'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);



      $this->load->view('helper/header');
      $this->load->view('rolemanagement/leaddocuments',$response);
}


public function deletedocument() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(5);
    $parameter= array(
      'act_mode'=>'deletedocument',
      'row_id'=>$this->uri->segment(4),
      'counname'=>'',
      'coucode'=>'',
      'commid'=>$loginid
      );
     $response = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);

    $this->session->set_flashdata('message', 'Your information was successfully delete.');
     redirect("admin/rolemngmnt/document/".$id);

    }


}