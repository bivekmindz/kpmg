<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class User extends MX_Controller{
  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }

  public function viewuser() {

  $logintype=$this->session->userdata('lw_login')->s_logintype;
  
 if($logintype==1)
 {
    $countryid='-1';
 }
 else
 {
    $countryid=$this->session->userdata('lw_login')->s_countryid;
 }

				$parameter = array(
                  'useremail'    => '',
                  'userpassword' => $countryid,
                  'act_mode'     => 'viewuser',
                  'row_id'       => '');
              $response['vieww'] = $this->supper_admin->call_procedure('proc_Adminlogin', $parameter);
      
      if(!empty($this->input->post('excel')))
          {

           
           $finalExcelArr = array('User ID','Name','Email ID','Contact Number','Date of Birth','Gender','Country','Status','Date Of Joining');
           $objPHPExcel = new PHPExcel();
           //p($response['vieww']);
           $objPHPExcel->setActiveSheetIndex(0);
           $objPHPExcel->getActiveSheet()->setTitle('User List');
           $cols= array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
            $j=2;
            
            //For freezing top heading row.
            $objPHPExcel->getActiveSheet()->freezePane('A2');

            //Set height for column head.
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(25);
                        
           for($i=0;$i<count($finalExcelArr);$i++){
            //echo 'hi';
            //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);

            //Set background color for heading column.
            $objPHPExcel->getActiveSheet()->getStyle($cols[$i].'1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '71B8FF')
                    ),
                      'font'  => array(
                      'bold'  => false,
                      'size'  => 15,
                      )
                )
            );

            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
            //p($response['vieww']);

            foreach ($response['vieww'] as $key => $value) {

              if($value->user_otp_status==1) 
            $status= 'Varified'; 
            else $status= 'Not Varify';
             
            $newvar = $j+$key;

            //Set height for all rows.
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);            
            $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->user_uid);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->t_FName);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->user_email);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, '+'.$value->ccode.'-'.$value->user_mobile);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->user_dob);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->user_gender);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->cname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $status);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->user_createdon);
                       
            }
          }

          $filename='User List.xls';
          header('Content-Type: application/vnd.ms-excel'); //mime type
          header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
          header('Cache-Control: max-age=0'); //no cache
          $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
          ob_end_clean();
          ob_start();  
          $objWriter->save('php://output');

         
          }


      $this->load->view('helper/header');      
      $this->load->view('user/viewuser',$response);
  }

public function employee()
{
      $parameter = array('act_mode'=>'getemptype','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
      $data['emp']      = $this->supper_admin->call_procedure('proc_geographic',$parameter); 

       $parameter= array(
      'act_mode'=>'allcountry',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $data['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter); 

      $this->load->view('helper/header');      
      $this->load->view('user/employee',$data);
}

 public function insertemployee() {

     $loginid = $this->session->userdata('lw_login')->s_loginid;

    $email=trim($this->input->post('s_loginemail'));

         

              $parameter = array(

                        'act_mode' => 'checkmerchant', 
                        'param2' => '',
                        'param3' => $email,
                        'param4' => '6',#login type
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => '',
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''                             

                      );



      $record  = $this->supper_admin->call_procedureRow('proc_merchant', $parameter);

    //  p( $record  );
        
            if($record->flag == 0)
            {      

            $pwd=$this->input->post('s_loginpassword');

            $empid=explode('#', $this->input->post('emptypeid'));

              $param = array(

                        'act_mode' => 'merchant_store_add', 
                        'param2' => $this->input->post('s_username'),
                        'param3' => $this->input->post('s_loginemail'),
                        'param4' => $this->input->post('s_mobileno'),
                        'param5' => $this->input->post('countryid'),
                        'param6' => $this->input->post('stateid'),
                        'param7' => '0',
                        'param8' => '0',
                        'param9' => '0',
                        'param10' => '0',
                        'param11' => $this->input->post('companyaddress'),
                        'param12' => '0',
                        'param13' => '0',
                        'param14' => '0',
                        'param15' => '0',
                        'param16' => '0',
                        'param17' => '0',
                        'param18' => '0',
                        'param19' => '0',
                        'param20' => '0',
                        'param21' => '0',
                        'param22' => base64_encode($pwd),
                        'param23' => 6,//Login type
                        'param24' => $empid[0],//Merchant ID/Employee Type ID
                        'param25' => 'EMP',// Unique/QR code
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => $loginid,//Login ID
                        'param31' => '',
                        'param32' => '',
                        'param33' => '0',
                        'param34' => '0',
                        'param35' => '0',
                        'param36' => '0'  
                        
                   );
            //p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
          $lastid = $response->last_id;
      /*    //p($response); exit;
       $content='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{line-height:18px; max-width:620px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{background-color:#16369d; width:100%; float:left; padding:5px;box-sizing: border-box;}
.logo{float:left; width:25%; padding:0;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left;}
.body_contain{    width: 100%;
    float: left;color:#444;}
.body_contain ul.top-list{ padding:0px;list-style-type:none;}
.body_contain ul.top-list li:first-child{font-size:16px; color:#444; line-height:22px;}
.body_contain ul.top-list li.second{font-size:13px; color:#444; line-height:20px;}
.body_contain ul.top-list li:last-child{font-size:13px; color:#444; line-height:20px;}
.content{ width:100%; float:left;}
.content .left_div{ width:60%; float:left;}
.content .left_div ul{ padding:0px; width:100%; float:left;list-style-type:none;}
.content .left_div ul li{ width:100%; float:left;}
.content .left_div ul li .list-img{ float:left; padding:0 10px 0 0;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#16369d; margin-top:15px;}
.left-f{padding-left:10px; color:#FFF; width:60%; float:left;}
.right-f{float:right; color:#fff; padding-right:5px;}
ul{ padding:0px;}
ul li{font-size:13px; color:#404040; list-style-type:none; padding:5px 0;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}
.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}

}
</style>
</head>

<body>
<div class="main_div">
  <div class="header">
    <div class="container">
        <div class="logo">
           <img src="'.base_url().'assets/KMPG.png" style="width:100%;">
      </div>
        <div class="right_div">
        <div class="right-txt"></div>
        </div>
    </div>
  </div>

<div class="body_contain">
  <div class="container">
      <ul>
          <li>Welcome to Loyalty Wallet, '.$this->input->post('s_username').'</li>
            <li>Thank You for Signing Up & Being a Part of Loyalty Wallet</li>
            <li>Your Account has been created Successfully & your credentials are :-</li>
        </ul>     
        <div class="user-form">
          <table>
            <tr>
              <td class="left">Login Email :</td>
                <td class="right">'.$email.'</td>
            </tr>
             <tr>
              <td class="left">Role :</td>
                <td class="right">'.$empid[1].'</td>
            </tr>
             <tr>
              <td class="left">Password :</td>
                <td class="right">'.$pwd.'</td>
            </tr>
          </table>
        </div>    
    <p class="help">Login URL <a href="'.base_url('lw-admin').'">start using the dashboard</a></p>
   </div>
</div>  
<div class="footer">
  <div class="container">
    <p class="left-f">Looking forward to do Business with You..!!</p>
    <p class="right-f">Loyalty Wallet Team.</p>
    </div>
</div>    
    
</div>

</body>
</html>
';
                  $this->load->library('email');
                  $this->email->set_newline("\r\n");
                  $this->email->from(mailfrome(),mailfromn());
                  $this->email->to($email); 
                  if(mailfromccstatus()==1){ $this->email->cc(mailfromcc()); } 
                  $this->email->subject('Employee Registered!');
                  $this->email->message($content);
                  $this->email->send();
*/
        

                echo $lastid;
                
            } 
            else
            {
              echo 0;
              
            }      
       
  }

public function employeelist(){

  
      $parameter = array(
      'act_mode' => 'merchantlist',
      'row_id'=>'6',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
       //p($parameter); exit;
     $response['list']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    //p($response['list']);
     $this->load->view('helper/header');
     $this->load->view('user/employeelisting', $response);
     
}

public function deleteemp(){

  $rowid       = $this->uri->segment(4);

  $parameter = array('act_mode'=>'deleteemp','row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
  $record = $this->supper_admin->call_procedureRow('proc_master',$parameter);
  
  $this->session->set_flashdata('message', 'Employee was successfully Deleted.');

  redirect(base_url('admin/user/employeelist'));

     
}

 public function editemp() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'allcountry',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);


       $parameter = array(

                        'act_mode' => 'editviewmerchant', 
                        'param2' => '',
                        'param3' => $id,
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => '',
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''                             

                      );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedureRow('proc_merchant', $parameter);
   
      $countryid          = $response['vieww']->s_countryid;
      $stateid            = $response['vieww']->s_stateid;
  $parameter = array('act_mode'=>'countrydata','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['state']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);


      if($this->input->post('retailersubmit')=='Submit')
      {
       
                 $param = array(

                        'act_mode' => 'merchant_store_update', 
                        'param2' => $this->input->post('s_username'),//company/store name
                        'param3' => '',#$this->input->post('s_loginemail'),
                        'param4' => $this->input->post('s_mobileno'),
                        'param5' => $this->input->post('countryid'),
                        'param6' => $this->input->post('stateid'),
                        'param7' => $this->input->post('cityname'),
                        'param8' => $this->input->post('location'),
                        'param9' => $this->input->post('pincode'),
                        'param10' => $this->input->post('owndername'),//owner/manager name
                        'param11' => $this->input->post('companyaddress'),
                        'param12' => $this->input->post('docrecdate'),// document received
                        'param13' => $this->input->post('agrementsigndate'),//agreement
                        'param14' => '',
                        'param15' => $this->input->post('operation'),//Assigned Manager ID/Store Type ID
                        'param16' => $this->input->post('accountholder'),
                        'param17' => $this->input->post('accountnumber'),
                        'param18' => $this->input->post('ifscode'),
                        'param19' => $this->input->post('bankname'),
                        'param20' => $this->input->post('branchname'),
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '0',//Merchant ID/Employee Type ID
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => $loginid,//Login ID
                        'param31' => $id,//Update ID
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
              $this->session->set_flashdata('message', 'Your information was successfully updated.');
              redirect("admin/user/employeelist/");
      }



      $this->load->view('helper/header');
      $this->load->view('user/employeeedit',$response);
  } 

public function profile() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

   
    $parameter= array(
      'act_mode'=>'allcountry',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);


       $parameter = array(

                        'act_mode' => 'editviewmerchant', 
                        'param2' => '',
                        'param3' => $loginid,
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => '',
                        'param25' => '',
                        'param26' => '',
                        'param27' => '',
                        'param28' => '',
                        'param29' => '',
                        'param30' => '',
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''                             

                      );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedureRow('proc_merchant', $parameter);
   
      $countryid          = $response['vieww']->s_countryid;
      $stateid            = $response['vieww']->s_stateid;
  $parameter = array('act_mode'=>'countrydata','row_id'=>$countryid,'counname'=>'','coucode'=>'','commid'=>'');
  $response['state']  = $this->supper_admin->call_procedure('proc_geographic',$parameter);

      $this->load->view('helper/header');
      $this->load->view('user/profile',$response);
  } 

 public function users() {

  $logintype=$this->session->userdata('lw_login')->s_logintype;
  $loginid  =$this->session->userdata('lw_login')->s_loginid;

        $parameter = array(
                  'useremail'    => $loginid,
                  'userpassword' => '',
                  'act_mode'     => 'users',
                  'row_id'       => $logintype);
              $response['vieww'] = $this->supper_admin->call_procedure('proc_Adminlogin', $parameter);
      //p($response['vieww']);
     
      $this->load->view('helper/header');      
      $this->load->view('user/viewuser',$response);
  }

}
?>
