<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Geographic extends MX_Controller{
  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    //$this->userfunction->loginAdminvalidation();

    //$this->load->library('PHPExcel');
    //$this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }

//---------------------- Country  -------------------------//

public function add_country(){
  
  //$this->userfunction->loginAdminvalidation();
  if($this->input->post('submit')){
   // Validation form fields.
   $this->form_validation->set_rules('couname','Country Name','trim|required|xss_clean');
   $this->form_validation->set_rules('coucode','Country Code','trim|required|xss_clean');
     
   //if($this->form_validation->run() != false){



      $name = $this->input->post('couname');

      $parameter = array('act_mode'=>'checkcountry','row_id'=>'','counname'=>$name,'coucode'=>'','commid'=>'');
      $data      = $this->supper_admin->call_procedure('proc_geographic',$parameter);  
     
      if($data[0]->total > 0 ) {
        $this->session->set_flashdata('message', 'Country Already Exists Try Another .');
        redirect(base_url().'admin/geographic/add_country');
      }


     	 $code      = $this->input->post('coucode');
       $currency      = $this->input->post('currency');
     	 $parameter = array('act_mode'=>'countryinsert','row_id'=>'','counname'=>$name,'coucode'=>$currency, 'commid'=>$code);
     	 //---------------------- proce ----------------------------------//
     	  
       $response  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
     	 $this->session->set_flashdata('message', 'Your information was successfully Saved.');
     	 redirect(base_url().'admin/geographic/add_country');
     	 //----------------------end proce ----------------------------------//
   // }//end if
  }//end if

    $parameter         = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);


     $this->load->view('helper/header');
     $this->load->view('helper/sidebar');

     $this->load->view('geographic/addcountry',$response);
        

}

//---------------------- Country View  -------------------------//
  public function viewcountry(){
    $this->load->library('pagination'); // load pagination library.
    //$this->userfunction->loginAdminvalidation();

  	$parameter         = array('act_mode'=>'viewcountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
  	$response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    
  	$this->load->view('helper/header');
     $this->load->view('helper/sidebar');
    $this->load->view('geographic/viewcountry',$response);
    

  }

//---------------------- Country Update -------------------------//
public function countryupdate($id){
	if($this->input->post('submit')){

   // Validation form fields.
   $this->form_validation->set_rules('couname','State Name','trim|required|xss_clean');
   //$this->form_validation->set_rules('coucode','State Code','trim|required|xss_clean');
	   
   if($this->form_validation->run() != false){
     $name      = $this->input->post('couname');
   	 $code      = $this->input->post('coucode');
     $currency      = $this->input->post('currency');
   	 $parameter = array('act_mode'=>'countryupdate','row_id'=>$id,'counname'=>$name,'coucode'=>$currency,'commid'=>$code);
   	 $response  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
   	 $this->session->set_flashdata('message', 'Your information was successfully Update.');
   	 redirect(base_url().'admin/geographic/add_country');
   }
	}// if submit

	$parameter         = array('act_mode'=>'viewcounid','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww'] = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
	$this->load->view('helper/header');
	$this->load->view('geographic/editcountry',$response);
}

//---------------------- Country Delete  -------------------------//
public function countrydelete($id){
	  $parameter = array('act_mode'=>'countrydelete','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
   	$response  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
   	redirect(base_url().'admin/geographic/add_country');
}

//---------------------- Country Status  -------------------------//
public function countrystatus (){
	$rowid         = $this->uri->segment(4);
	$status        = $this->uri->segment(5);
	$act_mode      = $status=='1'?'activeandinactive':'inactiveandinactive';
	$parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
	$response      = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
	redirect(base_url().'admin/geographic/add_country');
}

//---------------------- Country Name Check  -------------------------//
public function checkname(){
	$couname   = $this->input->post('name');
	$parameter = array('act_mode'=>'checkcoun','row_id'=>'','counname'=>$couname,'coucode'=>'','commid'=>'');
	$data      = $this->supper_admin->call_procedure('proc_geographic',$parameter);
	echo json_encode($data);
}
//---------------------- End Country  ----------------------------------//

//----------------------  City Add  ----------------------------------//
public function add_city(){
  // $this->userfunction->loginAdminvalidation();
  $parameter = array('act_mode'=>'viewactivecountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['vieww1'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

   if($this->input->post('submit')){
   	 $stateid   = $this->input->post('stateid');
     $cityname  = $this->input->post('cityname');
   	 $parameter = array('act_mode'=>'cityinsert','row_id'=>$stateid,'counname'=>$cityname,'coucode'=>'','commid'=>'');
   	 $response  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
   	 $this->session->set_flashdata('message', 'Your information was successfully Saved.');
   	 redirect('admin/geographic/viewcity');
   }

    $parameter         = array('act_mode'=>'viewcity','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/geographic/viewcity?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(@$_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter         = array('act_mode'=>'viewcity','row_id'=>$page,'counname'=>'','coucode'=>'','commid'=>$second);
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    
    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('geographic/addcity',$response);
   

  }


//---------------------- View City  -------------------------//
 public function viewcity(){
    //$this->userfunction->loginAdminvalidation();
    $parameter         = array('act_mode'=>'viewcity','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/geographic/add_city?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 50;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(@$_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter         = array('act_mode'=>'viewcity','row_id'=>$page,'counname'=>'','coucode'=>'','commid'=>$second);
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    //----------------  end pagination ------------------------//  

    $this->load->view('helper/header');
     $this->load->view('helper/sidebar');
    $this->load->view('geographic/viewcity',$response);
  
  }

//---------------------- City Delete  -------------------------//
 public function citydelete($id){
  $parameter  = array('act_mode'=>'citydelete','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
  $response   = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $this->session->set_flashdata('message', 'Your information was successfully deleted.');
  redirect('admin/geographic/viewcity');
} 

//---------------------- City Status  -------------------------//
public function citystatus (){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='A'?'activecity':'inactivecity';
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
  $response      = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect('admin/geographic/viewcity');
}

//---------------------- City Update -------------------------//
public function cityupdate($id){
  if($this->input->post('submit')){
   $name      = $this->input->post('couname');
   $parameter = array('act_mode'=>'cityupdate','row_id'=>$id,'counname'=>$name,'coucode'=>'','commid'=>'');
   $response  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
   $this->session->set_flashdata('message', 'Your information was successfully Update.');
   redirect('admin/geographic/viewcity');
  }

  $parameter         = array('act_mode'=>'viewcityid','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww'] = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
  $this->load->view('helper/header');
  $this->load->view('geographic/editcity',$response);
}

//---------------------- City Name Check  -------------------------//
public function checkcityname(){
  $couname   = $this->input->post('name');
  $parameter = array('act_mode'=>'checkcitycoun','row_id'=>'','counname'=>$couname,'coucode'=>'','commid'=>'');
  $data      = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  echo json_encode($data);
}

public function checklocname(){
  $couname   = $this->input->post('name');
  $parameter = array('act_mode'=>'checkloccoun','row_id'=>'','counname'=>$couname,'coucode'=>'','commid'=>'');
  $data      = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  echo json_encode($data);
}

//---------------------- State Add  -------------------------//

public function checkstate() {
     $parameter = array(
                            'act_mode' => 'stateexist',
                            'row_id'   => $this->input->post('countryid'),
                            'counname' => $this->input->post('name'),
                            'coucode'  => '',
                            'commid'   => ''
                       );
         
          $data = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
       echo json_encode($data);
}
public function addstate(){

    //$this->userfunction->loginAdminvalidation();
    if($this->input->post('submit')){
        // Validation form fields.
        $this->form_validation->set_rules('statename','State Name','trim|required|xss_clean');
        $this->form_validation->set_rules('statecode','State Code','trim|required|xss_clean');
        $this->form_validation->set_rules('countryid','Country ','trim|required|xss_clean');

        //if($this->form_validation->run() != false){
          $parameter = array(
                            'act_mode' => 'stateexist',
                            'row_id'   => $this->input->post('countryid'),
                            'counname' => $this->input->post('statename'),
                            'coucode'  => '',
                            'commid'   => ''
                       );
         
          $record['record'] = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
          if($record['record']->statecount>0){
            $this->session->set_flashdata("message", "State Already Exists");
            redirect("admin/geographic/addstate");
          }else{
          
              $parameter = array(
                             'act_mode' => 'stateinsert',
                             'row_id'   => $this->input->post('countryid'),
                             'counname' => $this->input->post('statename'),
                             'coucode'  => '',
                             'commid'   => ''
                           );

              $record    = $this->supper_admin->call_procedure('proc_geographic',$parameter);
              $this->session->set_flashdata('message', 'Your information was successfully Saved.');
              redirect("admin/geographic/statelist");
          }
       // }
    }

    $parameter             = array('act_mode'=>'viewactivecountry', 'row_id'=>'', 'counname'=>'', 'coucode'=>'', 'commid'=>'');
    $record['viewcountry'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

     //p($record['viewcountry']);


    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');

    $this->load->view('geographic/addstate', $record);
    $this->load->view('helper/footer');    

}

//---------------------- State List  -------------------------//
  public function statelist(){  
    //$this->userfunction->loginAdminvalidation();
    
    $parameter = array(
                       'act_mode' => 'viewstate',
                       'row_id'   => '',
                       'counname' => '',
                       'coucode'  => '',
                       'commid'   => ''
                      );
    $record['data'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    
    $this->load->view('helper/header');

    $this->load->view('helper/sidebar');
    $this->load->view('geographic/statelist',$record);
  
  }

//---------------------- State Delete  -------------------------//
  public function statedelete($id){
    $parameter = array('act_mode'=>'statedelete','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
    $response  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect('admin/geographic/statelist');
  }

//---------------------- State Status  -------------------------//
  public function statestatus(){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  $act_mode      = $status=='A'?'stateactiveandinactive':'stateinactiveandinactive';
  $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
  $response      = $this->supper_admin->call_procedure('proc_geographic',$parameter);
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/geographic/statelist');
  }

//---------------------- State Update  -------------------------//
  public function stateupdate($id){
  if($this->input->post('submit')){
        // Validation form fields.
        $this->form_validation->set_rules('statename','State Name','trim|required|xss_clean');
        //$this->form_validation->set_rules('statecode','State Code','trim|required|xss_clean');
        $this->form_validation->set_rules('countryid','Country ','trim|required|xss_clean');

        if($this->form_validation->run() != false){
                $parameter=array(
                             'act_mode' => 'stateupdate',
                             'row_id'   => $id,
                             'counname' => $this->input->post('statename'),
                             'coucode'  => $this->input->post('statecode'),
                             'commid'   => ''
                            ); 

          $record = $this->supper_admin->call_procedure('proc_geographic',$parameter);
          $this->session->set_flashdata('message', 'Your information was successfully Updated.');
          redirect("admin/geographic/statelist");
        }
    }
  $parameter         = array('act_mode'=>'vieweditstate','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
  $response['vieww'] = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
  $this->load->view('helper/header');
  $this->load->view('geographic/editstate',$response);
  
}
//---------------------- end state ----------------------------//



public function getlocation(){
      

     //$this->load->view('helper/header');
     $this->load->view('geographic/getlocation');

}

public function userlocation(){

  if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){
    //Send request and receive json data by latitude and longitude
    $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($_POST['latitude']).','.trim($_POST['longitude']).'&sensor=falsekey=AIzaSyDULV7j6QbDAPEpmQd76P-lFkHwwARmAQ4';
    $json = @file_get_contents($url);
    $data = json_decode($json);
    $status = $data->status;
    if($status=="OK"){
        //Get address from json data
        $location = $data->results[0]->formatted_address;
    }else{
        $location =  '';
    }
    //Print address 
    echo $location;
}

}

  public function addlocation(){

    if($this->input->post('submit')){
      $cityid   = $this->input->post('cityid');
      $streetname  = $this->input->post('streetname');
      $parameter = array('act_mode'=>'streetinsert','row_id'=>$cityid,'counname'=>$streetname,'coucode'=>'','commid'=>'');
      $response  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
      $this->session->set_flashdata('message', 'Your information was successfully Saved.');
      redirect('admin/geographic/viewlocation');
    }

    $parameter         = array('act_mode'=>'viewactivecountry','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    


    $parameter         = array('act_mode'=>'viewstreet1','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/geographic/viewstreet?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 10000;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(@$_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*50);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter         = array('act_mode'=>'viewstreet1','row_id'=>$page,'counname'=>'','coucode'=>'','commid'=>$second);
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);


    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('geographic/addlocation',$response);
  
  }

  public function viewlocation(){
    $parameter         = array('act_mode'=>'viewstreet','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    //----------------  start pagination ------------------------// 

      $config['base_url']         = base_url()."admin/geographic/viewstreet?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 100;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(@$_GET['page']){
       $page         = $_GET['page']-1 ;
       $page         = ($page*100);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter         = array('act_mode'=>'viewstreet','row_id'=>$page,'counname'=>'','coucode'=>'','commid'=>$second);
    $response['vieww'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

    //----------------  end pagination ------------------------//  

    $this->load->view('helper/header');
     $this->load->view('helper/sidebar');
    $this->load->view('geographic/viewlocation',$response);

  }

  //---------------------- Street Delete  -------------------------//
   public function streetdelete($id){
    $parameter  = array('act_mode'=>'streetdelete','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
    $response   = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    $this->session->set_flashdata('message', 'Your information was successfully deleted.');
    redirect('admin/geographic/viewlocation');
  } 

  //---------------------- Street Status  -------------------------//
  public function streetstatus (){
    $rowid         = $this->uri->segment(4);
    $status        = $this->uri->segment(5);
    $act_mode      = $status=='1'?'activestreet':'inactivestreet';
    $parameter     = array('act_mode'=>$act_mode,'row_id'=>$rowid,'counname'=>'','coucode'=>'','commid'=>'');
    $response      = $this->supper_admin->call_procedure('proc_geographic',$parameter);
    $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
    redirect('admin/geographic/viewlocation');
  }

  //---------------------- Street Update -------------------------//
  public function streetupdate($id){
    if($this->input->post('submit')){
     $streetname      = $this->input->post('streetname');
     $parameter = array('act_mode'=>'streetupdate','row_id'=>$id,'counname'=>$streetname,'coucode'=>'','commid'=>'');
     $response  = $this->supper_admin->call_procedure('proc_geographic',$parameter);
     $this->session->set_flashdata('message', 'Your information has been Updated successfully!');
     redirect('admin/geographic/viewlocation');
    }

    $parameter         = array('act_mode'=>'viewstreet','row_id'=>$id,'counname'=>'','coucode'=>'','commid'=>'');
    


    $response['vieww'] = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);

    
    $this->load->view('helper/header');
    $this->load->view('helper/sidebar');
    $this->load->view('geographic/editlocation',$response);
  
  }

  function getdetails() {

    $country=$this->input->post('country');

        $url = "https://restcountries.eu/rest/v2/name/".str_replace(' ', '%20', $country);

       //$data=file_get_contents($url);
      //p($url);
        $ch = curl_init();//callingCodes
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true            
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($output);
      //print_r($response);

        foreach ($response as $key => $value) {
          # code...
        }
         $code = $value->callingCodes[0];
        $currency = $value->currencies[0]->code;
        $ccode = $value->alpha3Code;
        echo $code.'##'.$currency.'##'.$ccode;
      /*  $code = $response[1]->callingCodes[0];
        $currency = $response[1]->currencies[0]->code;
        echo $code.'##'.$currency;*/
        exit;
    }


public function addgeography(){ # 18july new functionality
  
   $loginid = $this->session->userdata('lw_login')->s_loginid;
  if($this->input->post('submit')){
  
     
       $param = array(

                        'act_mode' => 'addgeography', 
                        'param2' => $this->input->post('city'),
                        'param3' => $this->input->post('state'),
                        'param4' => $this->input->post('country'),
                        'param5' => $loginid,//created by
                        'param6' => $this->input->post('countrycode'),
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => $this->input->post('address'),
                        'param12' => $this->input->post('code'),
                        'param13' => $this->input->post('currency'),
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => ''
                        
                   );
          // p($param); exit;

      $response = $this->supper_admin->call_procedureRow('proc_welcome', $param);
       $this->session->set_flashdata('message', 'Your information was successfully Saved.');
       redirect(base_url().'admin/geographic/addgeography');
       
   
  }


     $this->load->view('helper/header');
     $this->load->view('helper/sidebar');

     $this->load->view('geographic/geography');
        

}

public function geographyview(){ # 18july new functionality
  
    $param = array(

                        'act_mode' => 'viewgeography', 
                        'param2' => '',
                        'param3' => '',
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => ''
                        
                   );
          // p($param); exit;

    $response['vieww'] = $this->supper_admin->call_procedure('proc_welcome', $param);
    

     $this->load->view('helper/header');
     $this->load->view('helper/sidebar');

     $this->load->view('geographic/geographyview',$response);
        

}

public function geostatus (){
  $rowid         = $this->uri->segment(4);
  $s             = $this->uri->segment(5);
  $status        = $s=='1'?'0':'1';


    $param = array(

                        'act_mode' => 'geostatus', 
                        'param2' => $rowid,
                        'param3' => $status,
                        'param4' => '',
                        'param5' => '',
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => ''
                        
                   );
          // p($param); exit;

    $response = $this->supper_admin->call_procedureRow('proc_welcome', $param);
    
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/geographic/geographyview');
}


}// end class

?>