<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Order extends MX_Controller{
  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
  }

  public function loyalty() {

  $loginid  =$this->session->userdata('lw_login')->s_loginid;
  $logintype=$this->session->userdata('lw_login')->s_logintype;
  $countryid=$this->session->userdata('lw_login')->s_countryid;
  $stateid  =$this->session->userdata('lw_login')->s_stateid;

      //----------------  start pagination ------------------------// 

   $parameter = array(

                        'act_mode' => 'loyalty', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '1',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '' );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedure('proc_order', $parameter);

      $config['base_url']         = base_url()."/admin/order/loyalty?";
      $config['total_rows']       = count($response['vieww']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter = array(

                        'act_mode' => 'loyalty', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '0',
                        'param7' => $page,
                        'param8' => $second,
                        'param9' => '',
                        'param10' => '' );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedure('proc_order', $parameter);


      $this->load->view('helper/header');      
      $this->load->view('order/loyalty',$response);
  }

  public function orders() {

  $loginid  =$this->session->userdata('lw_login')->s_loginid;
  $logintype=$this->session->userdata('lw_login')->s_logintype;
  $countryid=$this->session->userdata('lw_login')->s_countryid;
  $stateid  =$this->session->userdata('lw_login')->s_stateid;
  $type=$this->uri->segment(4);

       $parameter = array(

                        'act_mode' => 'orderdata', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => $type,
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '' );

 //p($parameter); exit;
       $response['vieww'] = $this->supper_admin->call_procedure('proc_order', $parameter);
        

      $this->load->view('helper/header'); 
      if($type=='OF')  
      {
          $this->load->view('order/offers',$response);
      } 
      else if($type=='PC')  
      {
          $this->load->view('order/punch',$response);
      }  
      else if($type=='PCV')  
      {
          $this->load->view('order/voucher',$response);
      }  
      
  }

}
?>
