<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Access extends MX_Controller {
  public $data   =   array();
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
  }

public function pin(){
  $id=decrypt($this->uri->segment(2));

   $result = null;
      if($this->input->post('submit')) {
          $this->form_validation->set_rules('newpin', 'New Pin','trim|required|max_length[4]|min_length[4]|numeric');
          $this->form_validation->set_rules('conpin', 'Confirm Pin','trim|required|max_length[4]|xss_clean');
          if($this->form_validation->run() == true) {

            if($this->input->post('newpin')==$this->input->post('conpin'))
            {
                $parameter = array(
                  'useremail'    => '',
                  'userpassword' => base64_encode(trim($this->input->post('newpin'))),
                  'act_mode'     => 'resetpinmerchant',
                  'row_id'       => $id);
              $data = $this->supper_admin->call_procedureRow('proc_Adminlogin', $parameter);
              $result['result']='<span style="color:green">Pin set successful!</span>';
            }
            else
            {
                $result['result']='<span style="color:red">Confirm Pin Not Match!</span>';
            }
             
          }
      }

  $this->load->view('merchant/newpin',$result);

     
}


}