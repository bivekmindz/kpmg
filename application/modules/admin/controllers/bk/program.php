<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Program extends MX_Controller {
  public $data   =   array();
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->userfunction->loginAdminvalidation();
  }

  public function punch() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'merchantcategory',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['category'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

     $parameter = array('act_mode'=>'punchproducttyp','row_id'=>'','counname'=>'','coucode'=>'','commid'=>'');
     $response['protype']= $this->supper_admin->call_procedure('proc_master',$parameter);

     
      if($this->input->post('punchsubmit')=='Submit')
      {
          if(!empty($_FILES['oimg']['name'])) 
          {
            $upload_name =  time().str_replace(' ', '_', strtolower($_FILES['oimg']['name']));
            $filePath = FCPATH.'public/offer/'.$upload_name;
            $tmpFilePath = $_FILES['oimg']['tmp_name'];
            if(move_uploaded_file($tmpFilePath, $filePath)) 
            {
              $filename = $upload_name;
            } 
            else 
            {
              $filename = "";
            } 
          }
          else 
          {
            $filename = "";
          } 
       
                      $param = array(

                        'act_mode' => 'addprogram', 
                        'param2' => 'PC',//Punch Card
                        'param3' => $id,#merchant ID
                        'param4' => $this->input->post('category'),
                        'param5' => $this->input->post('noofpunch'),
                        'param6' => $this->input->post('pcount'),
                        'param7' => $this->input->post('fcount'),
                        'param8' => $this->input->post('imgaccess'),#1=Card Img,2=Punch Img
                        'param9' => $this->input->post('product'),
                        'param10' => '',
                        'param11' => '',
                        'param12' => $filename,
                        'param13' => $this->input->post('sdate'),
                        'param14' => $this->input->post('edate'),
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => $this->input->post('term'),
                        'param25' => $this->input->post('how'),
                        'param26' => '',
                        'param27' => '',
                        'param28' => $this->input->post('faq'),
                        'param29' => '0',
                        'param30' => $loginid,//Login ID
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
        
              $this->session->set_flashdata('message', 'Your information was successfully saved.');
              redirect("admin/program/punch/".$id);
      }


      $this->load->view('helper/header');
      $this->load->view('merchant/punchcard',$response);
  } 

 public function currency() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'merchantcountry',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

     
      if($this->input->post('currencysubmit')=='Submit')
      {

        
        $cou = explode("#", $this->input->post('country'));


       $parameter = array(

                        'act_mode' => 'setcurrency', 
                        'param2' => '',
                        'param3' => $id,
                        'param4' => $loginid,
                        'param5' => $cou[0],
                        'param6' => $this->input->post('curr'),
                        'param7' => $this->input->post('currency'),
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => '',
                        'param12' => '',
                        'param13' => '',                      
                        'param14' => '',
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => ''
                      );

           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_welcome', $parameter);
              $this->session->set_flashdata('message', 'Your information was successfully saved.');
              redirect("admin/program/currency/".$id);
      }

    $parameter= array(
      'act_mode'=>'getsetcurrency',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['getsetcurrency'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);


      $this->load->view('helper/header');
      $this->load->view('merchant/currency',$response);
  } 

  public function loyalty() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'merchantcategory',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['category'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

     $parameter= array(
      'act_mode'=>'merchantsetcurloy',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
     //p($response['country']);

     
      if($this->input->post('loyaltysubmit')=='Submit')
      {
       
                      $param = array(

                        'act_mode' => 'addprogram', 
                        'param2' => 'LP',//Loyalty Program
                        'param3' => $id,#merchant ID
                        'param4' => $this->input->post('category'),
                        'param5' => $this->input->post('noofuser'),
                        'param6' => $this->input->post('oqty_pcount'),
                        'param7' => $this->input->post('oday_fcount'),
                        'param8' => $this->input->post('odayvalue_earnc'),
                        'param9' => $this->input->post('of_pr_type'),
                        'param10' => $this->input->post('oftypevalue_earnp'),
                        'param11' => $this->input->post('oname_burnc'),
                        'param12' => $this->input->post('oimg_burnp'),
                        'param13' => $this->input->post('sdate'),
                        'param14' => $this->input->post('edate'),
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => $this->input->post('term'),
                        'param25' => $this->input->post('how'),
                        'param26' => '',
                        'param27' => '',
                        'param28' => $this->input->post('faq'),
                        'param29' => '0',
                        'param30' => $loginid,//Login ID
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
        
              $this->session->set_flashdata('message', 'Your information was successfully saved.');
              redirect("admin/program/loyalty/".$id);
      }


      $this->load->view('helper/header');
      $this->load->view('merchant/loyalty',$response);
  } 

  public function card() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;
    $logintype=$this->session->userdata('lw_login')->s_logintype;
    if($logintype==2) 
    { 
      $status=0;
    }
    else
    {
      $status=0;
    }
    $id=$this->uri->segment(4);   

     $parameter= array(
      'act_mode'=>'merchantcategory',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['category'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);


      if($this->input->post('cardsubmit')=='Submit')
      {

         if(!empty($_FILES['cardfilename']['name'])) {
                  $upload_name =  time().str_replace(' ', '_', strtolower($_FILES['cardfilename']['name']));
                  $filePath = FCPATH.'public/card/'.$upload_name;
                  $tmpFilePath = $_FILES['cardfilename']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $filename = $upload_name;
                  
              } 
              else 
              {
                    $filename = "";
              } 
              }
              else 
              {
                    $filename = "";
              } 
              

        
                $parameter= array(
                'act_mode'=>'addcard',
                'param2'=>$id,
                'param3'=>$this->input->post('category'),
                'param4'=>$this->input->post('cardname'),
                'param5'=>$filename,
                'param6'=>$status,
                'param7'=>$loginid,
                'param8'=>'',
                'param9'=>'',
                'param10'=>''
                );
                $response = $this->supper_admin->call_procedureRow('proc_other',$parameter);
              
            
            $this->session->set_flashdata('message', 'Your information was successfully saved!');
            redirect("admin/program/card/".$id);
         
      }

      $parameter= array(
      'act_mode'=>'getcard',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['getcard'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);



      $this->load->view('helper/header');
      $this->load->view('merchant/card',$response);
}

public function cardstatus (){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5)==1?'0':'1';
  $mrid          = $this->uri->segment(6);
  $catid         = $this->uri->segment(7);
  $loginid = $this->session->userdata('lw_login')->s_loginid;

            $parameter= array(
                'act_mode'=>'cardstatus',
                'param2'=>$rowid,
                'param3'=>$status,
                'param4'=>$mrid,
                'param5'=>$catid,
                'param6'=>$loginid,
                'param7'=>'',
                'param8'=>'',
                'param9'=>'',
                'param10'=>''
                );
                $response = $this->supper_admin->call_procedureRow('proc_other',$parameter);
 
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/program/card/'.$mrid);
}

public function offer() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4);   

    $parameter= array(
      'act_mode'=>'merchantcategory',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['category'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

      $parameter= array(
      'act_mode'=>'offertype',
      'row_id'=>'',
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['offertype'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

      if($this->input->post('offersubmit')=='Submit')
      {
        if($this->input->post('oday')==1)
        {
           $dayvalue='Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday';
        }
        else
        {
          $dayvalue=implode(',', $this->input->post('days'));
        }

       if(!empty($_FILES['oimg']['name'])) {
                  $upload_name =  time().str_replace(' ', '_', strtolower($_FILES['oimg']['name']));
                  $filePath = FCPATH.'public/offer/'.$upload_name;
                  $tmpFilePath = $_FILES['oimg']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $filename = $upload_name;
                  
              } 
              else 
              {
                    $filename = "";
              } 
              }
              else 
              {
                    $filename = "";
              } 

                      $param = array(

                        'act_mode' => 'addprogram', 
                        'param2' => 'OF',//Loyalty Program
                        'param3' => $id,#merchant ID
                        'param4' => $this->input->post('category'),
                        'param5' => $this->input->post('noofuser'),
                        'param6' => $this->input->post('oqty'),
                        'param7' => $this->input->post('oday'),
                        'param8' => $dayvalue,
                        'param9' => $this->input->post('offertype'),
                        'param10' => $this->input->post('offvalue').' '.$this->input->post('vtype'),
                        'param11' => $this->input->post('oname'),
                        'param12' => $filename,
                        'param13' => $this->input->post('sdate'),
                        'param14' => $this->input->post('edate'),
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => $this->input->post('term'),
                        'param25' => $this->input->post('desc'),
                        'param26' => '',
                        'param27' => '',
                        'param28' => $this->input->post('tagline'),
                        'param29' => '0',
                        'param30' => $loginid,//Login ID
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
        
              $this->session->set_flashdata('message', 'Your information was successfully saved.');
              redirect("admin/program/offer/".$id);
      }

    

      $this->load->view('helper/header');
      $this->load->view('merchant/offer',$response);
  } 

  public function programlist() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $type=$this->uri->segment(4); 
    if(empty($this->uri->segment(5)))
    {
      $merid=0;
    }
    else
    {
      $merid=$this->uri->segment(5);   
    }
    
      $parameter= array(
      'act_mode'=>'programlist',
      'row_id'=>$merid,
      'counname'=>$type,
      'coucode'=>'',
      'commid'=>''
      );
     $response['programlist'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
      //p($response['programlist']);

      $this->load->view('helper/header');
      if($type=='LP')
      {
          $this->load->view('merchant/loyaltylist',$response);
      }
      else if($type=='OF')
      {
          $this->load->view('merchant/offerlist',$response);
      }
      else if($type=='PC')
      {
          $this->load->view('merchant/punchcardlist',$response);
      }

   }


   public function programoutlet() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;
    $merid = $this->session->userdata('lw_login')->s_merchantid;
    $catid = $this->session->userdata('lw_login')->catid;

    $type=$this->uri->segment(4); 
   
      $parameter= array(
      'act_mode'=>'programlistoutlet',
      'row_id'=>$merid,
      'counname'=>$type,
      'coucode'=>'',
      'commid'=>$catid
      );
      //p($parameter);
     $response['programlist'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
      //p($response['programlist']);

      $this->load->view('helper/header');
      if($type=='LP')
      {
          $this->load->view('merchant/loyaltylist',$response);
      }
      else if($type=='OF')
      {
          $this->load->view('merchant/offerlist',$response);
      }
      else if($type=='PC')
      {
          $this->load->view('merchant/punchcardlist',$response);
      }

   }

public function punchofferstatus(){
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5)==1?'0':'1';
  $mrid          = $this->uri->segment(6);
  $type          = $this->uri->segment(7);
  $catid          = $this->uri->segment(8);
  $loginid = $this->session->userdata('lw_login')->s_loginid;
if($type=='LP')
{
    $act_mode='loyaltyprostatus';
}
else
{
    $act_mode='punchofferstatus';
}

            $parameter= array(
                'act_mode'=>$act_mode,
                'param2'=>$rowid,
                'param3'=>$status,
                'param4'=>$catid,
                'param5'=>$mrid,
                'param6'=>$loginid,
                'param7'=>'',
                'param8'=>'',
                'param9'=>'',
                'param10'=>''
                );
                $response = $this->supper_admin->call_procedureRow('proc_other',$parameter);
 
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  if(empty($this->uri->segment(9)))
  {
    redirect(base_url().'admin/program/unpublishprogram/'.$type);
  }
  else
  {
    redirect(base_url().'admin/program/programlist/'.$type.'/'.$mrid);
  }
}

public function unpublishprogram() {

  $loginid  =$this->session->userdata('lw_login')->s_loginid;
  $logintype=$this->session->userdata('lw_login')->s_logintype;
  $countryid=$this->session->userdata('lw_login')->s_countryid;
  $stateid  =$this->session->userdata('lw_login')->s_stateid;
  $emptype  =$this->session->userdata('lw_login')->s_merchantid;

    $type=$this->uri->segment(4); 
    $parameter = array(

                        'act_mode' => 'unpublishprogram', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '1',
                        'param7' => $type,
                        'param8' => '',
                        'param9' => '',
                        'param10' => $emptype );

 //p($parameter); exit;
       $response['programlist'] = $this->supper_admin->call_procedure('proc_order', $parameter);

      $config['base_url']         = base_url()."/admin/program/unpublishprogram/".$type."?";
      $config['total_rows']       = count($response['programlist']);
      $config['per_page']         = 10;
      $config['use_page_numbers'] = TRUE;

     $this->pagination->initialize($config);
     if(isset($_GET['page'])){
     if($_GET['page']){ 
       $page         = $_GET['page']-1 ;
       $page         = ($page*10);
       $second       = $config['per_page'];
     }
     else{
       $page         = 0;
       $second       = $config['per_page'];
     }
    } else {
      $page         = 0;
      $second       = $config['per_page'];
    }

     
     $str_links = $this->pagination->create_links();
     $response["links"]  = explode('&nbsp;',$str_links );

    $parameter = array(

                        'act_mode' => 'unpublishprogram', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '0',
                        'param7' => $type,
                        'param8' => $page,
                        'param9' => $second,
                        'param10' => $emptype );

 //p($parameter); exit;
       $response['programlist'] = $this->supper_admin->call_procedure('proc_order', $parameter);
    
      //p($response['programlist']);

      $this->load->view('helper/header');
      if($type=='LP')
      {
          $this->load->view('merchant/loyaltylist',$response);
      }
      else if($type=='OF')
      {
          $this->load->view('merchant/offerlist',$response);
      }
      else if($type=='PC')
      {
          $this->load->view('merchant/punchcardlist',$response);
      }

   }

    public function editprogram() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $id=$this->uri->segment(4); 
    $merid=$this->uri->segment(5);  
    $type=$this->uri->segment(6);

    $parameter= array(
      'act_mode'=>'merchantcategory',
      'row_id'=>$merid,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['category'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

     $parameter= array(
      'act_mode'=>'merchantsetcurloy',
      'row_id'=>$merid,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['country'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
     //p($response['country']);

     $parameter= array(
      'act_mode'=>'editprogramview',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['view'] = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);
//p($response['view']);
     
      if($this->input->post('program')=='Submit')
      {
        if($type=='LP')
        {
            $filename=$this->input->post('oimg_burnp');
            $earnday=$this->input->post('odayvalue_earnc');
        } 
        else
        {
            if($type=='PC')
            {
                $earnday=$this->input->post('imgaccess');
            } 
            else
            {
                if($this->input->post('oday_fcount')==1)#fully,partially
                {
                  $earnday='Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday';
                }
                else
                {
                  $earnday=implode(',', $this->input->post('days'));
                }
            }

            if(!empty($_FILES['oimg']['name'])) 
            {
              $upload_name =  time().str_replace(' ', '_', strtolower($_FILES['oimg']['name']));
              $filePath = FCPATH.'public/offer/'.$upload_name;
              $tmpFilePath = $_FILES['oimg']['tmp_name'];
              if(move_uploaded_file($tmpFilePath, $filePath)) 
              {
                $filename = $upload_name;
              } 
              else 
              {
                $filename = $this->input->post('oimg1');
              } 
            }
            else 
            {
              $filename = $this->input->post('oimg1');
            }
        }
        
       
                      $param = array(

                        'act_mode' => 'updateprogram', 
                        'param2' => '',
                        'param3' => $id,#Update ID
                        'param4' => $this->input->post('category'),#not update
                        'param5' => $this->input->post('noofuser'),
                        'param6' => $this->input->post('oqty_pcount'),
                        'param7' => $this->input->post('oday_fcount'),
                        'param8' => $earnday,#earnc,days
                        'param9' => $this->input->post('of_pr_type'),
                        'param10' => $this->input->post('oftypevalue_earnp'),
                        'param11' => $this->input->post('oname_burnc'),
                        'param12' => $filename,#img,burn point
                        'param13' => $this->input->post('sdate'),
                        'param14' => $this->input->post('edate'),
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => '',
                        'param19' => '',
                        'param20' => '',
                        'param21' => '',
                        'param22' => '',
                        'param23' => '',
                        'param24' => $this->input->post('term'),
                        'param25' => $this->input->post('how'),
                        'param26' => '',
                        'param27' => '',
                        'param28' => $this->input->post('faq'),
                        'param29' => '',
                        'param30' => $loginid,//Login ID
                        'param31' => '',
                        'param32' => '',
                        'param33' => '',
                        'param34' => '',
                        'param35' => '',
                        'param36' => ''  
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_merchant', $param);
        
              $this->session->set_flashdata('message', 'Your information was successfully updated.');
              redirect("admin/program/editprogram/".$id.'/'.$merid.'/'.$type);
      }

      if($type=='LP')
      {
        $viewpage='loyaltyedit';
      }
      else if($type=='OF')
      {
        $viewpage='offeredit';
      }
      else if($type=='PC')
      {
        $viewpage='punchcardedit';
      }


      $this->load->view('helper/header');
      $this->load->view('merchant/'.$viewpage,$response);
  } 


}