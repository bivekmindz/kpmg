<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Card extends MX_Controller {
  public $data   =   array();
  public function __construct() {
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->userfunction->loginAdminvalidation();
  }

  public function addpromotion() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

      $parameter= array(
      'act_mode'=>'merchantcategory',
      'row_id'=>$loginid,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['category'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);

     $parameter = array('act_mode'=>'viewactivecountry', 'row_id'=>'', 'counname'=>'', 'coucode'=>'', 'commid'=>'');
    $response['viewcountry'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
     
      if($this->input->post('cardsubmit')=='Submit')
      {
       
        if(!empty($_FILES['cardimage']['name'])) {
                  $upload_name =  time().strtolower(str_replace(' ', '-', $_FILES['cardimage']['name']));
                  $filePath = FCPATH.'public/card/'.$upload_name;
                  $tmpFilePath = $_FILES['cardimage']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $filename = $upload_name;
                  
              } else {
                    $filename = '';
                  } 
              }else{
                
                
                 $filename = '';
              }   
        
                 $param = array(

                        'act_mode' => 'addpromotion', 
                        'param2' => $this->input->post('category'),
                        'param3' => $this->input->post('amount'),
                        'param4' => $this->input->post('merchantid'),
                        'param5' => $loginid,//created by
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => $this->input->post('cdesc'),
                        'param12' => $this->input->post('sdate'),
                        'param13' => $this->input->post('edate'),
                        'param14' => $filename,
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => ''
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_welcome', $param);
              //p($response->valid);exit;
              if($response->valid==0)
              {
                  $this->session->set_flashdata('message', 'Your information was successfully saved.');
                  if($this->session->userdata('lw_login')->s_logintype!=2){
                  redirect("admin/card/promotionlist");
                }
                else
                {
                  redirect("admin/card/promotionlistm");
                }
              }
              else
              {
                $this->session->set_flashdata('message', 'allready Exist!.');
                redirect("admin/card/addpromotion");
              }
      }


      $this->load->view('helper/header');
      $this->load->view('card/promotionadd',$response);
  } 

 public function promotionlistm() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

    $parameter= array(
      'act_mode'=>'merchantcardlist',
      'row_id'=>$loginid,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['list'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
//p($response['list']);
      $this->load->view('helper/header');
      $this->load->view('card/promotionlist',$response);
  } 

  public function promotionlist() {

  $logintype=$this->session->userdata('lw_login')->s_logintype;
  $countryid=$this->session->userdata('lw_login')->s_countryid;
  $stateid=$this->session->userdata('lw_login')->s_stateid;

      $parameter = array(
      'act_mode' => 'promotionbannerlist',
      'row_id'=>'',
      'counname'=>$logintype,
      'coucode'=>$countryid,
      'commid'=>$stateid
      );
     $response['list'] = $this->supper_admin->call_procedure('proc_geographic',$parameter);
//p($response['list']);
      $this->load->view('helper/header');
      $this->load->view('card/promotionlist',$response);
  } 

public function promotionstatus (){
   $loginid = $this->session->userdata('lw_login')->s_loginid;
  $rowid         = $this->uri->segment(4);
  $status        = $this->uri->segment(5);
  //p($rowid);
  //p($status); die;
  $val      = $status==P?'1':'2';

  $parameter = array('act_mode'=>'paidcardstatus','row_id'=>$rowid,'counname'=>$val,'coucode'=>$loginid,'commid'=>'');
  $record = $this->supper_admin->call_procedureRow('proc_master',$parameter);
  
  $this->session->set_flashdata('message', 'Your Status was successfully Updated.');
  redirect(base_url().'admin/card/promotionlist');
}

 public function editpromotion() {

    $loginid = $this->session->userdata('lw_login')->s_loginid;

      $id=$this->uri->segment(4);
      $parameter= array(
      'act_mode'=>'editcardlist',
      'row_id'=>$id,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $response['card'] = $this->supper_admin->call_procedureRow('proc_geographic',$parameter);

     //p($response['card']);
      if($this->input->post('cardsubmit')=='Submit')
      {
       
        if(!empty($_FILES['cardimage']['name'])) {
                  $upload_name =  time().strtolower(str_replace(' ', '-', $_FILES['cardimage']['name']));
                  $filePath = FCPATH.'public/card/'.$upload_name;
                  $tmpFilePath = $_FILES['cardimage']['tmp_name'];
                  if(move_uploaded_file($tmpFilePath, $filePath)) {
                    $filename = $upload_name;
                  
              } else {
                    $filename = $this->input->post('cardimage1');
                  } 
              }else{
                
                
                 $filename = $this->input->post('cardimage1');
              }   
        
                 $param = array(

                        'act_mode' => 'editpromotion', 
                        'param2' => $id,
                        'param3' => $this->input->post('amount'),
                        'param4' => '',
                        'param5' => $loginid,//created by
                        'param6' => '',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '',
                        'param11' => $this->input->post('cdesc'),
                        'param12' => $this->input->post('sdate'),
                        'param13' => $this->input->post('edate'),
                        'param14' => $filename,
                        'param15' => '',
                        'param16' => '',
                        'param17' => '',
                        'param18' => ''
                        
                   );
           // p($param); exit;

              $response = $this->supper_admin->call_procedureRow('proc_welcome', $param);
              //p($response->valid);exit;
                  $this->session->set_flashdata('message', 'Your information was successfully saved.');
                  if($this->session->userdata('lw_login')->s_logintype!=2)
                  {
                    redirect("admin/card/promotionlist");
                  }
                  else
                  {
                    redirect("admin/card/promotionlistm");
                  }
             
      }


      $this->load->view('helper/header');
      $this->load->view('card/promotionedit',$response);
  } 


}