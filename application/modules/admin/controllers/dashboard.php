<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    $this->userfunction->loginAdminvalidation();
    
  }
  
public function index() 
{    
    $loginid  =$this->session->userdata('lw_login')->s_loginid;
    $logintype=$this->session->userdata('lw_login')->s_logintype;
    $countryid=$this->session->userdata('lw_login')->s_countryid;
    $stateid=$this->session->userdata('lw_login')->s_stateid;

    /*================ Super Admin/Employee Start==================*/

      $parameter = array(
                  'useremail'    => '16,30,39',#assigned menu id
                  'userpassword' => $loginid,
                  'act_mode'     => 'checkmenu',
                  'row_id'       => '');
      $empmenu = $this->supper_admin->call_procedure('proc_Adminlogin', $parameter);

       $menu='';
       foreach ($empmenu as $key => $value) 
       {
         $menu .=$value->sub_menu.',';
       }

       $response['menu']=$menu;

      $parameter = array(
      'act_mode' => 'merchantlist',
      'row_id'=>'2',
      'counname'=>$logintype,
      'coucode'=>$countryid,
      'commid'=>$stateid
      );
       //p($parameter); exit;
     $merchant = $this->supper_admin->call_procedure('proc_geographic',$parameter);
     $response['merchant']=count($merchant);

 if($logintype==1)
 {
    $countid='-1';
 }
 else
 {
    $countid=$countryid;
 }

        $parameter = array(
                  'useremail'    => '',
                  'userpassword' => $countid,
                  'act_mode'     => 'viewuser',
                  'row_id'       => '');
    $user = $this->supper_admin->call_procedure('proc_Adminlogin', $parameter);
    $response['user']=count($user);
/*

   $parameter = array(

                        'act_mode' => 'loyalty', 
                        'param2' => $logintype,
                        'param3' => $loginid,
                        'param4' => $countryid,
                        'param5' => $stateid,
                        'param6' => '1',
                        'param7' => '',
                        'param8' => '',
                        'param9' => '',
                        'param10' => '' );

 //p($parameter); exit;
       $loyalty = $this->supper_admin->call_procedure('proc_order', $parameter);
       $earn=0;
       $burn=0;
       $earnc=0;
       $burnc=0;
       foreach ($loyalty as $key => $value) 
       {
         if($value->po_trantype=='Earn')
         {
            $earn=$earn+$value->po_points;
            $earnc=$earnc+$value->po_currency;
         }
         else
         {
            $burn=$burn+$value->po_points;
            $burnc=$burnc+$value->po_currency;
         }
       }

       $response['earn']=$earn;
       $response['burn']=$burn;
       $response['earnc']=$earnc;
       $response['burnc']=$burnc;
*/
       /*================ Super Admin/Employee End ==================*/
       /*================ Merchant Start ==================*/

      $parameter = array('act_mode'=>'merchantoutletlist','row_id'=>$loginid,'counname'=>'','coucode'=>'','commid'=>'');
      $list= $this->supper_admin->call_procedure('proc_master',$parameter);
      $response['outlet']=count($list);

      $parameter= array(
      'act_mode'=>'merchantsetcurloy',
      'row_id'=>$loginid,
      'counname'=>'',
      'coucode'=>'',
      'commid'=>''
      );
     $country = $this->supper_admin->call_procedure('proc_geographic',$parameter);
      $response['sign']=$country[0]->csign;
       /*================ Merchant End ==================*/
$response['logintype']=$this->session->userdata('lw_login')->s_logintype;
   $this->load->view('helper/header',$response);
    $this->load->view('dashboard/dashboard',$response);
}


 public function changepassword()
  {
  
     if (isset($_POST["submit"]))
    {
           $parameter = array(
                  'useremail'    => '',
                  'userpassword' => '',
                  'act_mode'     => 'getpass',
                  'row_id'       => $this->session->userdata('lw_login')->s_loginid);

              $response = $this->supper_admin->call_procedureRow('proc_Adminlogin', $parameter);
          
          if($response->s_password==base64_encode(trim($this->input->post('oldpass'))))
          {

            $parameter = array(
                  'useremail'    => '',
                  'userpassword' => base64_encode(trim($this->input->post('newpass'))),
                  'act_mode'     => 'changepass',
                  'row_id'       => $this->session->userdata('lw_login')->s_loginid);

              $response = $this->supper_admin->call_procedureRow('proc_Adminlogin', $parameter);
          
          $this->session->set_flashdata('message', 'Your password has been changed successfully!');
          //redirect(base_url('lw-change-password'));
          $logintype=$this->session->userdata('lw_login')->s_logintype;
          if($logintype==2 || $logintype==3)
          { 
            redirect('partner-login');
          }
          else
          {
            redirect('lw-admin');
          }
          
        }
        else
        {
          
          $this->session->set_flashdata('message', 'Old password does`t match!');
          redirect(base_url('lw-change-password'));
         
        }
}

    $this->load->view('helper/header');      
    $this->load->view('dashboard/changepassword', $response);
  }

}#class
?>