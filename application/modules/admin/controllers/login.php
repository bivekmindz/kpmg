<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
    //$this->userfunction->loginAdminvalidation();
    
  }
  
  public function authenticate() {
      $result = null;
      if($this->input->post('submit')) {
          $this->form_validation->set_rules('email', 'Email','trim|required|xss_clean|valid_email');
          $this->form_validation->set_rules('password', 'Password','trim|required|max_length[20]|xss_clean');
          if($this->form_validation->run() == true) {
              $parameter = array(
                  'useremail'    => trim($this->input->post('email')),
                  'userpassword' => base64_encode(trim($this->input->post('password'))),
                  'act_mode'     => 'login',
                  'row_id'       => '');
              $data['result'] = $this->supper_admin->call_procedureRow('proc_Adminlogin', $parameter);

              if(!empty($data['result'])) 
              {
                  $this->session->set_userdata('lw_login',$data['result']); 
                  redirect('kpmg-dashboard'); 
              } 
              else
              {
                  $result['result']='<span style="color:red">Wrong login credentials!</span>';
              }
          }
      }
      $this->load->view("dashboard/login",$result);
  }

  //.............  Forgot Pass ............... //  
  public function forgotpass(){

    $result = null;
    if($this->input->post('submit'))
    {  
        $this->form_validation->set_rules('email', 'Email','trim|required|xss_clean|valid_email');
        if($this->form_validation->run() == true) 
        {
              $letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
              $char = "!@#$%^&*()!@#$%^&*()";
              $small = "abcdefghijklmnopqrstuvwxyz";
              $numbers = rand(1000, 9999);
              //$prefix = "B";
              $prefix = $small[rand(0, 25)];
              $special = $char[rand(0, 20)];
              $special1 = $char[rand(0, 10)];
              $sufix = $letters[rand(0, 25)];

              $email=trim($this->input->post('email'));
              $pass = $sufix . $numbers . $special . $prefix . $special1 ;
                $parameter = array(
                        'useremail'    => $email,
                        'userpassword' => base64_encode($pass),
                        'act_mode'     => 'forgotpass',
                        'row_id'       => '');
            $result = $this->supper_admin->call_procedureRow('proc_Adminlogin', $parameter);
      if($result->res==1)
      {
             
        $content='<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title></title>
<link href="https://fonts.googleapis.com/css?family=Rubik:400,700" rel="stylesheet">
<style>
.main_div{  line-height:20px;max-width: 800px; width:100%;overflow:hidden; border: solid 1px #CCCCCC; border-right: solid 1px #CCCCCC; font-family: Rubik, sans-serif; font-size:13px; margin:0 auto;}
.container{ width:100%; padding:0 10px; box-sizing:border-box;}
.header{width:100%; float:left; box-sizing: border-box;}
.logo{float:left; width:25%;  padding: 10px 20px;}
.right_div{float:right; width:37%; color:#FFF; padding:0px;}
.right_div a{ color:#fff; text-decoration:none;}
.right-txt{ text-align:left; width: 100%;
    float: left;    margin-left: 10px;}
.right-txt img{ width:100%; float:left; min-height: 102px;}
.body_contain{    width: 100%;
    float: left;color:#444;}
.body_contain ul.top-list{ padding:0px;list-style-type:none;}
.body_contain ul.top-list li:first-child{font-size:16px; color:#444; line-height:22px;}
.body_contain ul.top-list li.second{font-size:13px; color:#444; line-height:20px;}
.body_contain ul.top-list li:last-child{font-size:13px; color:#444; line-height:20px;}
.content{ width:100%; float:left;}
.content .left_div{ width:60%; float:left;}
.content .left_div ul{ padding:0px; width:100%; float:left;list-style-type:none;}
.content .left_div ul li{ width:100%; float:left;}
.content .left_div ul li .list-img{ float:left; padding:0 10px 0 0;}
.list_head{float:left; width:100%; font-size:17px; color:#ff1d00; padding:5px 0;}
.content .right_img{ width:40%; float:left; padding:5px; box-sizing:border-box;}
.footer{width:100%; float:left; background:#f0f0f0; border-top: 4px solid #e41b23;}
.left-f{padding: 0px 25px;color:#FFF; width:60%; float:left; color:#333; font-size:18px;}
.left-f a{ text-decoration:none; color:#6b6b6b;}
.right-f{ text-align:center; color:#fff; }
.user-form{ width:100%; float:left; padding: 15px 10px;}
.user-form p{ line-height:30px; }
.user-form ul{ list-style-type: disc;}
.thanks-re{ width:100%; float:left;}
.thanks-re p{ line-height:15px;}
.shadow img {width:100%;}
.shadow {width:100%;float:left;line-height:9px;}
@media screen and (max-width: 600px){
.logo{ width:100%; float:left; text-align:center;}  
.logo img{width:40%;}
.right_div{ width:100%; text-align:center;}
.right-txt{text-align: center; width:100%; float:left;}

.left-f{width:100%; float:left; text-align:center; line-height:15px; margin:0; padding:2px 0;}
.right-f{width:100%; float:left; text-align:center; padding:0px; line-height:15px; margin:0; padding:2px 0;}
.footer{ padding:5px 0;}
.content .left_div{ width:100%; float:left;}
.content .right_img{ width:100%; float:left;}
.shadow{ width:100%; float:left;}
}
</style>
</head>

<body>
<div class="main_div">
  <div class="header">
    <div class="container">
        <div class="logo">
            <img src="'.base_url().'assets/mail-img/logo.png">
      </div>
        <div class="right_div">
        <div class="right-txt">
        <img src="'.base_url().'assets/mail-img/all-img.png">
        
        </div>
        </div>
    </div>
  </div>
<div class="shadow"><img src="'.base_url().'assets/mail-img/line.png"></div>
<div class="body_contain">
  <div class="container">
     
        <div class="user-form">
        
        <p><strong>Dear Merchant,</strong> </p>
        <p>Your New Password: '.$pass.'</p>
        
        <div class="thanks-re">
        <p>Regards, </p>
        <p><strong>Team Ngage</strong></p>
        </div>
         
        </div>    
  
   </div>
</div>  
<div class="shadow"><img src="'.base_url().'assets/mail-img/line_footer.png"></div>
<div class="footer">
  <div class="container">
    <p class="left-f"><a href="'.base_url().'">www.letsngage.com</a></p>
    <p class="right-f"> <img src="'.base_url().'assets/mail-img/company_logo.png"></p>
    </div>
</div>    
    
</div>

</body>
</html>
';
        

        $this->load->library('email');
        $this->email->set_newline("\r\n");
        $this->email->from(mailfrome(),mailfromn());
        $this->email->to($email); 
        if(mailfromccstatus()==1){ $this->email->cc(mailfromcc()); } 
        $this->email->subject('Forgot Password');
        $this->email->message($content);
        $this->email->send();

        $msg='The Password Has Been Sent To Your Mail. Please Check Your Inbox/Spam!';
}
else
{
    $msg='Invalid Email!';
}
        $this->session->set_flashdata('message',$msg);
         redirect(base_url('kpmg-forgot-password'));
}
}
    
    $this->load->view("dashboard/forgotpass",$result);
  }

 
  public function logout(){
   
   $logintype=$this->session->userdata('lw_login')->s_logintype;
    if($logintype==1 || $logintype==6)
    { 
      redirect('kpmg-admin');
    }
    else
    {
      redirect('partner-login');
    }

    $this->session->unset_userdata('lw_login');
    
  }


}#class
?>