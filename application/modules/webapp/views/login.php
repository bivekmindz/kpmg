<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>KPMG</title>
  <link rel="stylesheet" href="<?php echo base_url();  ?>assets/webapp/css/app_style.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
  <script src="<?php echo base_url();  ?>assets/webapp/js/jquery-3.3.1.min.js"></script>
</head>
<body class="login-page">
   <form method="post" action="" id="addCont"> 
  <div class="login-box">
    <div class="login-box-left">
      <div class="logo">
        <img src="<?php echo base_url();  ?>assets/webapp/images/logo.png">
      </div>
        <div class="sessionerror"><p><?php echo $this->session->flashdata('message'); ?>
            <?php echo validation_errors();  if(isset($result) && $result!=''){echo ($result);}?></p></div>
      <div class="select_type">
        <div class="select_type_text">Select Login Type</div>
        <div class="select_type_box">
          <ul>
            <li><input id="hr" type="radio" name="acctype" value="hr" checked>
              <label for="hr"><img src="<?php echo base_url();  ?>assets/webapp/images/hr.png" ><span>HR</span></label></li>
            <li><input id="employee" type="radio" name="acctype" value="user">
              <label for="employee"><img src="<?php echo base_url();  ?>assets/webapp/images/employee.png"><span>Employee</span></label></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="login-box-right">
      <div class="login_box_text">Login</div>
      <div class="login-form">
        <div class="login_field">
          <label>Email </label>
          <input type="text" class="input_text" name="emailmob" required>
        </div>
        <div class="login_field">
          <label>Password</label>
          <input type="password" class="input_text" name="pwd"  required>
        </div>
        <div class="login_submit">
      
        <a href="">Forgot Password</a>
          <input type="hidden" class="input_text" name="current_url" value="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>">

          <input type="submit" value="LOGIN" name="submit">
        </div>
      </div>
    </div>
  </div></form> 
  <script>
    $(document).ready(function(){
        $(".input_text").focusin(function(){
        if($(this).val() == ""){
          $(this).parent().addClass("active");
        }
        else{
          $(this).parent().removeClass("active");
        }
        });
         $(".input_text").focusout(function(){
          
        if($(this).val() != ""){
          $(this).parent().addClass("active");
        }
        else{
          $(this).parent().removeClass("active");
        }
        });
      });
  </script>
</body>
</html>