<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Dashboard</title>
  <!--Css-->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url();  ?>assets/webapp/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url();  ?>assets/webapp/css/app_style.css">
    <link rel="stylesheet" href="<?php echo base_url();  ?>assets/webapp/bootstrap/font-awesome/css/font-awesome.min.css">


  <link rel="stylesheet" href="<?php echo base_url();  ?>assets/webapp/css/bootstrap-datepicker3.min.css">
  <link rel="stylesheet" href="<?php echo base_url();  ?>assets/webapp/css/bootstrap-datepicker3.min.css">
  <!--end Css-->

  <!--script-->
     <script type="text/javascript" src="<?php echo base_url();  ?>assets/webapp/js/jquery-3.3.1.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url();  ?>assets/webapp/js/bootstrap-datepicker.min.js"></script>
  <!--end script-->



  <!--end Css-->

  <!--script-->
  <!--    <script type="text/javascript" src="<?php echo base_url();  ?>assets/webapp/js/jquery-3.3.1.min.js"></script> -->
      <script type="text/javascript" src="<?php echo base_url();  ?>assets/webapp/js/anychart-core.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url();  ?>assets/webapp/js/anychart-pie.min.js"></script>
     <script type="text/javascript" src="<?php echo base_url();  ?>assets/webapp/js/paichart.js"></script>
     <script type="text/javascript" src="<?php echo base_url();  ?>assets/webapp/js/canvasjs.min.js"></script>
  <!--end script-->

</head>
<body>
  <div class="wrapper top_header_padding">
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-3">
            <div class="logo_page"><img src="<?php base_url();?>assets/webapp/images/logo.png" alt=""></div>
          </div>
          <div class="col-md-9 col-sm-9">
            <div class="top-right">Welcome <?php echo ucwords($this->session->userdata('lwuser_login')->t_FName.' '.$this->session->userdata('lwuser_login')->t_LName); ?>   |   <a href="<?php echo base_url();  ?>hrlogout">Logout</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper menu">
    <div class="container-fluid">
      <div class="container">
        
        <div class="menu_ul">
          <ul>
            <li><a class="active" href="<?php echo base_url();  ?>user-dashboard">When & Where</a></li>
            <li><a href="<?php echo base_url();  ?>employee-info">Employee Info</a></li>
            <li><a href="<?php echo base_url();  ?>employee-intro">Get Introduced</a></li>
            <li><a href="#">Task to Complete</a></li>
            <li><a href="#">Feedback</a></li>
          </ul>
        </div>
      
      </div>
    </div>
  </div>