<div class="wrapper">
<div class="container-fluid">
			<div class="container">
			<div class="row">
			<div class="emloyee_left">
			<div class="Employee_detail">
				<div class="em_box">
					<div class="em_image">
						<?php $proimgurl=base_url().'assets/admin/images/'.$this->session->userdata('lwuser_login')->user_profile; ?>
						<img src="<?php echo $proimgurl;?>" alt="">
					</div>
				</div>
				<div class="em_data">
					<div class="name"><?php echo ucwords($this->session->userdata('lwuser_login')->t_FName.' '.$this->session->userdata('lwuser_login')->t_LName); ?></div>
					<div class="email"><?php echo $this->session->userdata('lwuser_login')->user_email; ?></div>
					<div class="Mobile">+91 <?php echo $this->session->userdata('lwuser_login')->user_mobile; ?></div>
				</div>
			</div>
			</div>
			<div class="emloyee_mid">
				<div class="em_box box_padding">
					<div class="text-title">Welcome <?php echo ucwords($this->session->userdata('lwuser_login')->t_FName.' '.$this->session->userdata('lwuser_login')->t_LName); ?></div>
					<div class="cont_text">
						We are excited that you are joining us here at Your Own Co. We can't wait for you to start.We've included some helpful information and ask that you complete this new employee so you can hit the ground running on your first day. If you have any questions, please don't hesitate to ask.
					</div>
					<div class="text-title">Your First Day</div>
					<div class="cont_text">
						<ul class="first_day">
						<li>
							<i class="fa fa-calendar"></i><span>Monday, June 1, 2018</span>
						</li>
						<li>
							<i class="fa fa-clock-o"></i><span>8:00 AM</span>
						</li>
					</ul>
					</div>
					<div class="text-title">Who to Contact</div>
					<div class="cont_text">
						<div class="contact_person_img">
							<div class="person_img">
								<img src="<?php echo base_url();?>assets/webapp/images/emp_img02.png" alt="">
							</div>
						</div>
						<div class="contact_person_info">
							<div class="cont_name">Pam flores</div>
							<div class="cont_position">HR administrator</div>
							<div class="cont_mobile"><i class="fa fa-phone-square"></i><span>+99 9999 9999</span></div>
							<div class="cont_email"><i class="fa fa-envelope-square"></i><span>hr@kpmg.com</span></div>
						</div>
					</div>
					<div class="text-title">Other Insruction</div>
					<div class="cont_text">
						<ol class="other_inst">
							<li>You can wait in the lobby, and someone from HR with come down to greet you on the first day.</li>
							<li>
								Be sure to have your packet filled out, and we can't wait for you to start!!!
							</li></ol>
					</div>
				</div>
			</div>
			</div>
			</div>
		</div>
</div>