
<form action="" method='post'  enctype="multipart/form-data"
>

<div class="wrapper">
        <div class="container-fluid">
            <div class="container">
             <div class="bg_main">
              <div class="page_title">
                <span>Get Introduced</span> 
              </div>
                <div class="row">
                 <div class="col-md-12 col-sm-12">
                    <div class="emp_detail">
                    

                     <div class="personal_info">
                        <h1>Managers</h1>
                        <div class="row">
                      
                         <div class="col-md-6 col-sm-12">
                            <div class="info_detail">
                            <label>Line Manager</label>
                            <div class="input_box"></div>
                            </div> <!--end of info_detail-->
                         </div>
                         <div class="col-md-6 col-sm-12">
                            <div class="info_detail">
                            <label>Subject Matter Expert</label>
                            <div class="input_box"></div>
                            </div> <!--end of info_detail-->
                         </div>

                           
                        
                        </div>
                     </div> <!--end of personal_info-->

                     <div class="personal_info">
                        <h1>ORIENTATION DETAILS</h1>
                        <div class="row">
                         <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>Days</label>
                            <div class="input_box">Monday</div>
                            </div> <!--end of info_detail-->
                         </div>
                          <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>Date</label>
                            <div class="input_box">12/05/2018</div>
                            </div> <!--end of info_detail-->
                         </div>
                         <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>Time</label>
                            <div class="input_box">12:15 pm</div>
                            </div> <!--end of info_detail-->
                         </div>
                    

                          <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>City</label>
                            <div class="input_box">
                              Delhi </div>
                            </div> <!--end of info_detail-->
                         </div>
                      
                         
                        </div>
                     </div> <!--end of personal_info-->

                       <div class="personal_info">
                        <h1>TRAINING DETAILS</h1>
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>Trainer Name</label>
                            <div class="input_box">Umesh Yadav</div>
                            </div> <!--end of info_detail-->
                         </div>
                        
                          <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>Date</label>
                            <div class="input_box"> Monday 28/05/2018</div>
                            </div> <!--end of info_detail-->
                         </div>
                         <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>Time</label>
                            <div class="input_box">02:30 pm</div>
                            </div> <!--end of info_detail-->
                         </div>
                    

                          <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>City</label>
                            <div class="input_box">Pune</div>
                            </div> <!--end of info_detail-->
                         </div>
                              <div class="col-md-3 col-sm-12">
                            <div class="info_detail">
                            <label>Hours</label>
                            <div class="input_box">2 Hours</div>
                            </div> <!--end of info_detail-->
                         </div>

                        
                      
                         
                        </div>
                     </div> <!--end of personal_info-->

                    </div> <!--end of emp_detail-->
                 </div>
                </div>
            </div> <!--end of bg_main-->
          </div>
       </div> 

</form>
    