<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MX_Controller{

  public function __construct() {
    
    $this->load->model("supper_admin");
    $this->load->helper('my_helper');
    $this->load->library('PHPExcel');
    $this->load->library('PHPExcel_IOFactory');
   
    
  }
  
public function index() 
{   
  	$this->load->view("helper/header");
  	$this->load->view('dashboard',$response);
  	$this->load->view("helper/footer");
}
public function hrlogout() 
{   
  
  $this->session->sess_destroy();
  redirect('index');
}


}#class
?>