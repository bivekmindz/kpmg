<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = 'webapp/home/index';

$route['kpmg-admin'] = 'admin/login/authenticate';
$route['kpmg-logout'] = 'admin/login/logout';
$route['kpmg-dashboard'] = 'admin/dashboard';
$route['kpmg-change-password'] = 'admin/dashboard/changepassword';
$route['kpmg-forgot-password'] = 'admin/login/forgotpass';
$route['set-pin/(.*)'] = 'admin/merchant/setpin';
$route['set-outlets-pin'] = 'admin/merchant/outletsetpin';
$route['reset-pin/(.*)'] = 'admin/access/pin';
$route['pin'] = 'admin/merchant/resetpin';

$route['emp-dashboard'] = 'webapp/emp/index';
$route['partner-login'] = 'webapp/home/merchantlogin';
$route['forgot-password'] = 'webapp/home/forgotpass';

$route['404_override'] = 'webapp/home/index';
$route['translate_uri_dashes'] = FALSE;

	
