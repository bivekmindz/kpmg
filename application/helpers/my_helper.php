<?php

	function p($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}



	function pend($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		die();
	}

	
	function api_url(){
		
		$api_url =base_url();
		$result = $api_url."api/";
		return $result;
	}



if (!function_exists('fun_global')) {

    function fun_global($procname, $params = null) {
        $ci = &get_instance();
        $ci->load->model('supper_admin');
        $result = $ci->supper_admin->call_procedure("$procname", $params);
        return $result;
    }

  }



function convertToObject($array) {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = convertToObject($value);
            }
            $object->$key=$value;
        }
        return $object;
    }



	function randnumber(){
    srand ((double) microtime() * 1000000);
    $random5 = rand(10000,99999);
    return $random5;
  }

function seoUrl($string) 
    {
        $string = replaceAll($string);
        //return $string; exit();
        //source: http://stackoverflow.com/a/11330527/1564365
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        //$string = preg_replace("/[\s,]/", "-", $string);
        return strtolower($string);
    }



  function chardecode($ptext){
$array_from_to=array("Z1" =>"+",
      "Z2" => "-",
      "Z3" => "&",
      "Z5" => "|",
      "Z6" => "!",
      "Z7" => "(",
      "Z8" => ")",
      'Z9' => "[",
      'Zx1' =>"]",
      'Zx2' => "^",
      'Zx3' => '"',
      'Zx4' => "*",
      'Zx5' => "~",
      'Zx6' => "?",
      'Zx7' => ":",
      "Zx8" => "'",
      "Zx9" => "<",
      "Zy1" => ">",
      "Zy2" => "=");
       $ptext = strtr($ptext,$array_from_to);
 return $ptext;

  }


function mysql_time($time){

$date = DateTime::createFromFormat( 'H:i A', $time);
$formatted = $date->format( 'H:i:s');
  	return $formatted;

}


function encrypt($decrypted) { 

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'kpmg9898';
    $secret_iv = 'kpmg1239878';
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $gid1 = openssl_encrypt($decrypted, $encrypt_method, $key, 0, $iv);
    $encrypt_id = base64_encode($gid1);

    return $encrypt_id ;
} 

function decrypt($encrypted) {

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'kpmg9898';
     $secret_iv = 'kpmg1239878';
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $decrypt_id = openssl_decrypt(base64_decode($encrypted), $encrypt_method, $key, 0, $iv);
    
    return $decrypt_id;
}

function getWednesdays($y, $m, $d)
{
    return new DatePeriod(
        new DateTime("first ".$d." of $y-$m"),
        DateInterval::createFromDateString('next '.$d),
        new DateTime("last day of $y-$m+1")
    );
} 


function send_andriod_notification($deviceToken,$message,$notification_type)
{

//$server_key="AAAAnPqZQw8:APA91bHlpnulmgO6Mdh2nVSzSuQeukvkyBxSoFXN3lnJdHFb7wZMZuUDOI4QMxYfqgRaiSbx2LF3FFgcXyQ6uu8m_nATfw4arhwcSCNzB13FGxaLNDnz5_maLp0m15T_qVhEUedkcxLM";

//$server_key="AAAAkPBcJLc:APA91bESjGmVH-GpJhPdi4drjPc1cE6PvH0NrsbqGR6BUV0XK7viLV_1_j8aymF9rxNOGuD9WdRQQyAu_eOcP0EuT5VYvxR1NJRfOgWMXfcBBBvPn-TZZZrF3ZohzVRLnrIk45u3-He1";

$server_key="AAAAe8YoWCc:APA91bHie0_nT9mGUef8XOrAG6864HM2ttk3Sy0HMHXYTRmhBn_CVMvTeVxCEF8_xHxi8v08vN-zwrYtjvOPlImdpZpPvpopNBUKTBKFBC7m2eh7cnCQTjZfOcKrWITyQgfVSoklYJOv";


$data= array(
'message' => $message['message'],
'title' => $message['title'],
'subtitle'  => $message['title'],
'tickerText'=> $message['title'],
'result'=>$message['result'],
'vibrate' => 1,
'sound' => 1,
'notification_type'=>$notification_type

);

$url = 'https://fcm.googleapis.com/fcm/send';
     
$fields = array();
$fields['data'] = $data;
if(is_array($deviceToken))
{
  $fields['registration_ids'] = $deviceToken;
}else{
  $fields['to'] = $deviceToken;
}
//header with content_type api key
$headers = array(
  'Content-Type:application/json',
  'Authorization:key='.$server_key
);
   
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
$result = curl_exec($ch);
if ($result === FALSE) {
  die('FCM Send Error: ' . curl_error($ch));
}
curl_close($ch);
print_r($result);

}

function  send_ios_notification($deviceToken,$message,$notification_type)
{        

$passphrase = '12345';   
$file="MyPop_15Mar.pem";   
$ctx = stream_context_create();     
stream_context_set_option($ctx, 'ssl', 'local_cert', 'application/helpers/'.$file);        
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);        
$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);        
if (!$fp)           
 exit("Failed to connect: $err $errstr" . PHP_EOL);        
// echo 'Connected to APNS' . PHP_EOL;        

//$body['aps'] = array('alert' =>$message['message'],'notification_type'=>$notification_type,'data_array'=>$message['result'], 'sound' => 'default'); 
$body['aps'] = array('alert' =>$message['message'],'notification_type'=>$notification_type,'sound' => 'default','result' =>$message['result']);        
$payload = json_encode($body);        
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;        
$result = fwrite($fp, $msg, strlen($msg)); 
//print_r($result);
if (!$result) 
 echo 'Message not delivered' . PHP_EOL;       
  else    
echo 'Message successfully delivered' . PHP_EOL;        
fclose($fp);  
   }



?>
