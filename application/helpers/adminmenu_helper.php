<?php
function admin_mainmenu()
{

	
	$CI = & get_instance();  //get instance, access the CI superobject
  	
	

  	$isLoggedIn = $CI->session->all_userdata();
	
	
	$memId = $isLoggedIn['lw_login']->s_loginid;
	$userType = $isLoggedIn['lw_login']->s_logintype;
		
	$param = array('empid'=>$memId);
	

	$assignRole = fun_global('proc_getAssignRole',$param);

	

	if(!empty($assignRole))
	{
		foreach($assignRole as $k=>$v){

		$uMainMenu1[] = $v->main_menu;
		$uSubMenu1[] = $v->sub_menu;
	}
	$uMainMenu 	=	 	array_unique($uMainMenu1);
	$uSubMenu 	= 		array_unique($uSubMenu1);

}

	

	$parameter = array( 'login' => '' );
	

	
	$data['result'] = fun_global('proc_getAdminMenu',$parameter);

	if($memId==1){
		$msg = '<ul>';
		//$idr = 1;
			foreach($data['result'] as $values){
			 
			//if(in_array($values->id,$uMainMenu)){

				$msg .=' <li><a><i class="'.@$values->class.'"></i>'.$values->menuname.'<span class="'.str_replace(' ','',@$values->menuname).'"></span></a>';
                       $msg .= '<ul class="sub_menu">';
                        $idr++;
                        $parameter = array(

                        'userid' => '',
						'moduleid'	=>	$values->id);

                        $data1['submenu'] = fun_global('proc_leftMenu',$parameter);	

                    	foreach($data1['submenu'] as $submenu1){
                    		
                    		if (strpos($_SERVER['REQUEST_URI'].'/iw', '/lwallet/'.$submenu1->url.'/iw') !== false) {
    
                            $msg.='<li class="active_subnav"><a href='.base_url().$submenu1->url.'>'.$submenu1->menuname.'<span class="'.str_replace(' ','',@$submenu1->menuname).' "></span></a></li>';
                          } else {

                        		$msg.='<li><a href='.base_url().$submenu1->url.'>'.$submenu1->menuname.'<span class="'.str_replace(' ','',@$submenu1->menuname).' "></span></a></li>';
                        	}
                       
                        } 

                        $msg.='</ul>

                    </li>';
                   

			//}
     
     
	   } 
	   $msg .= '</ul>'; 

	} else if($userType==2) {
		$msg = '<ul>
			
			<li>
	            <a href="javascript:void(0);"><i class="fa fa-user"></i>My Profile</a>
	            <ul class="sub_menu">
	                <li><a href="'.site_url('admin/user/profile').'">Profile</a></li>
	            </ul>
	        </li>
	        <li>
	            <a href="javascript:void(0);"><i class="fa fa-bank"></i>My Outlets/Programs</a>
	            <ul class="sub_menu">
	            <li><a href="'.base_url('admin/merchant/addmerchant').'/'.$memId.'">Update Profile</a></li>
	            <li><a href="'.base_url('admin/merchant/merchantoutletlist').'/'.$memId.'">Outlets List</a></li>
	            <li><a href="'.base_url('admin/program/programlist/LP').'/'.$memId.'">Loyalty Program List</a></li>
	            <li><a href="'.base_url('admin/program/programlist/OF').'/'.$memId.'">Offers List</a></li>
	            <li><a href="'.base_url('admin/program/programlist/PC').'/'.$memId.'">Punch Card List</a></li>
	            </ul>
	        </li>
	        <li>
	            <a href="javascript:void(0);"><i class="fa fa-medium"></i>My Promotion</a>
	            <ul class="sub_menu">
	            		       <li><a href="'.base_url('admin/card/promotionlistm').'">Promotion List</a></li>
	            </ul>
	        </li>
	         <li>
	            <a href="javascript:void(0);"><i class="fa fa-exchange"></i>Set Transaction Pin</a>
	            <ul class="sub_menu">
	            <li><a href="'.base_url('set-pin/UUdOVWV6aDh0NzNxRWs1MitMZmorR3U3dzJtblhnTHRBbmttLytoRTQ3az0=').'">Merchant Pin</a></li>
	           <li><a href="'.base_url('set-outlets-pin').'">Outlets Pin</a></li>
	            </ul>
	        </li>
	         <li>
	            <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i>My Transaction</a>
	            <ul class="sub_menu">
	            <li><a href="'.base_url('admin/order/loyalty').'">Loyalty</a></li>
	            <li><a href="'.base_url('admin/order/orders/OF').'">Offer</a></li>
	            <li><a href="'.base_url('admin/order/orders/PC').'">Punch Card</a></li>
	            <li><a href="'.base_url('admin/order/orders/PCV').'">Punch Card Voucher</a></li>
	            </ul>
	        </li>
	        </ul>';
	         //$msg='<a href="javascript:void(0);"><i class="fa fa-user"></i>My Users</a><ul class="sub_menu"><li><a href="'.base_url('admin/user/users').'">Users List</a></li></ul></li>';

	} else if($userType==3) {
		$msg = '<ul>
			<li>
	            <a href="javascript:void(0);"><i class="fa fa-user"></i>My Profile</a>
	            <ul class="sub_menu">
	                <li><a href="'.site_url('admin/user/profile').'">Profile</a></li>
	            </ul>
	        </li>
	         <li>
	            <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i>My Programs</a>
	            <ul class="sub_menu">
	            <li><a href="'.base_url('admin/program/programoutlet/LP').'">Loyalty</a></li>
	            <li><a href="'.base_url('admin/program/programoutlet/OF').'">Offer</a></li>
	            <li><a href="'.base_url('admin/program/programoutlet/PC').'">Punch Card</a></li>
	            </ul>
	        </li>
	        <li>
	            <a href="javascript:void(0);"><i class="fa fa-shopping-bag"></i>My Transaction</a>
	            <ul class="sub_menu">
	            <li><a href="'.base_url('admin/order/loyalty').'">Loyalty</a></li>
	            <li><a href="'.base_url('admin/order/orders/OF').'">Offer</a></li>
	            <li><a href="'.base_url('admin/order/orders/PC').'">Punch Card</a></li>
	            <li><a href="'.base_url('admin/order/orders/PCV').'">Punch Card Voucher</a></li>
	            </ul>
	        </li>
	        <li>


	        </ul>';
	         //$msg='<a href="javascript:void(0);"><i class="fa fa-user"></i>My Users</a><ul class="sub_menu"><li><a href="'.base_url('admin/user/users').'">Users List</a></li></ul></li>';
			
	       
	   

	} else {	
			

			$msg = '<ul>';
			$msg .='<li>
	            <a href="javascript:void(0);"><i class="fa fa-user"></i>Profile</a>
	            <ul class="sub_menu">
	                <li><a href="'.site_url('admin/user/profile').'">View Profile</a></li>
	            </ul>
	        </li>';
	foreach($data['result'] as $values){


			 
			if(@in_array(@$values->id,$uMainMenu)){
				$msg .='<li><a><i class="'.@$values->class.'"></i>'.$values->menuname.'<span class="'.str_replace(' ','',$values->menuname).'"></span></a>';
                         $msg .= '<ul class="sub_menu">';
                        
                        $parameter = array(

                        'userid' => '',
						'moduleid'	=>	$values->id);

                        $data1['submenu'] = fun_global('proc_leftMenu',$parameter);	

                    	foreach($data1['submenu'] as $submenu1){
                    		if(in_array($submenu1->id,$uSubMenu)){

                    			if($submenu1->menuname=='View Leads')
                    			{
                    				$hideview = 'display:block';
                    			}else{
                    				$hideview = 'display:none';
                    			}

                        		$msg.='<li style=""><a href='.base_url().$submenu1->url.'>'.$submenu1->menuname.'<span class="'.str_replace(' ','',$submenu1->menuname).'"></span></a></li>';
                          }
                        } 

                        $msg.='</ul>

                    </li>';

			}
     
     
   } 

	}

               
     
     echo $msg ;

}


