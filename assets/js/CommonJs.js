//For Popup


$(function () {
    ValidateForm('login');
})

function FrmvalidateloginCredentials() {

    IsFormValidate('login')
    {
       

    }
}

$(function () {
    $(document).keyup(function (e) {

        if (e.keyCode == 27) {

            closePop();
        }

    });
});


var divi;
var fid;
function openPopup1(FocusInId, FocusOutID, dv_id, p_height, p_width,data) {
    fid = FocusOutID;
    var divid = typeof (dv_id) != 'object' ? $('#' + dv_id) : dv_id;
    divi = divid;
    divid.css('height', p_height);
    divid.css('width', p_width);
    divid.html(data);
    divid.fadeIn();
    $('.overlay').fadeIn();
    $('body').width($('body').width());
    $('body').css('overflow', 'hidden');
    $('#' + FocusInId).focus();
}
function openPopup(FocusInId, FocusOutID, dv_id, p_height, p_width) {
    fid = FocusOutID;
    var divid = typeof (dv_id) != 'object' ? $('#' + dv_id) : dv_id;
    divi = divid;
    divid.css('height', p_height);
    divid.css('width', p_width);
    divid.fadeIn();
    $('.overlay').fadeIn();
    $('body').width($('body').width());
    $('body').css('overflow', 'hidden');
    $('#' + FocusInId).focus();
}

function openPopupForResult(FocusInId, FocusOutID, dv_id, p_height, p_width, data) {

    debugger;
    fid = FocusOutID;
    var divid = typeof (dv_id) != 'object' ? $('#' + dv_id) : dv_id;
    divi = divid;
    divid.css('height', p_height);
    divid.css('width', p_width);
    divid.slideDown();
    $('.overlay').fadeIn();
    $('body').width($('body').width());
    $('body').css('overflow', 'hidden');
    $('#' + FocusInId).focus();
    var str = data.toString().replace(/,/g, '\n');

    $('.dv_PopUpContentfororder').html(str);
}

function closePop() {
    $('.overlay').fadeOut();
    //alert(typeof (divi));

    divi.fadeOut();
    $('body').removeAttr('style')
    if ($(fid).length == 1) {
        $(fid).focus();
    }
    else {
        $('#' + fid).focus();
    }
}


$(document).ready(function () {
    $('.closePop,.overlay').click(function () {

        $('body').removeAttr('style');
    });
});

var PageDataArray = new Array();
var counter = 0;
var DDVal = new Array();
function getdata() {
    debugger;
    PageDataArray[counter] = $('#dv_PartialView').clone(true, true);
    var temparray = new Array();
    $('#dv_PartialView  select').each(function (i, item) {
        temparray[i] = {
            id: $(this).attr('id'),
            value: $(this).val()
        }
    });
    DDVal[counter] = temparray;
    counter++;
}

function setdata() {
    counter--;
    var tempsetArray = new Array();
    tempsetArray = DDVal[counter];
    $('#dv_PartialView').replaceWith(PageDataArray[counter]);
    $.each(tempsetArray, function (i, tr) {
        $('#' + tr.id).val(tr.value);
    });
}





function fn_wrnMsg(mtxt) {
    $('.warningMsg').stop().slideDown().delay(3000).slideUp();
    $('.warningMsg').find('p').html('<i class="fa fa-exclamation-triangle war"></i>' + mtxt);

}

///For 9000 milisec
function fn_FixwrnMsg(mtxt) {
    $('.warningMsg').stop().slideDown().delay(9000).slideUp();
    $('.warningMsg').find('p').html('<i class="fa fa-exclamation-triangle war"></i>' + mtxt);

}

function fn_successMsg(mtxt) {
    $('.successMsg').stop().slideDown().delay(3000).slideUp();
    $('.successMsg').find('p').html('<i class="fa fa-check-circle"></i>' + mtxt);
}

function fn_FailMsg(mtxt) {
    $('.errorMsg').stop().slideDown().delay(3000).slideUp();
    $('.errorMsg').find('p').html('<i class="fa fa-ban fa-3x"></i>' + mtxt);
}

function fn_FixFailMsg(mtxt) {
    $('.errorMsg').stop().slideDown().delay(9000).slideUp();
    $('.errorMsg').find('p').html('<i class="fa fa-ban fa-3x"></i>' + mtxt);
}

function ValidateForm(FormId) {
    $('#' + FormId).validationEngine('attach', { promptPosition: "bottomLeft" });
}
function hideAllErrMsg(FormId) {
    $('#' + FormId).validationEngine('hideAll')
}
function IsFormValidate(formID) {
    return $('#' + formID).validationEngine('validate');
}


function GetEnumValue(url, key, obj, controlId) {
    $.getJSON(url, { ddlId: $('#' + obj).val() }, function (data) {
        $('#' + controlId).empty();
        $.each(data, function (row, item) {
            $('#' + controlId).append($('<option/>', {
                value: item.Value,
                text: item.Text
            }));
        });
        $('#' + controlId + ' option:first').before('<option value=-1>---Select---</option>');
    });
}



function logout() {
    $.ajax({
        type: "get",
        url: "/VendorRegistration/vendorLogout/",
        data: {},
        beforeSend: function () {
            $('#spinner').show()
        },
        success: function (data) {
            var loc = window.location.protocol + '//' + window.location.host + data;
            alert(loc);
            window.location = loc;
            $('#spinner').hide()
        }
    })
}


function uploadFile(elementId, url, callback) {
   // $('#spinner').show()
    var $id = $("#" + elementId);
    debugger;
    //var formData = new FormData($id[0]);
    var data = new FormData();

    var files = $id.get(0).files;

    if (files.length > 0) {
        debugger;
        data.append("uploadedFile", files[0]);

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            //async: false,
            beforeSend: function () {
                $('#spinner').show()
            },
            success: function (data) {
                if (callback != null) {
                    debugger;
                    callback(data);
                    
                }
            },
            complete: function () {
                $('#spinner').hide()
            } ,
            cache: false,
            contentType: false,
            processData: false
        });
    } else {
        alert("Please select file");
    }

}

function uploadFileQuntityUpdate(elementId, url, callback) {
    // $('#spinner').show()
    closePop();
    debugger;
    var $id = $("#" + elementId);
    debugger;
    //var formData = new FormData($id[0]);
    var data = new FormData();

    var files = $id.get(0).files;

    if (files.length > 0) {
        debugger;
        data.append("uploadedFile", files[0]);

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            async: true,
            dataType: "json",
            //async: isAsync,
            beforeSend: function () {
                // $('#spinner').show()
            },
            success: function (data) {
                if (callback != null) {
                    debugger;
                    callback(data);

                }
            },
            complete: function () {
                // $('#spinner').hide()
            },
            cache: false,
            contentType: false,
            processData: false
        });
    } else {
        alert("Please select file");
    }

}

function uploadMultipleFile(elementIds, url, callback) {
    debugger;
    // $('#spinner').show()
    var id = new Array();
    //var files=new Array();
    var Elements=new Array();
    Elements=elementIds.split(',');
    for(var i=0;i<Elements.length;i++)
    {
       // files =files+ $("#"+Elements[i]).get(0).files;
        id[i] = $("#" + Elements[i]);
     
    }

   // var files = id[0].get(0).files;
   // var $id1 = $("#" + elementId1);
   // var $id2 = $("#" + elementId2);
    debugger;
    ////var formData = new FormData($id[0]);
    var data = new FormData();
    //var files = id[0].get(0).files;
    //var files1 = id[1].get(0).files;
    //var files = $id1.get(0).files;

   
    var files1 = id[0].get(0).files;
    var files2 = id[1].get(0).files;
   
    //alert(files[0]);

    if (files1.length > 0) {

        data.append("file", files1[0]);
        data.append("file", files2[0]);
        //for (var i = 0; i < files.length; i++)
        //{
        //    data.append("file", files[i]);

        //}
        
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            //async: false,
            beforeSend: function () {
                $('#spinner').show()
            },
            success: function (data) {
                if (callback != null) {
                    callback(data);
                }
            },
            complete: function () {
                $('#spinner').hide()
            },
            cache: false,
            contentType: false,
            processData: false
        });
    } else {
        alert("Please select file");
    }
   
}