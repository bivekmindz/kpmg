$(document).ready(function () {
	/*Tabier Script*/
	$(".tabs-menu a").click(function(event) {
		event.preventDefault();
		$(this).parent().addClass("current");
		$(this).parent().siblings().removeClass("current");
		var tab = $(this).attr("href");
		$(".tab-content").not(tab).css("display", "none");
		$(tab).fadeIn();
	});
			
	/*Slider Script*/
	var startSlide1 = $('.active').closest('li').index()
    slider = $('.slider1').bxSlider({
        slideWidth: 220,
        minSlides: 2,
        maxSlides: 5,
		moveSlides: 1,
		auto: true,
		startingSlide: 3,
        pager: false,
        slideMargin: 100,
		 onSliderLoad: function () {
        $('.slider1>li:not(.bx-clone)').eq(2).addClass('active-slide');
    },
    onSlideAfter: function ($slideElement, oldIndex, newIndex) {
        $('.slide').removeClass('active-slide');
        $($slideElement).next().next().addClass('active-slide');        
    }
    });
	

	
	
	/*slicknav*/
	$('#menu-infiexp').slicknav({
		//label: 'Other Menu'
		});


    /*Tabier Script*/
    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
    /*Sticky Script*/
    $('body > nav').spaNav(); // plugin init
});
$(window).scroll(function () {
    $('#section-2,#section-5').find('.inner').animate({
        opacity: 1
    }, 1500);
    if ($(this).scrollTop() > 1) {
        $('.NgageLogo').addClass("sticky1");
    }
    else {
        $('.NgageLogo').removeClass("sticky1");
    }
    if ($(this).scrollTop() > 100) {
        $('#backtop').fadeIn();
    } else {
        $('#backtop').fadeOut();
    }
});
/*label show hide script*/
function placeholderOnFocus(obj, lblId) {
    if (obj.value == "") {
        $("#" + lblId).hide();
    }
}
function placeholderOnBlur(obj, lblId) {
    if (obj.value == "") {
        $("#" + lblId).show();
    }
}

function ShowOTP(){
$("#USerInfoDiv").hide();
$("#OTPDiv").fadeIn();
return false;
}
function ShowConfirm(){
$("#OTPDiv").hide();
$("#ConfirmDiv").fadeIn();
return false;
}